package edu.cmu.cs.akv.ds.rmi.registry;

import static edu.cmu.cs.akv.ds.rmi.common.Constants.START_REGISTRY_SCRIPT;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * {@code RegistryServiceMain} is the starting point of execution for the registry service.
 */
public final class RegistryServiceMain {
    private static final Logger LOG = LogManager.getLogger(RegistryServiceMain.class);

    /**
     * An empty private constructor to prevent instantiation of this class.
     */
    private RegistryServiceMain() {
    }

    /**
     * The starting point of execution of the registry service. It parses the command line
     * arguments and starts the registry service.
     *
     * @param args - The command line arguments containing the port the service should listen
     *               to for remote commands.
     */
    public static void main(final String[] args) {
        // Parse the command line arguments.
        final Option portOption = new Option("p", "port", true,
                "port the master should listen to");
        portOption.setRequired(true);

        final Options options = new Options();
        options.addOption(portOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final int port = Integer.parseInt(line.getOptionValue('p'));

            // Start the registry service.
            new RegistryService(port).start();
        } catch (final ParseException | NumberFormatException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(START_REGISTRY_SCRIPT, options);
        } catch (final IOException e) {
            LOG.fatal("Received fatal IOException while trying to listen to port", e);
            System.out.println("Unable to listen to port, check logs for more details...dying");
        }
    }
}

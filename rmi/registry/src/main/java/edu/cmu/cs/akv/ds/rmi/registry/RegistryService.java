package edu.cmu.cs.akv.ds.rmi.registry;

import edu.cmu.cs.akv.ds.rmi.common.Utils;
import edu.cmu.cs.akv.ds.rmi.AlreadyBoundException;
import edu.cmu.cs.akv.ds.rmi.NotBoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static edu.cmu.cs.akv.ds.rmi.common.Constants.*;

/**
 * {@code RegistryService} has logic to start a registry service and maintain an object store
 * that can be remotely written to or read from.
 */
public class RegistryService {
    private static final Logger LOG = LogManager.getLogger(RegistryService.class);

    private final ServerSocket commandsSocket;
    private final ConcurrentMap<String, Object> objectStore;

    /**
     * Creates a new instance of {@code RegistryService}
     *
     * @param port - The port the registry service should listen to for remote access.
     * @throws IOException if there was a problem when attempting to listen to the port.
     */
    public RegistryService(final int port) throws IOException {
        this.commandsSocket = new ServerSocket(port);
        this.objectStore = new ConcurrentHashMap<>();
    }

    /**
     * Starts the registry service and listens to remote commands. It invokes the appropriate
     * method and sends a successful or failure response.
     */
    public void start() {
        System.out.println(String.format("Listening on port %d...",
                commandsSocket.getLocalPort()));

        while (true) {
            try {
                try (final Socket connectionSocket = commandsSocket.accept()) {
                    // Read a message from the socket.
                    final Map<String, Object> messageMap =
                            Utils.getRemoteMessage(connectionSocket);
                    System.out.println(
                            "Received message from client '" + messageMap.toString() + "'");
                    LOG.info("Received message from client '" + messageMap.toString() + "'");
                    final Map<String, Object> responseMap = new HashMap<>();

                    // Checks the method name key and invokes the appropriate method.
                    final String methodName = (String) messageMap.get(METHOD_NAME_KEY);
                    if ("bind".equals(methodName)) {
                        try {
                            bind((String) messageMap.get("arg1"), messageMap.get("arg2"));

                            responseMap.put(STATUS_KEY, STATUS_OK);
                        } catch (final AlreadyBoundException e) {
                            LOG.error("Received exception", e);

                            responseMap.put(STATUS_KEY, STATUS_EXCEPTION);
                            responseMap.put(EXCEPTION_KEY, e);
                        }
                    } else if ("lookup".equals(methodName)) {
                        try {
                            final Object o = lookup((String) messageMap.get("arg1"));
                            responseMap.put(STATUS_KEY, STATUS_OK);
                            responseMap.put(RETURN_VALUE_KEY, o);
                        } catch (final NotBoundException e) {
                            LOG.error("Received exception", e);

                            responseMap.put(STATUS_KEY, STATUS_EXCEPTION);
                            responseMap.put(EXCEPTION_KEY, e);
                        }
                    } else if ("rebind".equals(methodName)) {
                        rebind((String) messageMap.get("arg1"), messageMap.get("arg2"));
                        responseMap.put(STATUS_KEY, STATUS_OK);
                    } else if ("unbind".equals(methodName)) {
                        try {
                            unbind((String) messageMap.get("arg1"));
                            responseMap.put(STATUS_KEY, STATUS_OK);
                        } catch (final NotBoundException e) {
                            LOG.error("Received exception", e);

                            responseMap.put(STATUS_KEY, STATUS_EXCEPTION);
                            responseMap.put(EXCEPTION_KEY, e);
                        }
                    } else {
                        // Sends an error response with the methodName doesn't match.
                        responseMap.put(STATUS_KEY, STATUS_EXCEPTION);
                        responseMap.put(EXCEPTION_KEY, new IllegalArgumentException("Bad method"));
                    }

                    // Send the response.
                    Utils.sendRemoteMessage(connectionSocket, responseMap);
                } catch (final ClassCastException | ClassNotFoundException e) {
                    LOG.error("Received bad message from client, ignoring...", e);
                }
            }catch (final IOException e) {
                LOG.fatal("Unable to listen to port", e);
                break;
            }
        }
    }

    /**
     * Binds the object to the name.
     *
     * @param name - The name to bind the object to.
     * @param o - The object to bind.
     * @throws AlreadyBoundException if there was another object already bound to the same name.
     */
    public void bind(final String name, final Object o) throws AlreadyBoundException {
        if (objectStore.putIfAbsent(name, o) != null) {
            throw new AlreadyBoundException(name + " already bound");
        }
        System.out.println("Bound object to name '" + name + "'");
    }

    /**
     * Looks up and returns the object bound to the given name.
     *
     * @param name - The name to which the object is bound.
     * @return the object bound to the given name.
     * @throws NotBoundException if no object is currently bound to the name.
     */
    public Object lookup(final String name) throws NotBoundException {
        final Object o = objectStore.get(name);
        if (o != null) {
            System.out.println("Looked up object with name '" + name + "'");
            return o;
        } else {
            throw new NotBoundException(name + " not bound");
        }
    }

    /**
     * Rebinds the object to the given name. The difference between {@link #bind(String, Object)}
     * and this method is that it replaces an existing object, if any, that was already
     * mapped to the name.
     *
     * @param name - The name to which the object should be bound.
     * @param o - The object to rebind.
     */
    public void rebind(final String name, final Object o) {
        objectStore.put(name, o);
        System.out.println("Rebound object to name '" + name + "'");
    }

    /**
     * Unbinds an object from a name.
     *
     * @param name - The name the object was bound to.
     * @throws NotBoundException if no object was bound to the name.
     */
    public void unbind(final String name) throws NotBoundException {
        if (objectStore.remove(name) == null) {
            throw new NotBoundException(name + " not bound");
        }
        System.out.println("Unbound object from name '" + name + "'");
    }
}

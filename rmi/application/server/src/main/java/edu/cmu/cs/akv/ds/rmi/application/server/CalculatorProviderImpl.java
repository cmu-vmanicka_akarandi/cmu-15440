package edu.cmu.cs.akv.ds.rmi.application.server;

import edu.cmu.cs.akv.ds.rmi.application.shared.CalculatorProvider;
import edu.cmu.cs.akv.ds.rmi.application.shared.PostfixCalculator;
import edu.cmu.cs.akv.ds.rmi.application.shared.PrefixCalculator;
import edu.cmu.cs.akv.ds.rmi.RemoteException;

/**
 * {@code CalculatorProviderImpl} provides accessors for a
 * {@link edu.cmu.cs.akv.ds.rmi.application.shared.PrefixCalculator} and a
 * {@link edu.cmu.cs.akv.ds.rmi.application.shared.PostfixCalculator}.
 */
public class CalculatorProviderImpl implements CalculatorProvider {

    /**
     * {@inheritDoc}
     */
    @Override
    public PrefixCalculator getPrefixCalculator() throws RemoteException {
        return new PrefixCalculatorImpl();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PostfixCalculator getPostfixCalculator() throws RemoteException {
        return new PostfixCalculatorImpl();
    }
}

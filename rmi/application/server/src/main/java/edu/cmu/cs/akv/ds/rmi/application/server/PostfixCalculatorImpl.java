package edu.cmu.cs.akv.ds.rmi.application.server;

import edu.cmu.cs.akv.ds.rmi.RemoteException;
import edu.cmu.cs.akv.ds.rmi.application.shared.PostfixCalculator;

import java.util.Stack;
import java.util.StringTokenizer;

/**
 * {@code PostfixCalculatorImpl} evaluates postfix expressions.
 */
public class PostfixCalculatorImpl implements PostfixCalculator {

    /**
     * {@inheritDoc}
     */
    @Override
    public Double evaluate(String exp) throws RemoteException, IllegalArgumentException {
        final StringTokenizer stringTokenizer = new StringTokenizer(exp.trim());
        final Stack<Double> stack = new Stack<>();

        // Parse the space separated tokens
        while (stringTokenizer.hasMoreElements()) {
            final String token=stringTokenizer.nextToken();
            switch (token) {
                case "+":
                    if (stack.size() < 2) {
                        throw new IllegalArgumentException("Not a correct postfix expression.");
                    } else {
                        final Double d2 = stack.pop();
                        final Double d1 = stack.pop();
                        stack.push(d1 + d2);
                    }
                    break;
                case "-":
                    if (stack.size() < 2) {
                        throw new IllegalArgumentException("Not a correct postfix expression.");
                    } else {
                        final Double d2 = stack.pop();
                        final Double d1 = stack.pop();
                        stack.push(d1 - d2);
                    }
                    break;
                case "*":
                    if (stack.size() < 2) {
                        throw new IllegalArgumentException("Not a correct postfix expression.");
                    } else {
                        final Double d2 = stack.pop();
                        final Double d1 = stack.pop();
                        stack.push(d1 * d2);
                    }
                    break;
                case "/":
                    if (stack.size() < 2) {
                        throw new IllegalArgumentException("Not a correct postfix expression.");
                    } else {
                        final Double d2 = stack.pop();
                        final Double d1 = stack.pop();
                        if (Math.abs(d2) < 1.0e-12) {
                            throw new IllegalArgumentException("Attempt to divide by zero.");
                        } else {
                            stack.push(d1 / d2);
                        }
                    }
                    break;
                default:
                    try {
                        final Double d = Double.parseDouble(token);
                        stack.push(d);
                    } catch (NumberFormatException e) {
                        throw new IllegalArgumentException("Expression contains invalid tokens.");
                    }
            }
        }

        if(stack.size() != 1) {
            throw new IllegalArgumentException("Not a correct postfix expression.");
        } else {
            return stack.pop();
        }
    }
}

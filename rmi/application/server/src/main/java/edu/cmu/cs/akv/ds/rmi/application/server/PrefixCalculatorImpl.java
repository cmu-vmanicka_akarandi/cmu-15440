package edu.cmu.cs.akv.ds.rmi.application.server;

import edu.cmu.cs.akv.ds.rmi.application.shared.PrefixCalculator;
import edu.cmu.cs.akv.ds.rmi.RemoteException;

import java.util.Stack;
import java.util.StringTokenizer;

/**
 * {@code PrefixCalculatorImpl} evaluates prefix expressions.
 */
public class PrefixCalculatorImpl implements PrefixCalculator {

    /**
     * {@inheritDoc}
     */
    @Override
    public Double evaluate(String exp) throws RemoteException, IllegalArgumentException {
        try {
            final StringTokenizer stringTokenizer = new StringTokenizer(exp.trim());
            final Stack<String> stack = new Stack<>();

            // Parse the space separated tokens.
            while(stringTokenizer.hasMoreElements())
            {
                String token=stringTokenizer.nextToken();
                stack.push(token);
                while(stack.size()>=3)
                {
                    if("+".equals(stack.peek()) || "-".equals(stack.peek())
                            || "*".equals(stack.peek()) || "/".equals(stack.peek())) {
                        break;
                    } else {
                        final Double d2 = Double.parseDouble(stack.pop());
                        if("+".equals(stack.peek()) || "-".equals(stack.peek())
                                || "*".equals(stack.peek()) || "/".equals(stack.peek()))
                        {
                            stack.push(d2.toString());
                            break;
                        } else {
                            final Double d1 = Double.parseDouble(stack.pop());
                            final Double res;

                            if ("+".equals(stack.peek())) {
                                res = d1 + d2;
                            } else if ("-".equals(stack.peek())) {
                                res = d1 - d2;
                            } else if ("*".equals(stack.peek())) {
                                res = d1 * d2;
                            } else if ("/".equals(stack.peek())) {
                                if (Math.abs(d2) < 1.0e-12) {
                                    throw new IllegalArgumentException(
                                            "Attempt to divide by zero.");
                                } else {
                                    res = d1 / d2;
                                }
                            } else {
                                throw new IllegalArgumentException(
                                        "Not a correct prefix expression.");
                            }
                            stack.pop();
                            stack.push(res.toString());
                        }
                    }
                }
            }

            if(stack.size() != 1) {
                throw new IllegalArgumentException("Not a correct prefix expression.");
            } else if("+".equals(stack.peek()) || "-".equals(stack.peek())
                    || "*".equals(stack.peek()) || "/".equals(stack.peek()))
            {
                throw new IllegalArgumentException("Not a correct prefix expression.");
            } else {
                return Double.parseDouble(stack.pop());
            }
        } catch (final NumberFormatException e) {
            throw new IllegalArgumentException("Expression contains invalid tokens.");
        }
    }
}

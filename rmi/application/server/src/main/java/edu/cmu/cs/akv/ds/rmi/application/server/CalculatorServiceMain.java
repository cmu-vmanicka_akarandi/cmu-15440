package edu.cmu.cs.akv.ds.rmi.application.server;

import edu.cmu.cs.akv.ds.rmi.RemoteException;
import edu.cmu.cs.akv.ds.rmi.RemoteObjectManager;
import org.apache.commons.cli.*;

import java.io.IOException;

import static edu.cmu.cs.akv.ds.rmi.common.Constants.START_CALCULATOR_SERVICE_SCRIPT;

/**
 * {@code CalculatorServiceMain} parses the command line arguments and binds
 * a {@link edu.cmu.cs.akv.ds.rmi.application.shared.CalculatorProvider} to the registry
 * service.
 */
public class CalculatorServiceMain {
    /**
     * The starting point of entry for the calculator service. It parses the command line
     * arguments and binds a {@link edu.cmu.cs.akv.ds.rmi.application.shared.CalculatorProvider}
     * to the registry service.
     *
     * @param args - The command line arguments containing the registry service ip, registry
     *               service port, dispatcher ip and dispatcher port.
     * @throws IOException if there was a problem with the dispatcher listening to the port.
     * @throws RemoteException if there was a problem communicating with the registry.
     */
    public static void main(String[] args) throws IOException, RemoteException {
        // Parse the command line arguments.
        final Option registryServiceIpOption = new Option("ri", "registry-service-ip", true,
                "ip of the host in which the registry service is running");
        registryServiceIpOption.setRequired(true);
        final Option registryServicePortOption = new Option("rp", "registry-service-port", true,
                "port the registry service is listening to");
        registryServicePortOption.setRequired(true);
        final Option ipOption = new Option("i", "ip", true,
                "ip of this host that stubs can use to talk to the dispatcher");
        ipOption.setRequired(true);
        final Option dispatcherPortOption = new Option("dp", "dispatcher-port", true,
                "the port the dispatcher can use to communicate with stubs");
        dispatcherPortOption.setRequired(true);

        final Options options = new Options();
        options.addOption(registryServiceIpOption);
        options.addOption(registryServicePortOption);
        options.addOption(ipOption);
        options.addOption(dispatcherPortOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final String registryServiceIp = line.getOptionValue("ri");
            final int registryServicePort = Integer.parseInt(line.getOptionValue("rp"));
            final String ip = line.getOptionValue('i');
            final int dispatcherPort = Integer.parseInt(line.getOptionValue("dp"));

            // Bind the calculator provider.
            final RemoteObjectManager remoteObjectManager =
                    new RemoteObjectManager(registryServiceIp, registryServicePort, ip,
                            dispatcherPort);
            remoteObjectManager.rebind("calculator_provider", new CalculatorProviderImpl());
            System.out.println("Bound calculator provider to registry...");
        } catch (final ParseException | NumberFormatException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(START_CALCULATOR_SERVICE_SCRIPT, options);
        } catch (final RemoteException e) {
            System.out.println("Received exception while contacting registry, please check " +
                    "if it is started...dying");
            throw e;
        }
    }
}

package edu.cmu.cs.akv.ds.rmi.application.shared;

import edu.cmu.cs.akv.ds.rmi.RemoteException;
import edu.cmu.cs.akv.ds.rmi.common.Utils;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static edu.cmu.cs.akv.ds.rmi.common.Constants.*;
import static edu.cmu.cs.akv.ds.rmi.common.Constants.EXCEPTION_KEY;
import static edu.cmu.cs.akv.ds.rmi.common.Constants.RETURN_VALUE_KEY;

/**
 * {@code PostfixCalculatorStub} is a stub implementation of
 * {@link edu.cmu.cs.akv.ds.rmi.application.shared.PostfixCalculator} that communicates
 * with the {@code Dispatcher} running on the server to complete
 * method invocations.
 */
public class PostfixCalculatorStub implements PostfixCalculator, Serializable {
    private final Integer objectId;
    private final String dispatcherIp;
    private final int dispatcherPort;

    /**
     * Creates a new instance of {@code PostfixCalculatorStub}.
     *
     * @param objectId - The unique id of the object that can be used by the dispatcher
     *                   to locate the actual
     *                   {@link edu.cmu.cs.akv.ds.rmi.application.shared.PostfixCalculator}
     *                   that this object is a stub of.
     * @param dispatcherIp - The IP address of the server host in which the dispatcher is running.
     * @param dispatcherPort - The port to which the dispatcher is listening to.
     */
    public PostfixCalculatorStub(final Integer objectId, final String dispatcherIp,
                                 final int dispatcherPort) {
        this.objectId = objectId;
        this.dispatcherIp = dispatcherIp;
        this.dispatcherPort = dispatcherPort;
    }

    /**
     * Delegates the evaluation of the postfix expression to the actual
     * {@link edu.cmu.cs.akv.ds.rmi.application.shared.PostfixCalculator} in the
     * server.
     *
     * @param exp - The postfix expression to evaluate.
     * @return the result of the evaluation.
     * @throws RemoteException if there was a problem communicating with the dispatcher running
     *   in the server.
     * @throws IllegalArgumentException if the postfix expression was invalid.
     */
    @Override
    public Double evaluate(String exp) throws RemoteException, IllegalArgumentException {
        try {
            // Construct and send a message containing information about the
            // method name, objectId, argument types and values.
            final Map<String, Object> messageMap = new HashMap<>();
            messageMap.put(MESSAGE_TYPE_KEY, STUB_METHOD_CALL);
            messageMap.put(OBJECT_ID_KEY, objectId);
            messageMap.put(METHOD_NAME_KEY, "evaluate");
            messageMap.put(N_ARGS_KEY, 1);
            messageMap.put(String.format(ARG_TYPE_FORMAT, 1), String.class);
            messageMap.put(String.format(ARG_VALUE_FORMAT, 1), exp);

            final Map<String, Object> responseMap =
                    Utils.callRemote(dispatcherIp, dispatcherPort, messageMap);
            // Check if the response status is okay or if there was an exception
            if (!STATUS_OK.equals(responseMap.get(STATUS_KEY))) {
                final Throwable t = (Throwable) responseMap.get(EXCEPTION_KEY);
                if (IllegalArgumentException.class.isAssignableFrom(t.getClass())) {
                    throw new IllegalArgumentException(t.getMessage());
                }
                throw new RemoteException((Throwable) responseMap.get(EXCEPTION_KEY));
            } else {
                return (Double) responseMap.get(RETURN_VALUE_KEY);
            }
        } catch (IOException | ClassNotFoundException | ClassCastException e) {
            throw new RemoteException(e);
        }
    }
}

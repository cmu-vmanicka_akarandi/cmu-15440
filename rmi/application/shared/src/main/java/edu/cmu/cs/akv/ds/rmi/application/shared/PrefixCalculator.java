package edu.cmu.cs.akv.ds.rmi.application.shared;

import edu.cmu.cs.akv.ds.rmi.Remote;
import edu.cmu.cs.akv.ds.rmi.RemoteException;

/**
 * {@code PrefixCalculator} can be used to evaluate prefix expressions.
 */
public interface PrefixCalculator extends Remote {

    /**
     * Evaluates a prefix expression and returns the result of the evaluation.
     *
     * @param exp - The prefix expression to evaluate.
     * @return the result of the prefix expression.
     * @throws RemoteException if there was a problem communicating with the dispatcher/server.
     * @throws IllegalArgumentException if the prefix expression was illegal.
     */
    Double evaluate(String exp) throws RemoteException, IllegalArgumentException;
}

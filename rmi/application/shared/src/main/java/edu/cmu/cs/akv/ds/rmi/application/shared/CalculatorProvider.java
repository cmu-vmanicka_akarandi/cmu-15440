package edu.cmu.cs.akv.ds.rmi.application.shared;

import edu.cmu.cs.akv.ds.rmi.Remote;
import edu.cmu.cs.akv.ds.rmi.RemoteException;

/**
 * {@code CalculatorProvider} provides accessors for a
 * {@link edu.cmu.cs.akv.ds.rmi.application.shared.PrefixCalculator} and a
 * {@link edu.cmu.cs.akv.ds.rmi.application.shared.PostfixCalculator}.
 */
public interface CalculatorProvider extends Remote {

    /**
     * Returns a {@link edu.cmu.cs.akv.ds.rmi.application.shared.PrefixCalculator}.
     *
     * @return a {@link edu.cmu.cs.akv.ds.rmi.application.shared.PrefixCalculator}.
     * @throws RemoteException if there was a problem fetching the
     *   {@link edu.cmu.cs.akv.ds.rmi.application.shared.PrefixCalculator} from the server.
     */
    PrefixCalculator getPrefixCalculator() throws RemoteException;

    /**
     * Returns a {@link edu.cmu.cs.akv.ds.rmi.application.shared.PostfixCalculator}.
     *
     * @return a {@link edu.cmu.cs.akv.ds.rmi.application.shared.PostfixCalculator}.
     * @throws RemoteException if there was a problem fetching the
     *   {@link edu.cmu.cs.akv.ds.rmi.application.shared.PostfixCalculator} from the server.
     */
    PostfixCalculator getPostfixCalculator() throws RemoteException;
}

package edu.cmu.cs.akv.ds.rmi.application.shared;

import edu.cmu.cs.akv.ds.rmi.Remote;
import edu.cmu.cs.akv.ds.rmi.RemoteException;

/**
 * {@code PostfixCalculator} can be used to evaluate postfix expressions.
 */
public interface PostfixCalculator extends Remote {

    /**
     * Evaluates a postfix expression and returns the result of the evaluation.
     *
     * @param exp - The postfix expression to evaluate.
     * @return the result of the postfix expression.
     * @throws RemoteException if there was a problem communicating with the dispatcher/server.
     * @throws IllegalArgumentException if the postfix expression was illegal.
     */
    Double evaluate(String exp) throws RemoteException,IllegalArgumentException;
}

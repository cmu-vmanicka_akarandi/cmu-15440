package edu.cmu.cs.akv.ds.rmi.application.shared;

import edu.cmu.cs.akv.ds.rmi.RemoteException;
import edu.cmu.cs.akv.ds.rmi.common.Utils;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static edu.cmu.cs.akv.ds.rmi.common.Constants.*;

/**
 * {@code CalculatorProviderStub} is a stub implementation of
 * {@link edu.cmu.cs.akv.ds.rmi.application.shared.CalculatorProvider} that communicates
 * with the {@code Dispatcher} running on the server to complete
 * method invocations.
 */
public class CalculatorProviderStub implements CalculatorProvider, Serializable {
    private final Integer objectId;
    private final String dispatcherIp;
    private final int dispatcherPort;

    /**
     * Creates a new instance of {@code CalculatorProviderStub}.
     *
     * @param objectId - The unique id of the object that can be used by the dispatcher
     *                   to locate the actual
     *                   {@link edu.cmu.cs.akv.ds.rmi.application.shared.CalculatorProvider}
     *                   that this object is a stub of.
     * @param dispatcherIp - The IP address of the server host in which the dispatcher is running.
     * @param dispatcherPort - The port to which the dispatcher is listening to.
     */
    public CalculatorProviderStub(final Integer objectId, final String dispatcherIp,
                                  final int dispatcherPort) {
        this.objectId = objectId;
        this.dispatcherIp = dispatcherIp;
        this.dispatcherPort = dispatcherPort;
    }

    /**
     * Delegates the invocation of this method to the actual
     * {@link edu.cmu.cs.akv.ds.rmi.application.shared.CalculatorProvider} in the server.
     *
     * @return the {@link edu.cmu.cs.akv.ds.rmi.application.shared.PrefixCalculator}
     *         returned from the server.
     * @throws RemoteException if there was a problem communicating with the server.
     */
    @Override
    public PrefixCalculator getPrefixCalculator() throws RemoteException {
        try {
            // Construct and send a message containing information about the
            // method name and objectId
            final Map<String, Object> messageMap = new HashMap<>();
            messageMap.put(MESSAGE_TYPE_KEY, STUB_METHOD_CALL);
            messageMap.put(OBJECT_ID_KEY, objectId);
            messageMap.put(METHOD_NAME_KEY, "getPrefixCalculator");
            messageMap.put(N_ARGS_KEY, 0);
            final Map<String, Object> responseMap =
                    Utils.callRemote(dispatcherIp, dispatcherPort, messageMap);
            // Check if the response status is okay or if there was an exception
            if (!STATUS_OK.equals(responseMap.get(STATUS_KEY))) {
                throw new RemoteException((Throwable) responseMap.get(EXCEPTION_KEY));
            } else {
                return (PrefixCalculator) responseMap.get(RETURN_VALUE_KEY);
            }
        } catch (IOException | ClassNotFoundException | ClassCastException e) {
            throw new RemoteException(e);
        }
    }

    /**
     * Delegates the invocation of this method to the actual
     * {@link edu.cmu.cs.akv.ds.rmi.application.shared.CalculatorProvider} in the server.
     *
     * @return the {@link edu.cmu.cs.akv.ds.rmi.application.shared.PostfixCalculator}
     *         returned from the server.
     * @throws RemoteException if there was a problem communicating with the server.
     */
    @Override
    public PostfixCalculator getPostfixCalculator() throws RemoteException {
        try {
            // Construct and send a message containing information about the
            // method name and objectId
            final Map<String, Object> messageMap = new HashMap<>();
            messageMap.put(MESSAGE_TYPE_KEY, STUB_METHOD_CALL);
            messageMap.put(OBJECT_ID_KEY, objectId);
            messageMap.put(METHOD_NAME_KEY, "getPostfixCalculator");
            messageMap.put(N_ARGS_KEY, 0);
            final Map<String, Object> responseMap =
                    Utils.callRemote(dispatcherIp, dispatcherPort, messageMap);
            // Check if the response status is okay or if there was an exception
            if (!STATUS_OK.equals(responseMap.get(STATUS_KEY))) {
                throw new RemoteException((Throwable) responseMap.get(EXCEPTION_KEY));
            } else {
                return (PostfixCalculator) responseMap.get(RETURN_VALUE_KEY);
            }
        } catch (IOException | ClassNotFoundException | ClassCastException e) {
            throw new RemoteException(e);
        }
    }
}

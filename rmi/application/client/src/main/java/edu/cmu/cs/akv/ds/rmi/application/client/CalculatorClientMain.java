package edu.cmu.cs.akv.ds.rmi.application.client;

import edu.cmu.cs.akv.ds.rmi.NotBoundException;
import edu.cmu.cs.akv.ds.rmi.RemoteException;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static edu.cmu.cs.akv.ds.rmi.common.Constants.START_CALCULATOR_CLIENT_SCRIPT;

/**
 * {@code CalculatorClientMain} is the starting point of execution for the
 * {@link edu.cmu.cs.akv.ds.rmi.application.client.CalculatorApp}.
 * It parses the command line arguments and supplies them to the
 * {@link edu.cmu.cs.akv.ds.rmi.application.client.CalculatorApp}.
 */
public class CalculatorClientMain {
    private static final Logger LOG = LogManager.getLogger(CalculatorClientMain.class);

    /**
     * Parses the command line arguments and supplies them to the
     * {@link edu.cmu.cs.akv.ds.rmi.application.client.CalculatorApp}.
     *
     * @param args - Command line arguments containing the ip and port of the registry service.
     *
     * @throws NotBoundException if the server did not bind the
     *   {@link edu.cmu.cs.akv.ds.rmi.application.shared.CalculatorProvider}.
     * @throws RemoteException if the server or registry is down.
     */
    public static void main(final String args[]) throws NotBoundException, RemoteException {
        // Parse command line arguments
        final Option registryServiceIpOption = new Option("ri", "registry-service-ip", true,
                "ip of the host in which the registry service is running");
        registryServiceIpOption.setRequired(true);
        final Option registryServicePortOption = new Option("rp", "registry-service-port", true,
                "port the registry service is listening to");

        final Options options = new Options();
        options.addOption(registryServiceIpOption);
        options.addOption(registryServicePortOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final String registryServiceIp = line.getOptionValue("ri");
            final int registryServicePort = Integer.parseInt(line.getOptionValue("rp"));

            // Start the calculator app
            new CalculatorApp(registryServiceIp, registryServicePort).start();
        } catch (final ParseException | NumberFormatException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(START_CALCULATOR_CLIENT_SCRIPT, options);
        } catch (RemoteException e) {
            System.out.println("Received exception while talking to registry or server, " +
                    "please check...dying");
            LOG.error("Received exception from registry or server", e);
            throw e;
        } catch (NotBoundException e) {
            System.out.println("Calculator provider not bound to registry, please start server" +
                    " first...dying");
            LOG.error("Received exception from registry", e);
            throw e;
        }
    }
}

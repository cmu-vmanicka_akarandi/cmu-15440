package edu.cmu.cs.akv.ds.rmi.application.client;

import edu.cmu.cs.akv.ds.rmi.NotBoundException;
import edu.cmu.cs.akv.ds.rmi.RegistryClient;
import edu.cmu.cs.akv.ds.rmi.RemoteException;
import edu.cmu.cs.akv.ds.rmi.application.shared.CalculatorProvider;
import edu.cmu.cs.akv.ds.rmi.application.shared.PostfixCalculator;
import edu.cmu.cs.akv.ds.rmi.application.shared.PrefixCalculator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * {@code CalculatorApp} is an application with a command line interface that uses
 * a {@link edu.cmu.cs.akv.ds.rmi.application.shared.CalculatorProvider} looked up from
 * the registry service. It offers a
 * {@link edu.cmu.cs.akv.ds.rmi.application.shared.PrefixCalculator} and a
 * {@link edu.cmu.cs.akv.ds.rmi.application.shared.PostfixCalculator}.
 */
public class CalculatorApp {
    private static final Logger LOG = LogManager.getLogger(CalculatorApp.class);

    private final RegistryClient registryClient;

    /**
     * Creates a new instance of {@code CalculatorApp} using the ip and port of the registry
     * service.
     *
     * @param registryServiceIp - The IP address of the host in which the registry service is
     *                            running.
     * @param registryServicePort - The port to which the registry service is listening.
     */
    public CalculatorApp(final String registryServiceIp, final int registryServicePort) {
        this.registryClient = new RegistryClient(registryServiceIp, registryServicePort);
    }

    /**
     * Starts the application by displaying a welcome message in the command line.
     * Based on the user's choice, the application either evaluates a prefix expression, or
     * evaluates a postfix expression or exits. All the computations are done remotely in the
     * server.
     *
     * @throws RemoteException if the server or registry is down.
     * @throws NotBoundException if the server did not bind the
     *   {@link edu.cmu.cs.akv.ds.rmi.application.shared.CalculatorProvider} in the registry
     *   service.
     */
    public void start() throws RemoteException, NotBoundException {
        final CalculatorProvider calculatorProvider;
        try {
            calculatorProvider =
                    (CalculatorProvider) registryClient.lookup("calculator_provider");
        } catch (final RemoteException e) {
            System.out.println("Received exception while trying to contact registry, please " +
                    "check if it is running...dying");
            throw e;
        } catch (final NotBoundException e) {
            System.out.println("Unable to lookup object, make sure calculator " +
                    "service is started...dying");
            throw e;
        }

        System.out.println("Welcome to the remote calculator app!");

        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("1. Prefix Calculator");
            System.out.println("2. Postfix Calculator");
            System.out.println("0. Exit");
            System.out.println("Choose an option from the menu.");
            System.out.print("> ");
            try {
                final String line = scanner.nextLine();
                final int choice = Integer.parseInt(line);
                switch (choice) {
                    case 0 :
                        System.out.println("Thank you for using the app! Bye.");
                        System.exit(0);
                        break;
                    case 1 :
                        // Fetch a prefix calculator from the server
                        final PrefixCalculator prefixCalculator =
                                calculatorProvider.getPrefixCalculator();
                        System.out.println("Enter a prefix expression" +
                                " (space separated) to compute");
                        final String prefixExp = scanner.nextLine();
                        try {
                            // Compute the expression remotely
                            System.out.println("Result = " + prefixCalculator.evaluate(prefixExp));
                        } catch (final RemoteException e) {
                            LOG.error("Received exception", e);
                            System.out.println("Received remote exception, please try again");
                        } catch (final IllegalArgumentException e) {
                            LOG.error("Bad expression", e);
                            System.out.println(String.format("Server says '%s', please try " +
                                    "again", e.getMessage()));
                        }
                        break;
                    case 2 :
                        // Fetch a postfix calculator from the server
                        final PostfixCalculator postfixCalculator =
                                calculatorProvider.getPostfixCalculator();
                        System.out.println("Enter a postfix expression" +
                                " (space separated) to compute");
                        final String postfixExp = scanner.nextLine();
                        try {
                            // Compute the expression remotely
                            System.out.println("Result = "
                                    + postfixCalculator.evaluate(postfixExp));
                        } catch (final RemoteException e) {
                            LOG.error("Received exception", e);
                            System.out.println("Received remote exception, please try again");
                        } catch (final IllegalArgumentException e) {
                            LOG.error("Bad expression", e);
                            System.out.println(String.format("Server says '%s', please try " +
                                    "again", e.getMessage()));
                        }
                        break;
                    default :
                        System.out.println("Invalid choice, please enter one of 1, 2 or 0");
                }
            } catch (final NumberFormatException e) {
                System.out.println("Invalid choice, please enter one of 1, 2 or 0");
            } catch (final NoSuchElementException e) {
                System.out.println("Received kill signal while trying to read from stdin");
                LOG.error("Received exception while reading from prompt", e);
                break;
            }
        }
    }
}

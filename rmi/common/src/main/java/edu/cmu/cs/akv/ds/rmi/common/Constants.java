package edu.cmu.cs.akv.ds.rmi.common;

/**
 * All the constants used by the rmi library
 */
public final class Constants {
    private Constants() {
    }

    public static final String START_REGISTRY_SCRIPT = "./startregistry.sh";
    public static final String START_CALCULATOR_SERVICE_SCRIPT = "./startcalcservice.sh";
    public static final String START_CALCULATOR_CLIENT_SCRIPT = "./startcalcclient.sh";
    public static final String METHOD_NAME_KEY = "method_name";
    public static final String STATUS_KEY = "status";
    public static final String STATUS_OK = "ok";
    public static final String STATUS_EXCEPTION = "exception";
    public static final String EXCEPTION_KEY = "exception";
    public static final String MESSAGE_TYPE_KEY = "message_type";
    public static final String STUB_METHOD_CALL = "stub_method_call";
    public static final String OBJECT_ID_KEY = "object_id";
    public static final String N_ARGS_KEY = "n_args";
    public static final String ARG_TYPE_FORMAT = "arg%dType";
    public static final String ARG_VALUE_FORMAT = "arg%dValue";
    public static final String RETURN_VALUE_KEY = "return_value";
}

package edu.cmu.cs.akv.ds.rmi.common;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.*;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * {@code Utils} class provides a set of reusable static methods for serialization and
 * communication over the wire.
 */
public final class Utils {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * Empty private constructor to prevent instantiation of this class.
     */
    private Utils() {
    }

    /**
     * Serializes the message map, sends it the the said ip and port, waits for a response
     * and deserializes the response.
     *
     * @param ip - The IP address of the host to which the message should be sent.
     * @param port - The port of the process thread that should receive this message.
     * @param messageMap - The map of key-value pairs that should be serialized and sent.
     * @return the deserialized key-value pairs response from the other end of the socket.
     * @throws IOException if there was a problem sending/receiving the message to/from the
     *   intended thread/process.
     * @throws ClassNotFoundException if the classes for the objects in the message are not in
     *   the classpath.
     */
    public static Map<String, Object> callRemote(final String ip, final int port,
                                                 final Map<String, Object> messageMap)
            throws IOException, ClassNotFoundException {
        final Socket socket = new Socket(ip, port);
        sendRemoteMessage(socket, messageMap);
        final Map<String, Object> responseMap = getRemoteMessage(socket);

        socket.close();
        return responseMap;
    }

    /**
     * Accepts a message from the socket and deserializes it.
     *
     * @param remoteSocket - The socket in which to accept the message.
     * @return the deserialized key-value pairs response from the other end of the socket.
     * @throws IOException if there was a problem receiving the message from the intended
     *   thread/process.
     * @throws ClassNotFoundException if the classes for the objects in the message are not in
     *   the classpath.
     */
    public static Map<String, Object> getRemoteMessage(final Socket remoteSocket)
            throws IOException, ClassNotFoundException
    {
        final Scanner in = new Scanner(remoteSocket.getInputStream());
        final String response;
        try {
            // Read the response.
            response = in.nextLine();
        } catch (final NoSuchElementException e) {
            throw new IOException(e);
        }

        // Deserialize the response.
        final Map<String, byte[]> responseMap =
                MAPPER.readValue(response, new TypeReference<Map<String, byte[]>>(){});
        final Map<String, Object> deserializedResponseMap = new HashMap<>();
        for (final String key : responseMap.keySet()) {
            final byte[] value = responseMap.get(key);
            deserializedResponseMap.put(key, deserialize(value));
        }

        return deserializedResponseMap;
    }

    /**
     * Serializes the key-value pairs message and sends it over the wire using the socket.
     *
     * @param remoteSocket - The socket in which the message is to be sent.
     * @param messageMap - The key-value pairs message to send.
     * @throws IOException if there was a problem sending the message over the network.
     */
    public static void sendRemoteMessage(final Socket remoteSocket,
                                         final Map<String, Object> messageMap)
            throws IOException
    {
        // Serialize the message.
        final Map<String, byte[]> serializedMessageMap = new HashMap<>(messageMap.size());
        for (final String key : messageMap.keySet()) {
            final Object value = messageMap.get(key);
            serializedMessageMap.put(key, serialize(value));
        }
        final String message = MAPPER.writeValueAsString(serializedMessageMap);

        // Send the message.
        final DataOutputStream out = new DataOutputStream(remoteSocket.getOutputStream());
        out.writeBytes(message + '\n');
    }

    /**
     * Serializes the object so that it can be sent over the network or stored in a file.
     *
     * @param o - The object to serialize.
     * @return the serialized object as a byte array.
     * @throws IOException if there was a problem serializing the object.
     */
    public static byte[] serialize(final Object o) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final ObjectOutputStream oos = new ObjectOutputStream(baos);

        oos.writeObject(o);
        oos.close();

        return baos.toByteArray();
    }

    /**
     * Deserializes the object from its serialized byte array representation.
     *
     * @param serializedBytes - The serialized byte array representation of the object.
     * @return the deserialized object.
     * @throws IOException if there was a problem deserializing the object.
     * @throws ClassNotFoundException if the class corresponding to the object is not found
     *   in the classpath.
     */
    public static Object deserialize(final byte[] serializedBytes)
            throws IOException, ClassNotFoundException
    {
        final ObjectInputStream ois =
                new ObjectInputStream(new ByteArrayInputStream(serializedBytes));

        final Object object = ois.readObject();
        ois.close();

        return object;
    }
}

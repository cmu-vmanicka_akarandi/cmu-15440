#!/bin/sh

cp ./source/rmi/build/libs/rmi-1.0.jar ./libs/
cp ./source/rmi/registry/build/libs/rmi_registry-1.0.jar ./libs/
cp ./source/rmi/common/build/libs/rmi_common-1.0.jar ./libs/
cp ./source/rmi/application/client/build/libs/rmi_application_client-1.0.jar ./libs/
cp ./source/rmi/application/server/build/libs/rmi_application_server-1.0.jar ./libs/
cp ./source/rmi/application/shared/build/libs/rmi_application_shared-1.0.jar ./libs/

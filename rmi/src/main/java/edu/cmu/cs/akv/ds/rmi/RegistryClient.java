package edu.cmu.cs.akv.ds.rmi;

import static edu.cmu.cs.akv.ds.rmi.common.Constants.*;

import edu.cmu.cs.akv.ds.rmi.common.Utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * {@code RegistryClient} provides methods to communicate with the registry service.
 */
public class RegistryClient {
    private final String registryServiceIp;
    private final int registryServicePort;

    /**
     * Creates a new instance of the {@code RegistryClient}.
     *
     * @param registryServiceIp - The IP address of the host in which the registry
     *                            service is running.
     * @param registryServicePort - The port to which the registry service is listening to.
     */
    public RegistryClient(final String registryServiceIp, final int registryServicePort) {
        this.registryServiceIp = registryServiceIp;
        this.registryServicePort = registryServicePort;
    }

    /**
     * Binds an object to the given name.
     *
     * @param name - The name to bind the object to.
     * @param o - The object to bind.
     * @throws AlreadyBoundException if there was another object already bound to the same name.
     * @throws RemoteException if there was a problem communicating with the registry service.
     */
    public void bind(final String name, final Object o)
            throws AlreadyBoundException, RemoteException
    {
        // Construct the message to send.
        final Map<String, Object> messageMap = new HashMap<>();
        messageMap.put(METHOD_NAME_KEY, "bind");
        messageMap.put("arg1", name);
        messageMap.put("arg2", o);

        try {
            // Send the message and get the response.
            final Map<String, Object> responseMap =
                    Utils.callRemote(registryServiceIp, registryServicePort, messageMap);

            // Check if the response was successful or if there was an exception.
            if (!STATUS_OK.equals(responseMap.get(STATUS_KEY))) {
                final Throwable t = (Throwable) responseMap.get(EXCEPTION_KEY);
                if (t != null) {
                    if (t instanceof AlreadyBoundException) {
                        throw new AlreadyBoundException(name + " already bound");
                    } else {
                        throw new RemoteException(t);
                    }
                } else {
                    throw new RemoteException("Unknown exception from server");
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new RemoteException(e);
        }
    }

    /**
     * Looks up and returns the object bound to the given name.
     *
     * @param name - The name to which the object is bound.
     * @return the object bound to the given name.
     * @throws NotBoundException if no object is currently bound to the name.
     * @throws RemoteException if there was a problem communicating with the registry service.
     */
    public Object lookup(final String name) throws NotBoundException, RemoteException {
        // Construct the message to send.
        final Map<String, Object> messageMap = new HashMap<>();
        messageMap.put(METHOD_NAME_KEY, "lookup");
        messageMap.put("arg1", name);

        try {
            // Send the message and get the response.
            final Map<String, Object> responseMap =
                    Utils.callRemote(registryServiceIp, registryServicePort, messageMap);

            // Check if the response was successful or if there was an exception.
            if (!STATUS_OK.equals(responseMap.get(STATUS_KEY))) {
                final Throwable t = (Throwable) responseMap.get(EXCEPTION_KEY);
                if (t != null) {
                    if (NotBoundException.class.isAssignableFrom(t.getClass())) {
                        throw new NotBoundException(name + " not bound");
                    } else {
                        throw new RemoteException(t);
                    }
                } else {
                    throw new RemoteException("Unknown exception from server");
                }
            } else {
                return responseMap.get(RETURN_VALUE_KEY);
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new RemoteException(e);
        }
    }

    /**
     * Rebinds the object to the given name. The difference between {@link #bind(String, Object)}
     * and this method is that it replaces an existing object, if any, that was already
     * mapped to the name.
     *
     * @param name - The name to which the object should be bound.
     * @param o - The object to rebind.
     * @throws RemoteException if there was a problem communicating with the registry service.
     */
    public void rebind(final String name, final Object o) throws RemoteException {
        // Construct the message to send.
        final Map<String, Object> messageMap = new HashMap<>();
        messageMap.put(METHOD_NAME_KEY, "rebind");
        messageMap.put("arg1", name);
        messageMap.put("arg2", o);

        try {
            // Send the message and get the response.
            final Map<String, Object> responseMap =
                    Utils.callRemote(registryServiceIp, registryServicePort, messageMap);

            // Check if the response was successful or if there was an exception.
            if (!STATUS_OK.equals(responseMap.get(STATUS_KEY))) {
                final Throwable t = (Throwable) responseMap.get(EXCEPTION_KEY);
                if (t != null) {
                    throw new RemoteException(t);
                } else {
                    throw new RemoteException("Unknown exception from server");
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new RemoteException(e);
        }
    }

    /**
     * Unbinds an object from a name.
     *
     * @param name - The name the object was bound to.
     * @throws NotBoundException if no object was bound to the name.
     * @throws RemoteException if there was a problem communicating with the registry service.
     */
    public void unbind(final String name) throws NotBoundException, RemoteException {
        // Construct the message to send.
        final Map<String, Object> messageMap = new HashMap<>();
        messageMap.put(METHOD_NAME_KEY, "unbind");
        messageMap.put("arg1", name);

        try {
            // Send the message and get the response.
            final Map<String, Object> responseMap =
                    Utils.callRemote(registryServiceIp, registryServicePort, messageMap);

            // Check if the response was successful or if there was an exception.
            if (!STATUS_OK.equals(responseMap.get(STATUS_KEY))) {
                final Throwable t = (Throwable) responseMap.get(EXCEPTION_KEY);
                if (t != null) {
                    if (t instanceof NotBoundException) {
                        throw new NotBoundException(name + " not bound");
                    } else {
                        throw new RemoteException(t);
                    }
                } else {
                    throw new RemoteException("Unknown exception from server");
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new RemoteException(e);
        }
    }
}

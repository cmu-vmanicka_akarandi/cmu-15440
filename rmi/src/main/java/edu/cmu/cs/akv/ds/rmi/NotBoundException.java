package edu.cmu.cs.akv.ds.rmi;

/**
 * {@code NotBoundException} is used to indicate exceptions when an attempt
 * was made to look up an object by its name, but no such binding to the name
 * existed.
 */
public class NotBoundException extends Exception {

    /**
     * {@inheritDoc}
     */
    public NotBoundException(final String message) {
        super(message);
    }
}

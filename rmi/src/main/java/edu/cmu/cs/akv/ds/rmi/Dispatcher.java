package edu.cmu.cs.akv.ds.rmi;

import edu.cmu.cs.akv.ds.rmi.common.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import static edu.cmu.cs.akv.ds.rmi.common.Constants.*;

/**
 * The {@code Dispatcher} runs in the server and accept remote method delegations from stubs.
 */
/* package */ class Dispatcher {
    private static final Logger LOG = LogManager.getLogger(Dispatcher.class);

    private final int port;
    private final String publicIp;
    private final Object lock;
    private final Map<Integer, Object> objectsMap;
    private final AtomicInteger idProvider;

    private boolean started;

    /**
     * Creates an instance of the {@code Dispatcher}.
     *
     * @param publicIp - The IP address of this host that can be used by the stubs to communicate
     *                   with the dispatcher.
     * @param port - The port the dispatcher should listen to for remote commands from stubs.
     */
    /* package */ Dispatcher(final String publicIp, final int port) {
        this.port = port;
        this.publicIp = publicIp;
        this.objectsMap = new ConcurrentHashMap<>();
        this.started = false;
        this.lock = new Object();
        this.idProvider = new AtomicInteger(0);
    }

    /**
     * Atomically checks if the {@code Dispatcher} has not already started and starts the same
     * if it wasn't already started.
     */
    /* package */ void startIfStopped() {
        // Check first without acquiring a lock as this is usually true and acquiring locks
        // are expensive.
        if (!started) {
            // Acquire a lock to prevent the dispatcher from being started twice.
            synchronized (lock) {
                // Check again as the state might have changed before acquiring the lock.
                if (!started) {
                    started = true;
                    // Spawn a new thread and start the dispatcher.
                    final ExecutorService es = Executors.newSingleThreadExecutor();
                    es.submit(new Callable<Void>() {
                        @Override
                        public Void call() throws IOException {
                            try {
                                start();
                            } catch (final IOException e) {
                                LOG.error("Unable to listen to dispatcher port", e);
                                throw e;
                            }
                            return null;
                        }
                    });
                }
            }
        }
    }

    /**
     * Starts the {@code Dispatcher}. The {@code Dispatcher} serves remote commands from stubs
     * by invoking the appropriate methods in the appropriate object requested in the message.
     * It also checks if the returned value is a {@link edu.cmu.cs.akv.ds.rmi.Remote} reference
     * and returns a stub of the reference instead if that is the case.
     *
     * @throws IOException if there was a problem when attempting to listen to the port.
     */
    private void start() throws IOException {
        System.out.println("Starting dispatcher...");
        LOG.info("Starting dispatcher...");

        final ServerSocket serverSocket = new ServerSocket(port);
        while (true) {
            // Block and wait for a client connection.
            try (final Socket connectionSocket = serverSocket.accept()) {
                // Read the remote message.
                final Map<String, Object> messageMap = Utils.getRemoteMessage(connectionSocket);
                System.out.println("Received message from client '" + messageMap.toString() + "'");
                LOG.info("Received message from client '" + messageMap.toString() + "'");

                // Make sure the message typ was a stub method call. This allows
                // for future extensibility where new message types can be added.
                if (STUB_METHOD_CALL.equals(messageMap.get(MESSAGE_TYPE_KEY))) {
                    final Integer objectId = (Integer) messageMap.get(OBJECT_ID_KEY);
                    final Object o = objectsMap.get(objectId);
                    // Make sure we have the object in the store that the stub wants to delegate
                    // the invocation to.
                    if (o == null) {
                        // Return an error response if we cannot find the object.
                        final Map<String, Object> responseMap = new HashMap<>();
                        responseMap.put(STATUS_KEY, STATUS_EXCEPTION);
                        responseMap.put(EXCEPTION_KEY, new RemoteException("Object not found"));
                        Utils.sendRemoteMessage(connectionSocket, responseMap);
                    } else {
                        final Integer nArgs = (Integer) messageMap.get(N_ARGS_KEY);
                        final String methodName = (String) messageMap.get(METHOD_NAME_KEY);
                        final Class<?>[] argTypes = new Class[nArgs];
                        final Object[] argValues = new Object[nArgs];
                        for (Integer i = 1; i <= nArgs; i++) {
                            argTypes[i - 1] =
                                    (Class<?>) messageMap.get(String.format(ARG_TYPE_FORMAT, i));
                            argValues[i - 1] = messageMap.get(String.format(ARG_VALUE_FORMAT, i));
                        }
                        // Locate the method using information from the message about its
                        // signature.
                        final Method m = o.getClass().getDeclaredMethod(methodName, argTypes);
                        try {
                            final Object returnVal = m.invoke(o, argValues);
                            // If the return type is Remote, then return a stub instead.
                            if (returnVal instanceof Remote) {
                                final Object retStub = createStub(returnVal);

                                final Map<String, Object> responseMap = new HashMap<>();
                                responseMap.put(STATUS_KEY, STATUS_OK);
                                responseMap.put(RETURN_VALUE_KEY, retStub);
                                Utils.sendRemoteMessage(connectionSocket, responseMap);
                            } else {
                                final Map<String, Object> responseMap = new HashMap<>();
                                responseMap.put(STATUS_KEY, STATUS_OK);
                                responseMap.put(RETURN_VALUE_KEY, returnVal);
                                Utils.sendRemoteMessage(connectionSocket, responseMap);
                            }
                        } catch (final Throwable t) {
                            // If there was an exception during the invocation, then send
                            // an error response.
                            if (t instanceof InvocationTargetException) {
                                final Map<String, Object> responseMap = new HashMap<>();
                                responseMap.put(STATUS_KEY, STATUS_EXCEPTION);
                                responseMap.put(EXCEPTION_KEY, t.getCause());
                                Utils.sendRemoteMessage(connectionSocket, responseMap);
                            } else {
                                final Map<String, Object> responseMap = new HashMap<>();
                                responseMap.put(STATUS_KEY, STATUS_EXCEPTION);
                                responseMap.put(EXCEPTION_KEY, t);
                                Utils.sendRemoteMessage(connectionSocket, responseMap);
                            }
                        }
                    }
                } else {
                    LOG.error("Unknown message type " + messageMap.get(MESSAGE_TYPE_KEY));
                    final Map<String, Object> responseMap = new HashMap<>();
                    responseMap.put(STATUS_KEY, STATUS_EXCEPTION);
                    responseMap.put(EXCEPTION_KEY, new RemoteException("Unknown request"));
                    Utils.sendRemoteMessage(connectionSocket, responseMap);
                }
            } catch (final IOException | ClassNotFoundException | ClassCastException e) {
                LOG.error("Exception serving client, ignoring...", e);
            } catch (final Throwable t) {
                LOG.error("Unexpected exception while serving client, ignoring...", t);
            }
        }
    }

    /**
     * Atomically increments and returns the id counter's value.
     *
     * @return the id counter's value after atomically incrementing it.
     */
    private Integer nextId() {
        return idProvider.incrementAndGet();
    }

    /**
     * Given an object, identifies the Remote interface it implements and creates a stub
     * for the same using reflection. The {@code Dispatcher} also adds the remote object
     * to its store to allow the stub to invoke its methods remotely later.
     *
     * @param o - The object for which the stub should be created.
     * @return the stub instance for the object.
     * @throws ClassNotFoundException if no stub class was found for the object.
     * @throws NoSuchMethodException if the stub class doesn't have an empty public constructor.
     * @throws IllegalAccessException if there was an access problem when instantiating the stub
     *   class.
     * @throws InvocationTargetException if there was a problem invoking the constructor of the
     *   stub class.
     * @throws InstantiationException if there was a problem instantiating the stub class.
     */
    /* package */ Object createStub(final Object o) throws ClassNotFoundException,
            NoSuchMethodException, IllegalAccessException, InvocationTargetException,
            InstantiationException
    {
        Class<?>[] interfaces = o.getClass().getInterfaces();

        // Look for the remote interface
        Class<?> remoteInterface = null;
        for (Class<?> interfaceI : interfaces) {
            if (Remote.class.isAssignableFrom(interfaceI)) {
                remoteInterface = interfaceI;
                break;
            }
        }
        if (remoteInterface == null) {
            throw new ClassNotFoundException("Stub not found");
        }

        // Instantiate the stub with enough information to locate the dispatcher and
        // the original object.
        final String stubClassName = remoteInterface.getName() + "Stub";
        final Class<?> stubClass = Class.forName(stubClassName);
        final Constructor<?> c = stubClass.getDeclaredConstructor(
                Integer.class, String.class, int.class);

        // Add the original object to the store.
        final Integer oId = nextId();
        objectsMap.put(oId, o);

        // Create and return a new instance of the stub.
        return c.newInstance(oId, publicIp, port);
    }

}

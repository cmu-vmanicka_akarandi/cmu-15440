package edu.cmu.cs.akv.ds.rmi;

/**
 * A {@code RemoteObjectManager} is a thin wrapper around the
 * {@link edu.cmu.cs.akv.ds.rmi.RegistryClient} and the
 * {@link edu.cmu.cs.akv.ds.rmi.Dispatcher} that makes it easier to write RMI server code.
 */
public class RemoteObjectManager {
    private final RegistryClient registryClient;
    private final Dispatcher dispatcher;

    /**
     * Creates a new instance of {@code RemoteObjectManager}
     *
     * @param registryServiceIp - The IP address of the host in which the registry service is
     *                            running.
     * @param registryServicePort - The port the registry service is listening to.
     * @param dispatcherIp - The IP address of this host that can be used by the stubs to
     *                       communicate with the dispatcher running in the server.
     * @param dispatcherPort - The port the dispatcher should listen to.
     */
    public RemoteObjectManager(final String registryServiceIp,
                               final int registryServicePort,
                               final String dispatcherIp,
                               final int dispatcherPort)
    {
        this.registryClient = new RegistryClient(registryServiceIp, registryServicePort);
        this.dispatcher = new Dispatcher(dispatcherIp, dispatcherPort);
    }

    /**
     * Binds an object's stub to the given name. It creates a stub for
     * the given object before binding it. It also starts the dispatcher if it was
     * not already started and registers this object with it.
     *
     * @param name - The name to bind the object's stub to.
     * @param o - The object's stub to bind.
     * @throws AlreadyBoundException if there was another object already bound to the same name.
     * @throws RemoteException if there was a problem communicating with the registry service.
     */
    public void bind(final String name, final Object o)
            throws AlreadyBoundException, RemoteException
    {
        dispatcher.startIfStopped();
        final Object stub;
        try {
            stub = dispatcher.createStub(o);
        } catch (Throwable t) {
            throw new RemoteException(t);
        }
        registryClient.bind(name, stub);
    }

    /**
     * Rebinds the object's stub to the given name. The difference between
     * {@link #bind(String, Object)}
     * and this method is that it replaces an existing object, if any, that was already
     * mapped to the name.
     *
     * @param name - The name to which the object should be bound.
     * @param o - The object to rebind.
     * @throws RemoteException if there was a problem communicating with the registry service.
     */
    public void rebind(final String name, final Object o) throws RemoteException {
        dispatcher.startIfStopped();
        final Object stub;
        try {
            stub = dispatcher.createStub(o);
        } catch (Throwable t) {
            throw new RemoteException(t);
        }
        registryClient.rebind(name, stub);
    }

    /**
     * Unbinds an object's stub from the name it was previously bound to.
     *
     * @param name - The name the object's stub was bound to.
     * @throws NotBoundException if no object was bound to the name.
     * @throws RemoteException if there was a problem communicating with the registry service.
     */
    public void unbind(final String name) throws NotBoundException, RemoteException {
        registryClient.unbind(name);
    }
}

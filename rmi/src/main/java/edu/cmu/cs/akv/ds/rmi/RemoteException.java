package edu.cmu.cs.akv.ds.rmi;

/**
 * A {@code RemoteException} indicates any network related issues while
 * communicating with a remote host.
 */
public class RemoteException extends Exception {

    /**
     * {@inheritDoc}
     */
    public RemoteException(final Throwable cause) {
        super(cause);
    }

    /**
     * {@inheritDoc}
     */
    public RemoteException(final String message) {
        super(message);
    }
}

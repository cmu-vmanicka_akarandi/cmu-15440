package edu.cmu.cs.akv.ds.rmi;

/**
 * {@code AlreadyBoundException} indicates an exception because an attempt was
 * made to bind an object to a name that already had an object bound to it.
 */
public class AlreadyBoundException extends Exception {

    /**
     * {@inheritDoc}
     */
    public AlreadyBoundException(final String message) {
        super(message);
    }
}

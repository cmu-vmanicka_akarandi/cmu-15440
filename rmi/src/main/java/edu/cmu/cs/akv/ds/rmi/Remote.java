package edu.cmu.cs.akv.ds.rmi;

/**
 * Interfaces extending the {@code Remote} interface are those that can be passed
 * by remote reference by creating a stub implementation for the same.
 */
public interface Remote {};
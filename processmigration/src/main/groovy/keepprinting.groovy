def suspending;
def initialized = false;

def init(String[] args) {
    suspending=false;
    initialized = true;
}

def execute() {
    println("initialized = " + initialized);
    while (!suspending) {
        println("A\nB");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            break
        }
    }

    suspending = false;
}

def suspend() {
    suspending = true;
    while (suspending);
}

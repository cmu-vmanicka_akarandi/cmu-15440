import edu.cmu.cs.akv.ds.proj1.akv.io.UncachedTransactionFileInputStream
import edu.cmu.cs.akv.ds.proj1.akv.io.UncachedTransactionFileOutputStream

import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream;

/**
 * Zips an input file and produces an output file. This process sleeps multiple times
 * while reading the input file and writing to the output file to allow initiating a
 * migration while I/O is in progress.
 */
class Script {
    static final int BUFFER = 2048

    def suspending;
    UncachedTransactionFileOutputStream fos
    UncachedTransactionFileInputStream fis
    byte[] inputBytes
    int inputBytesOffset
    byte[] outputBytes
    int outputBytesOffset
    String inputPath
    boolean readingDone
    boolean zippingDone
    boolean writingDone

    /**
     * The init method is invoked exactly once in the life time of a process/job (across
     * migrations). This is also the first method that will be invoked.
     *
     * @param args - The arguments to the script passed from the command line.
     */
    def init(String[] args) {
        suspending = false
        readingDone = false
        zippingDone = false
        writingDone = false
        new File(args[1]).getParentFile().mkdirs()
        new File(args[1]).delete()
        inputPath = args[0]
        fis = new UncachedTransactionFileInputStream().forPath(inputPath)
        fos = new UncachedTransactionFileOutputStream().forPath(args[1])
        inputBytes = new byte[BUFFER];
        inputBytesOffset = 0;
        outputBytesOffset = -1;
    }

    /**
     * Checks the ununsed space of the input bytes array and expands it if necessary.
     */
    def checkInputBytes() {
        if (inputBytes.length - inputBytesOffset < BUFFER) {
            inputBytes = Arrays.copyOf(inputBytes, inputBytes.length + BUFFER)
        }
    }

    /**
     * This method is invoked once when the process/job was created and once per migration.
     */
    def execute() {
        try {
            if (!readingDone) {
                // Reading input file into bytes
                InputStream is = new BufferedInputStream(fis, BUFFER)
                checkInputBytes()
                int count
                while ((count = is.read(inputBytes, inputBytesOffset, BUFFER)) != -1) {
                    inputBytesOffset += count
                    checkInputBytes()

                    // Can be suspended when the job is in the middle of reading a file
                    if (suspending) {
                        suspending = false
                        return
                    }
                    // Sleeps to make it easier to suspend while I/O is in progress.
                    try {
                        Thread.sleep(1000)
                    } catch(InterruptedException e) {
                        e.printStackTrace()
                        return
                    }
                }
                readingDone = true
            }

            if (!zippingDone) {
                // Zipping the bytes.
                // The job cannot be suspended when executing in this segment.
                ByteArrayInputStream inputBytesStream = new ByteArrayInputStream(inputBytes)
                ByteArrayOutputStream outputBytesStream = new ByteArrayOutputStream()
                ZipOutputStream zipos =
                        new ZipOutputStream(new BufferedOutputStream(outputBytesStream))
                byte[] buf = new byte[BUFFER]
                zipos.putNextEntry(new ZipEntry(inputPath))
                int count
                while ((count = inputBytesStream.read(buf, 0, BUFFER)) != -1) {
                    zipos.write(buf, 0, count)
                }
                inputBytesStream.close()
                zipos.close()
                outputBytes = outputBytesStream.toByteArray()
                zippingDone = true
            }

            if (!writingDone) {
                // Storing the zipped bytes to file
                for (outputBytesOffset++; outputBytesOffset < outputBytes.length; outputBytesOffset++) {
                    fos.write(outputBytes[outputBytesOffset])

                    // Can be suspended when the job is in the middle of writing to the file
                    if (outputBytesOffset % BUFFER == 0) {
                        if (suspending) {
                            fos.close()
                            suspending = false
                            return
                        }
                        // Sleeps to make it easier to suspend while I/O is in progress.
                        try {
                            Thread.sleep(1000)
                        } catch (InterruptedException e) {
                            e.printStackTrace()
                            return
                        }
                    }
                }
                fos.close()
                writingDone = true
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        suspending = false
    }

    /**
     * This method is invoked before the job is suspended for migration.
     */
    def suspend() {
        suspending = true
        while (suspending);
    }
}

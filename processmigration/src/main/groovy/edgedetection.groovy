import edu.cmu.cs.akv.ds.proj1.akv.io.UncachedTransactionFileInputStream
import edu.cmu.cs.akv.ds.proj1.akv.io.UncachedTransactionFileOutputStream

import javax.imageio.ImageIO
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import java.awt.image.BufferedImageOp
import java.awt.image.ConvolveOp
import java.awt.image.Kernel

/**
 * Reads an input image file and produces an output image file highlighting edges in the original
 * file. This process sleeps multiple times while reading the input file and writing to the
 * output file to allow initiating a migration while I/O is in progress.
 */
class Script {
    static final int BUFFER = 2048

    def suspending;
    UncachedTransactionFileInputStream fis
    UncachedTransactionFileOutputStream fos
    String inputPath
    boolean readingDone
    boolean detectionDone
    boolean writingDone
    byte[] inputBytes
    int inputBytesOffset
    byte[] outputBytes
    int outputBytesOffset

    /**
     * The init method is invoked exactly once in the life time of a process/job (across
     * migrations). This is also the first method that will be invoked.
     *
     * @param args - The arguments to the script passed from the command line.
     */
    def init(String[] args) {
        suspending = false
        readingDone = false
        detectionDone = false
        writingDone = false
        inputPath = args[0]
        new File(args[1]).getParentFile().mkdirs()
        new File(args[1]).delete()
        fis = new UncachedTransactionFileInputStream().forPath(inputPath)
        fos = new UncachedTransactionFileOutputStream().forPath(args[1])
        inputBytes = new byte[BUFFER];
        inputBytesOffset = 0;
        outputBytesOffset = -1;
    }

    /**
     * Checks the ununsed space of the input bytes array and expands it if necessary.
     */
    def checkInputBytes() {
        if (inputBytes.length - inputBytesOffset < BUFFER) {
            inputBytes = Arrays.copyOf(inputBytes, inputBytes.length + BUFFER)
        }
    }

    /**
     * This method is invoked once when the process/job was created and once per migration.
     */
    def execute() {
        try {
            if (!readingDone) {
                // Reading input file into bytes.
                InputStream is = new BufferedInputStream(fis, BUFFER)
                checkInputBytes()
                int count
                while ((count = is.read(inputBytes, inputBytesOffset, BUFFER)) != -1) {
                    inputBytesOffset += count
                    checkInputBytes()

                    // Can be suspended when the job is in the middle of reading a file
                    if (suspending) {
                        suspending = false
                        return
                    }
                    // Sleeps to make it easier to suspend while I/O is in progress.
                    try {
                        Thread.sleep(1000)
                    } catch(InterruptedException e) {
                        e.printStackTrace()
                        return
                    }
                }
                readingDone = true
            }

            if (!detectionDone) {
                // Run edge detection using the bytes.
                // The job cannot be suspended when executing in this segment.
                ByteArrayOutputStream outputBytesStream = new ByteArrayOutputStream()
                BufferedImage img = ImageIO.read(new ByteArrayInputStream(inputBytes))
                float ninth = 1.0f / 9.0f;
                float[] blurKernel = new float[9]
                float[] edgeKernel = new float[9]
                for (int i = 0; i < 9; ++i)
                    blurKernel[i] = ninth;
                edgeKernel[0] = edgeKernel[2] = edgeKernel[6] = edgeKernel[8] = 0.0
                edgeKernel[1] = edgeKernel[3] = edgeKernel[5] = edgeKernel[7] = -1.0
                edgeKernel[4] = 4.0
                BufferedImageOp blur = new ConvolveOp(new Kernel(3, 3, blurKernel));
                BufferedImageOp edge = new ConvolveOp(new Kernel(3, 3, edgeKernel));

                BufferedImage bmg = new BufferedImage(img.getWidth(), img.getHeight(),
                        BufferedImage.TYPE_BYTE_BINARY);
                Graphics2D g = bmg.createGraphics();
                g.drawImage(img, blur, 0, 0);

                BufferedImage omg = new BufferedImage(img.getWidth(), img.getHeight(),
                        BufferedImage.TYPE_BYTE_BINARY);
                g = omg.createGraphics()
                g.drawImage(bmg, edge, 0, 0);
                ImageIO.write(omg, "jpg", outputBytesStream);
                outputBytes = outputBytesStream.toByteArray()
                detectionDone = true
            }

            if (!writingDone) {
                // Storing the edge detected image bytes to file.
                for (outputBytesOffset++; outputBytesOffset < outputBytes.length; outputBytesOffset++) {
                    fos.write(outputBytes[outputBytesOffset])

                    // Can be suspended when the job is in the middle of writing to the file
                    if (outputBytesOffset % BUFFER == 0) {
                        if (suspending) {
                            fos.close()
                            suspending = false
                            return
                        }
                        // Sleeps to make it easier to suspend while I/O is in progress.
                        try {
                            Thread.sleep(1000)
                        } catch (InterruptedException e) {
                            e.printStackTrace()
                            return
                        }
                    }
                }
                fos.close()
                writingDone = true
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        suspending = false
    }

    /**
     * This method is invoked before the job is suspended for migration.
     */
    def suspend() {
        suspending = true
        while (suspending);
    }
}

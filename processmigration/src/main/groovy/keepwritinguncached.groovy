import edu.cmu.cs.akv.ds.proj1.akv.io.UncachedTransactionFileOutputStream;

class Script {
    def suspending
    char current
    UncachedTransactionFileOutputStream fis

    def init(String[] args) {
        current = 'A'
        suspending = false
        fis = new UncachedTransactionFileOutputStream().forPath(args[0])
    }

    def execute() {
        while (!suspending) {
            fis.write((int) current);
            current++;

            try {
                Thread.sleep(1000)
            } catch (InterruptedException e) {
                break
            }
        }

        suspending = false
    }

    def suspend() {
        suspending = true
        while (suspending);
    }
}

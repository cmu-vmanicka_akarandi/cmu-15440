import edu.cmu.cs.akv.ds.proj1.akv.io.CachedTransactionFileInputStream;

class Script {
    def suspending;
    CachedTransactionFileInputStream fis;

    def init(String[] args) {
        suspending = false
        fis = new CachedTransactionFileInputStream().forPath(args[0])
    }

    def execute() {
        while (!suspending) {
            println(new String((char) fis.read()))

            try {
                Thread.sleep(1000)
            } catch (InterruptedException e) {
                break
            }
        }

        suspending = false
    }

    def suspend() {
        suspending = true
        while (suspending);
        fis.clearCache();
    }
}

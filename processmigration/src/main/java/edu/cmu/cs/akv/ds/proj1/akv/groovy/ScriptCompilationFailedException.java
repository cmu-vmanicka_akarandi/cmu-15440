package edu.cmu.cs.akv.ds.proj1.akv.groovy;

/**
 * Represents a failed attempt to compile a groovy script and create a new instance of it in
 * memory.
 */
public class ScriptCompilationFailedException extends Exception {
    /**
     * {@inheritDoc}
     */
    public ScriptCompilationFailedException(String message) {
        super(message);
    }
    /**
     * {@inheritDoc}
     */
    public ScriptCompilationFailedException(Throwable cause) {
        super(cause);
    }
}

package edu.cmu.cs.akv.ds.proj1.akv.groovy;

/**
 * Utility class that has methods to validate that a script does not violate contracts
 * on the methods that it is expected to define and the number of arguments in those methods.
 */
public class ScriptValidator {
    private ScriptValidator() {
    }

    /**
     * Validates that a script does not violate contracts on the methods that it is expected
     * to define and the number of arguments in those methods.
     *
     * @param scriptBytecode - The compiled bytecode of the script to validate.
     * @throws ScriptCompilationFailedException if the validation fails.
     */
    public static void validate(final Class<?> scriptBytecode)
            throws ScriptCompilationFailedException {
        try {
            scriptBytecode.getMethod("init", String[].class);
        } catch (final NoSuchMethodException e) {
            throw new ScriptCompilationFailedException("Method 'init' not found in the script");
        }

        try {
            scriptBytecode.getMethod("execute");
        } catch (final NoSuchMethodException e) {
            throw new ScriptCompilationFailedException("Method 'execute' not found in the script");
        }

        try {
            scriptBytecode.getMethod("suspend");
        } catch (final NoSuchMethodException e) {
            throw new ScriptCompilationFailedException("Method 'run' not found in the script");
        }
    }
}

package edu.cmu.cs.akv.ds.proj1.akv.groovy;

import groovy.lang.GroovyObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Future;

/**
 * Wraps an instance of the script to abstract reflection and other details.
 */
public class ScriptWrapper {
    private static final Logger LOG = LogManager.getLogger(ScriptWrapper.class);

    private final String scriptName;
    private final GroovyObject compiledScript;

    private Future<?> executingFuture;
    private boolean suspended;

    /**
     * Creates a new instance of {@link ScriptWrapper}.
     *
     * @param scriptName - The name of the script that is to be wrapper.
     * @param compiledScript - The compiled byte code of the script.
     */
    public ScriptWrapper(final String scriptName, final GroovyObject compiledScript) {
        this.scriptName = scriptName;
        this.compiledScript = compiledScript;
        this.executingFuture = null;
        this.suspended = false;
    }

    /**
     * Invokes {@code init} on the script instance with {@code args} as parameter.
     *
     * @param args - The {@code args} parameter to pass to the invoke method.
     * @return null if there was no exception or an error message otherwise.
     */
    public String init(final String[] args) {
        suspended = false;
        try {
            compiledScript.invokeMethod("init", args);
            return null;
        } catch (final Throwable t) {
            LOG.error("Received error from script during init", t);
            return t.getMessage();
        }
    }

    /**
     * Invokes {@code execute} on the script instance.
     *
     * @return null if there was no exception or an error message otherwise.
     */
    public String execute() {
        suspended = false;
        try {
            compiledScript.invokeMethod("execute", null);
            return null;
        } catch (final Throwable t) {
            LOG.error("Received error from script during execute", t);
            return t.getMessage();
        }
    }

    /**
     * Suspends execution of the script by killing the thread after invoking the script's
     * {@code suspend} method. It prepares the instance for migration.
     */
    public void suspend() {
        suspended = true;
        try {
            compiledScript.invokeMethod("suspend", null);
        } catch (final Throwable t) {
            LOG.error("Received error from script during suspend, ignoring", t);
        }

        if (executingFuture == null) {
            throw new IllegalStateException("Executing future should be non-null at this point");
        }
        executingFuture.cancel(true);
        executingFuture = null;
    }

    /**
     * Kills the instance to terminate it by killing the thread in which it is running.
     */
    public void kill() {
        suspended = true;

        if (executingFuture == null) {
            throw new IllegalStateException("Executing future should be non-null at this point");
        }
        executingFuture.cancel(true);
        executingFuture = null;
    }

    /**
     * Returns the compiled script instance this wraps.
     * @return the compiled script instance this wraps.
     */
    public GroovyObject getScript() {
        return compiledScript;
    }

    /**
     * Returns the name of the script that was used to create the script instance this wraps.
     * @return the name of the script that was used to create the script instance this wraps.
     */
    public String getScriptName() {
        return scriptName;
    }

    /**
     * Sets the {@link java.util.concurrent.Future} corresponding to the thread
     * in which the wrapped job/process is running.
     *
     * @param executingFuture - The {@link java.util.concurrent.Future} to assign.
     */
    public void setExecutingFuture(final Future<?> executingFuture) {
        this.executingFuture = executingFuture;
    }

    /**
     * Returns true if the job is suspended or false otherwise.
     *
     * @return true if the job is suspended or false otherwise.
     */
    public boolean isSuspended() {
        return suspended;
    }
}

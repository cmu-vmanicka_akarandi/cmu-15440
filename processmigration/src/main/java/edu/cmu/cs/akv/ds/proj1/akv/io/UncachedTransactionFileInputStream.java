package edu.cmu.cs.akv.ds.proj1.akv.io;

import java.io.*;

/**
 * {@code UncachedTransactionFileInputStream} is an {@link java.io.InputStream} that can be
 * suspended and migrated to a different process with access to the same file system.
 * It reopens the file descriptor every time it reads a byte from the file.
 */
public class UncachedTransactionFileInputStream extends InputStream {
    private String path;
    private RandomAccessFile raf;
    private long pos;

    /**
     * Creates a new instance of
     * {@link edu.cmu.cs.akv.ds.proj1.akv.io.UncachedTransactionFileInputStream}
     */
    public UncachedTransactionFileInputStream() {
        this.path = null;
        this.raf = null;
        this.pos = 0;
    }

    /**
     * Sets the path of the file.
     *
     * @param path - The path of the file to set.
     * @return this {@link edu.cmu.cs.akv.ds.proj1.akv.io.UncachedTransactionFileInputStream}
     *         instance to allow for chaining.
     */
    public UncachedTransactionFileInputStream forPath(final String path) {
        if (this.path == null) {
            this.path = path;
        } else {
            throw new IllegalStateException("forFile should be invoked only once");
        }
        this.raf = null;
        this.pos = 0;

        return this;
    }

    /**
     * Reads a single byte from the file after reopening the stream.
     *
     * @return the byte read.
     * @throws IOException if there was a problem reading from the file.
     */
    @Override
    public int read() throws IOException {
        synchronized(this) {
            raf = new RandomAccessFile(new File(path), "r");
            raf.seek(pos);
            int read = raf.read();
            raf.close();
            raf = null;
            pos++;
            return read;
        }
    }
}

package edu.cmu.cs.akv.ds.proj1.akv;

import edu.cmu.cs.akv.ds.proj1.akv.command.CommandsExecutor;
import edu.cmu.cs.akv.ds.proj1.akv.command.MasterCommandsExecutor;
import edu.cmu.cs.akv.ds.proj1.akv.command.SocketCommandsListener;
import edu.cmu.cs.akv.ds.proj1.akv.command.StdinCommandsListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * {@code MasterProcessManager} is the starting point of the master process manager.
 */
public class MasterProcessManager {
    private static final Logger LOG = LogManager.getLogger(MasterProcessManager.class);

    private final CommandsExecutor executor;
    private final int port;
    private final ObjectMapper mapper;
    private final ExecutorService threadPool;

    /**
     * Creates a new instance of {@link edu.cmu.cs.akv.ds.proj1.akv.MasterProcessManager}.
     *
     * @param scriptsPath - The path of the directory containing the scripts.
     * @param port - The port the master should listen to for commands.
     */
    public MasterProcessManager(final String scriptsPath, final int port) {
        this.mapper = new ObjectMapper();
        this.mapper.setVisibilityChecker(mapper.getSerializationConfig()
                .getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));

        this.threadPool = Executors.newCachedThreadPool();
        this.port = port;
        this.executor = new MasterCommandsExecutor(scriptsPath, mapper, port);
    }

    /**
     * Launches a thread to listen to commands from stdin and another to listen to sockets.
     *
     * @throws IOException if there was a problem while listening to sockets.
     */
    public void start() throws IOException {
        launchStdinCommandsExecutorAsync();
        launchSocketCommandsExecutorAsync();
    }

    /**
     * Launches a thread to listen to commands from a network socket.
     */
    public void launchSocketCommandsExecutorAsync() {
        threadPool.submit(new Runnable() {
            public void run() {
                try {
                    LOG.info(String.format("Listening on port [%d]", port));
                    new SocketCommandsListener(port, executor).run();
                } catch (IOException e) {
                    LOG.fatal(String.format("Received exception while trying to listen to port %d",
                            port), e);
                }
            }
        });
    }

    /**
     * Launches a thread to listen to commands from the standard input.
     */
    public void launchStdinCommandsExecutorAsync() {
        threadPool.submit(new Runnable() {
            public void run() {
                new StdinCommandsListener(executor).run();
            }
        });
    }
}

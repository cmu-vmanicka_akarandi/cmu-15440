package edu.cmu.cs.akv.ds.proj1.akv.command;

import edu.cmu.cs.akv.ds.proj1.akv.Slave;
import edu.cmu.cs.akv.ds.proj1.akv.groovy.ScriptLoader;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.cmu.cs.akv.ds.proj1.akv.command.Constants.*;

/**
 * Executes commands submitted to the master either via sockets or stdin.
 */
public class MasterCommandsExecutor implements CommandsExecutor {
    private static final Logger LOG = LogManager.getLogger(MasterCommandsExecutor.class);

    private final ScriptLoader scriptLoader;
    private final ConcurrentHashMap<String, Slave> instancesMap;
    private final Map<Integer, JobMetadata> jobsMap;
    private final AtomicInteger jobIdCounter;
    private final int port;

    /**
     * Creates an instance of {@link edu.cmu.cs.akv.ds.proj1.akv.command.MasterCommandsExecutor}
     *
     * @param scriptsPath - The path of the directory containing the scripts.
     * @param mapper - The {@link org.codehaus.jackson.map.ObjectMapper} to serialize and/or
     *                 deserialize scripts.
     * @param port - The port the master should use to communicate to its slaves.
     */
    public MasterCommandsExecutor(final String scriptsPath,
                                  final ObjectMapper mapper,
                                  final int port) {
        this.scriptLoader = new ScriptLoader(scriptsPath, mapper);
        this.port = port;
        this.instancesMap = new ConcurrentHashMap<String, Slave>();
        this.jobsMap = new ConcurrentHashMap<Integer, JobMetadata>();
        this.jobIdCounter = new AtomicInteger(0);
    }

    /**
     * {@inheritDoc}
     */
    public String execute(final Map<String, Object> context, final String command) {
        final String[] split = command.split(" ", 2);
        final String[] args = split.length == 2 ? splitArgs(split[1]) : new String[]{};

        if (SLAVE_ADD.equals(split[0])) {
            return processSlaveAddCommand(args);
        } else if (LIST_SLAVES.equals(split[0])) {
            return listSlavesCommand();
        } else if (LIST_JOBS.equals(split[0])) {
            return listJobsCommand();
        } else if (JOB_COMPLETED.equals(split[0])) {
            return processJobCompletedCommand(args);
        } else if (JOB_FAILED.equals(split[0])) {
            return processJobFailedCommand(args);
        } else if (MIGRATE.equals(split[0])) {
            return forwardMigrateCommand(args);
        } else if (JOB_KILL.equals(split[0])) {
            return forwardJobKillCommand(args);
        } else if (LIST_SCRIPTS.equals(split[0])) {
            return listScripts();
        } else { // check if its a command to submit a job to a slave
            final Slave slave = instancesMap.get(split[0]);
            if (slave != null) {
                final String[] nameAndArgs = split[1].split(" ", 2);
                final String jobArgs = nameAndArgs.length == 2 ? nameAndArgs[1] : "";
                return forwardStartJobCommand(slave, nameAndArgs[0], jobArgs);
            } else {
                return BAD_COMMAND;
            }
        }
    }

    /**
     * Splits arguments into an array of Strings using space delimiter (except when the arg is
     * surrounded by single quotes).
     *
     * @param unsplit - The flat String containing the space separated arguments.
     * @return the array of String with each element corresponding to an argument.
     */
    private String[] splitArgs(final String unsplit) {
        final List<String> list = new ArrayList<String>();
        final Matcher m = Pattern.compile("([^']\\S*|'.+?')\\s*").matcher(unsplit);
        while (m.find()) {
            String arg = m.group(1);
            if (arg.length() >= 2) {
                if (arg.charAt(0) == '\'') {
                    arg = arg.substring(1);
                }
                if (arg.charAt(arg.length() - 1) == '\'') {
                    arg = arg.substring(0, arg.length() - 1);
                }
            }
            list.add(arg);
        }

        final String[] args = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            args[i] = list.get(i);
        }
        return args;
    }

    /**
     * Lists the scripts that can be run from the command line apart from the inbuilt commands.
     *
     * @return the scripts that can be run from the command line apart from the inbuilt commands.
     */
    private String listScripts() {
        return scriptLoader.listScripts();
    }

    /**
     * Lists the currently active jobs. It also lists jobs that are running on slaves that the
     * master cannot communicate with (due to an intermittent network issue or because the slave
     * went down).
     *
     * @return the currently active jobs
     */
    private String listJobsCommand() {
        final StringBuilder jobsList = new StringBuilder();
        jobsList.append("JOB_ID\tHOST_SLAVE\tSTATUS    \tCOMMAND\n");
        for (final JobMetadata jobMetadata : jobsMap.values()) {
            if (isHealthy(jobMetadata.getExecutingSlave())) {
                jobsList.append(String.format("%-6d\t%-10s\tRunning   \t%s %s\n",
                        jobMetadata.getJobId(), jobMetadata.getExecutingSlave().getId(),
                        jobMetadata.getScriptName(), jobMetadata.getArgs()));
            } else {
                jobsList.append(String.format("%-6d\t%-10s\tSlave down\t%s %s\n",
                        jobMetadata.getJobId(), jobMetadata.getExecutingSlave().getId(),
                        jobMetadata.getScriptName(), jobMetadata.getArgs()));
            }
        }
        return jobsList.toString();
    }

    /**
     * Lists the statuses of every slave that was registered with the master.
     *
     * @return the statuses of every slave that was registered with the master.
     */
    private String listSlavesCommand() {
        final StringBuilder slavesList = new StringBuilder();
        slavesList.append("SLAVE_ID\tIP             \tPORT\tSTATUS\n");
        for (final Slave slave : instancesMap.values()) {
            if (isHealthy(slave)) {
                slavesList.append(String.format("%-8s\t%-15s\t%d\tHealthy\n", slave.getId(),
                        slave.getIp(), slave.getPort()));
            } else {
                slavesList.append(String.format("%-8s\t%-15s\t%d\tDown\n", slave.getId(),
                        slave.getIp(), slave.getPort()));
            }
        }
        return slavesList.toString();
    }

    /**
     * Processes a job completion by a slave. It prints the completion message and removes the
     * job from its jobs data structure.
     *
     * @param args - The job-id of the job that was completed.
     * @return {@link edu.cmu.cs.akv.ds.proj1.akv.command.Constants#OK} if successful or
     *         an error message otherwise.
     */
    private String processJobCompletedCommand(final String[] args) {
        final Option jobIdOption = new Option("j", "job-id", true, "job-id");
        jobIdOption.setRequired(true);

        final Options options = new Options();
        options.addOption(jobIdOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);
            final int jobId = Integer.parseInt(line.getOptionValue('j'));
            System.out.println(String.format("\n[%d] Completed", jobId));
            System.out.print("master>");
            jobsMap.remove(jobId);
            return OK;
        } catch (final ParseException e) {
            LOG.error("Received exception while processing job completed command", e);
            return BAD_COMMAND;
        } catch (final NumberFormatException e) {
            LOG.error("Received exception while processing job completed command", e);
            return BAD_COMMAND;
        }
    }

    /**
     * Processes a job termination by a slave due to a failure. It prints the failure message and
     * removes the job from its jobs data structure.
     *
     * @param args - The job-id of the job and an error message that describes the failure.
     * @return {@link edu.cmu.cs.akv.ds.proj1.akv.command.Constants#OK} if successful or
     *         an error message otherwise.
     */
    private String processJobFailedCommand(final String[] args) {
        final Option jobIdOption = new Option("j", "job-id", true, "job-id");
        jobIdOption.setRequired(true);

        final Option errorMessageOption = new Option("e", "error-message", true, "error-message");
        errorMessageOption.setRequired(true);

        final Options options = new Options();
        options.addOption(jobIdOption);
        options.addOption(errorMessageOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final int jobId = Integer.parseInt(line.getOptionValue('j'));
            final String errorMessage = line.getOptionValue('e');

            System.out.println(String.format("\n[%d] Failed", jobId));
            System.out.println(errorMessage);
            System.out.print("master>");

            jobsMap.remove(jobId);
            return OK;
        } catch (final ParseException e) {
            LOG.error("Received exception while processing job completed command", e);
            return BAD_COMMAND;
        } catch (final NumberFormatException e) {
            LOG.error("Received exception while processing job completed command", e);
            return BAD_COMMAND;
        }
    }

    /**
     * Forwards a request to kill a job to the appropriate slave running the job. It removes
     * the job from its jobs datastructure if successful.
     *
     * @param args - The job-id of the job.
     * @return {@link edu.cmu.cs.akv.ds.proj1.akv.command.Constants#OK} if successful or
     *         an error message otherwise.
     */
    private String forwardJobKillCommand(final String[] args) {
        final Option jobIdOption = new Option("j", "job-id", true, "job-id");
        jobIdOption.setRequired(true);

        final Options options = new Options();
        options.addOption(jobIdOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final int jobId = Integer.parseInt(line.getOptionValue('j'));

            final JobMetadata jobMetadata = jobsMap.get(jobId);
            if (jobMetadata == null) {
                return String.format("Job with id %d doesn't exist", jobId);
            }

            final Slave slave = jobMetadata.getExecutingSlave();

            try {
                final Socket socket;
                socket = slave.connect();

                final DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                out.writeBytes(String.format("%s -j %d\n", DO_JOB_KILL, jobId));

                final Scanner in = new Scanner(socket.getInputStream());
                final String response = in.nextLine();
                LOG.info(String.format("Received response [%s] for job kill from" +
                        " slave [%s]", response, slave.toString()));
                socket.close();
                if (OK.equals(response)) {
                    System.out.println(String.format("[%d] Killed", jobId));
                    jobsMap.remove(jobId);
                    return null;
                } else {
                    return response;
                }
            } catch (IOException e) {
                LOG.error(String.format("Received exception during job kill from" +
                        " slave [%s]", slave.toString()), e);
                return String.format("Slave [%s] seems to be down", slave.toString());
            }
        } catch (final ParseException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(JOB_KILL, options);
            return null;
        } catch (final NumberFormatException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(JOB_KILL, options);
            return null;
        }
    }

    /**
     * Forwards the request to migrate a job to the appropriate slave running the job. It updates
     * its jobs datastructure if the migration is successful.
     *
     * @param args - The job-id of the job and the destination slave to migrate to.
     * @return {@link edu.cmu.cs.akv.ds.proj1.akv.command.Constants#OK} if successful or
     *         an error message otherwise.
     */
    private String forwardMigrateCommand(final String[] args) {
        final Option jobIdOption = new Option("j", "job-id", true, "job-id");
        jobIdOption.setRequired(true);

        final Option destinationSlaveIdOption =
                new Option("d", "destination-slave-id", true, "destination-slave-id");
        destinationSlaveIdOption.setRequired(true);

        final Options options = new Options();
        options.addOption(jobIdOption);
        options.addOption(destinationSlaveIdOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final int jobId = Integer.parseInt(line.getOptionValue('j'));
            final String destinationSlaveId = line.getOptionValue('d');

            final JobMetadata jobMetadata = jobsMap.get(jobId);
            if (jobMetadata == null) {
                return String.format("Job with id %d doesn't exist", jobId);
            }

            final Slave destinationSlave = instancesMap.get(destinationSlaveId);
            if (destinationSlave == null) {
                return String.format("Slave %s doesn't exist", destinationSlaveId);
            }

            final Slave sourceSlave = jobMetadata.getExecutingSlave();

            if (sourceSlave.getId().equals(destinationSlaveId)) {
                return String.format("Job %d is already running in slave %s",
                        jobId, destinationSlaveId);
            }

            try {
                final Socket socket;
                socket = sourceSlave.connect();

                final DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                out.writeBytes(String.format("%s -j %d -i %s -p %d\n", INITIATE_MIGRATION,
                        jobId, destinationSlave.getIp(), destinationSlave.getPort()));

                final Scanner in = new Scanner(socket.getInputStream());
                final String response = in.nextLine();
                LOG.info(String.format("Received response [%s] for migration from" +
                                " source slave [%s]", response, sourceSlave.toString()));
                socket.close();
                if (OK.equals(response)) {
                    jobsMap.put(jobId,
                            new JobMetadata(
                                    jobMetadata.getJobId(),
                                    destinationSlave,
                                    jobMetadata.getScriptName(),
                                    jobMetadata.getArgs()
                            )
                    );
                    return null;
                } else {
                    return response;
                }
            } catch (IOException e) {
                LOG.error(String.format("Received exception during migration from" +
                                " source slave [%s]", sourceSlave.toString()), e);
                return String.format("Source slave [%s] seems to be down", sourceSlave.toString());
            }
        } catch (final ParseException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(MIGRATE, options);
            return null;
        } catch (final NumberFormatException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(MIGRATE, options);
            return null;
        }
    }

    /**
     * Forwards the request to start a job to the requested slave. It updates
     * its jobs datastructure if the job was successfully started.
     *
     * @param slave - The target slave to run the job.
     * @param scriptName - The name of the script to run.
     * @param jobArgs - The arguments to the job.
     * @return {@link edu.cmu.cs.akv.ds.proj1.akv.command.Constants#OK} if successful or
     *         an error message otherwise.
     */
    private String forwardStartJobCommand(final Slave slave, final String scriptName,
                                          final String jobArgs) {
        final int jobId = jobIdCounter.incrementAndGet();

        try {
            final Socket socket;
            socket = slave.connect();

            final DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            if (!jobArgs.isEmpty()) {
                out.writeBytes(String.format("%s %s %d %s %s %s '%s'\n",
                        START_JOB, "-j", jobId, "-s", scriptName, "-a", jobArgs));
            } else {
                out.writeBytes(String.format("%s %s %d %s %s\n",
                        START_JOB, "-j", jobId, "-s", scriptName));
            }

            final Scanner in = new Scanner(socket.getInputStream());
            final StringBuilder responseBuilder = new StringBuilder();
            while (in.hasNextLine()) {
                responseBuilder.append(in.nextLine());
            }
            final String response = responseBuilder.toString();
            LOG.info(String.format("Received response [%s] while forwarding job to slave [%s]",
                    response, slave.toString()));
            socket.close();
            if (OK.equals(response)) {
                jobsMap.put(jobId, new JobMetadata(jobId, slave, scriptName, jobArgs));
                return "[" + String.valueOf(jobId) + "]";
            } else {
                return response;
            }
        } catch (IOException e) {
            LOG.error(String.format("Received exception while forwarding job to slave [%s]",
                    slave.toString()), e);
            return String.format("Looks like slave %s is down", slave.getId());
        }
    }

    /**
     * Processes command to add a slave to the system.
     *
     * @param args - The slave-id of the slave, the IP address of the host it is running in
     *               and the port it is listening to.
     * @return {@code null} if successful or an error message otherwise.
     */
    private String processSlaveAddCommand(final String[] args) {
        final Option slaveIdOption =
                new Option("s", "slave-id", true, "slave-d of the slave to add");
        slaveIdOption.setRequired(true);

        final Option ipOption = new Option("i", "ip", true,
                "ip address of the host in which the slave is running");
        ipOption.setRequired(true);

        final Option portOption = new Option("p", "port", true,
                "port in the host the slave is listening to");
        portOption.setRequired(true);

        final Options options = new Options();
        options.addOption(slaveIdOption);
        options.addOption(ipOption);
        options.addOption(portOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final String slaveId = line.getOptionValue('s');
            final String ip = line.getOptionValue('i');
            final int port = Integer.parseInt(line.getOptionValue('p'));

            final Slave slave = new Slave(slaveId, ip, port);
            if (!handshake(slave)) {
                System.out.println(String.format("Slave [%s] is either down or its id is" +
                                " incorrect", slave.toString()));
            } else if (instancesMap.putIfAbsent(slaveId, new Slave(slaveId, ip, port)) != null) {
                System.out.println(String.format("Slave id %s is already in use, " +
                        "please use a different id", slaveId));
            }
        } catch (final ParseException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(SLAVE_ADD, options);
        } catch (final NumberFormatException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(SLAVE_ADD, options);
        }

        return null;
    }

    /**
     * Verifies if the slave is alive and listening to the supplied port and makes sure the
     * slave-id submitted in the command and what was told to the slave are the same.
     *
     * @param slave - The slave to handshake with.
     * @return true if the handshake was successful or false otherwise.
     */
    private boolean handshake(final Slave slave) {
        try {
            final Socket socket;
            socket = slave.connect();

            final DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            out.writeBytes(String.format("%s %s %s %s %d\n", HANDSHAKE, "-s",
                    slave.getId(), "-p", port));

            final Scanner in = new Scanner(socket.getInputStream());
            final String response = in.nextLine();
            LOG.info(String.format("Received response [%s] for handshake from slave [%s]",
                    response, slave.toString()));
            socket.close();
            if (OK.equals(response)) {
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            LOG.error(String.format("Received exception during handshake from slave [%s]",
                    slave.toString()), e);
            return false;
        }
    }

    /**
     * Checks if the given slave is healthy by checking its response to a health check
     * command sent over the network.
     *
     * @param slave - The slave whose health should be checked.
     * @return true if the health check was successful or false otherwise
     */
    private boolean isHealthy(final Slave slave) {
        try {
            final Socket socket;
            socket = slave.connect();

            final DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            out.writeBytes(HEALTH_CHECK + "\n");

            final Scanner in = new Scanner(socket.getInputStream());
            final String response = in.nextLine();
            LOG.info(String.format("Received response [%s] for healthcheck from slave [%s]",
                    response, slave.toString()));
            socket.close();
            if (HEALTHY.equals(response)) {
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            LOG.error(String.format("Received exception during healthcheck from slave [%s]",
                    slave.toString()), e);
            return false;
        }
    }
}

package edu.cmu.cs.akv.ds.proj1.akv;

import org.apache.commons.cli.*;

import java.io.IOException;

import static edu.cmu.cs.akv.ds.proj1.akv.command.Constants.START_SLAVE_SCRIPT;

/**
 * The starting point of execution for the slave.
 */
public class SlaveMain {

    /**
     * The starting point of execution for the slave.
     *
     * @param args - The path to the directory containing the scripts and the
     *               port the master should listen to.
     * @throws IOException if there was a problem while performing network or file I/O.
     */
    public static void main(final String[] args) throws IOException {
        final Option slaveIdOption = new Option("i", "slave-id", true, "id of the slave");
        slaveIdOption.setRequired(true);

        final Option scriptsDirOption = new Option("s", "scripts-path", true,
                "path of the directory containing the scripts");
        scriptsDirOption.setRequired(true);

        final Option portOption = new Option("p", "port", true,
                "port the master should listen to");
        portOption.setRequired(true);

        final Options options = new Options();
        options.addOption(slaveIdOption);
        options.addOption(scriptsDirOption);
        options.addOption(portOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final String slaveId = line.getOptionValue('i');
            final String scriptsDir = line.getOptionValue('s');
            final int port = Integer.parseInt(line.getOptionValue('p'));

            new SlaveProcessManager(slaveId, scriptsDir, port).start();
        } catch (final ParseException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(START_SLAVE_SCRIPT, options);
        } catch (final NumberFormatException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(START_SLAVE_SCRIPT, options);
        }
    }
}

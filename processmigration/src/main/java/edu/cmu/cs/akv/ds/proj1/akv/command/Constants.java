package edu.cmu.cs.akv.ds.proj1.akv.command;

/**
 * Constants related to the commands.
 */
public class Constants {
    private Constants() {
    }

    public static final String START_MASTER_SCRIPT = "startmaster.sh";
    public static final String START_SLAVE_SCRIPT = "startslave.sh";

    public static final String HANDSHAKE = "handshake";
    public static final String OK = "ok";

    public static final String CONTEXT_IP = "ip";
    public static final String CONTEXT_PORT = "port";

    public static final String BAD_COMMAND = "Bad command";
    public static final String BAD_ID = "Bad id";

    public static final String LIST_SLAVES = "listslaves";
    public static final String LIST_JOBS = "listjobs";
    public static final String START_JOB = "startjob";
    public static final String ACCEPT_MIGRATION = "acceptmigration";
    public static final String JOB_COMPLETED = "jobcompleted";
    public static final String JOB_FAILED = "jobfailed";
    public static final String JOB_KILL = "jobkill";
    public static final String DO_JOB_KILL = "dojobkill";
    public static final String SLAVE_ADD = "slaveadd";
    public static final String HEALTH_CHECK = "healthcheck";
    public static final String HEALTHY = "healthy";
    public static final String MIGRATE = "migrate";
    public static final String INITIATE_MIGRATION = "initiatemigration";
    public static final String LIST_SCRIPTS = "listscripts";
}

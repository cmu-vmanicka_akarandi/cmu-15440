package edu.cmu.cs.akv.ds.proj1.akv;

import edu.cmu.cs.akv.ds.proj1.akv.command.SlaveCommandsExecutor;
import edu.cmu.cs.akv.ds.proj1.akv.command.SocketCommandsListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * {@code SlaveProcessManager} is the starting point of the slave process manager.
 */
public class SlaveProcessManager {
    private static final Logger LOG = LogManager.getLogger(SlaveProcessManager.class);

    private final String id;
    private final String scriptsDir;
    private final int port;
    private final ObjectMapper mapper;
    private final ExecutorService threadPool;

    /**
     * Creates a new instance of {@link edu.cmu.cs.akv.ds.proj1.akv.SlaveProcessManager}.
     *
     * @param id - The id of this slave.
     * @param scriptsDir - The path to the directory containing the scripts.
     * @param port - The port the slave should listen to.
     */
    public SlaveProcessManager(final String id, final String scriptsDir, final int port) {
        this.id = id;
        this.mapper = new ObjectMapper();
        this.mapper.setVisibilityChecker(mapper.getSerializationConfig()
                .getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));

        this.scriptsDir = scriptsDir;
        this.threadPool = Executors.newCachedThreadPool();
        this.port = port;
    }

    /**
     * Spawns a thread that listens to commands from a network socket.
     *
     * @throws IOException if there was a problem while listening to the socket.
     */
    public void start() throws IOException {
        launchSocketCommandsExecutorAsync();
    }

    /**
     * Launches a thread to listen to commands from a network socket.
     */
    public void launchSocketCommandsExecutorAsync() {
        threadPool.submit(new Runnable() {
            public void run() {
                try {
                    LOG.info(String.format("Listening on port [%d]", port));
                    new SocketCommandsListener(port,
                            new SlaveCommandsExecutor(
                                    id,
                                    scriptsDir,
                                    threadPool,
                                    mapper)
                    ).run();
                } catch (IOException e) {
                    LOG.fatal(String.format("Received exception while trying to listen to port %d",
                            port), e);
                }
            }
        });
    }
}

package edu.cmu.cs.akv.ds.proj1.akv.command;

import edu.cmu.cs.akv.ds.proj1.akv.groovy.ScriptCompilationFailedException;
import edu.cmu.cs.akv.ds.proj1.akv.groovy.ScriptLoader;
import edu.cmu.cs.akv.ds.proj1.akv.groovy.ScriptWrapper;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.cmu.cs.akv.ds.proj1.akv.command.Constants.*;

/**
 * Executes commands submitted to the slave via stdin.
 */
public class SlaveCommandsExecutor implements CommandsExecutor {
    private static final Logger LOG = LogManager.getLogger(SlaveCommandsExecutor.class);

    private final String id;
    private final ScriptLoader scriptLoader;
    private final ExecutorService threadPool;
    private final ObjectMapper mapper;
    private final Object lock;
    private final Map<Integer, ScriptWrapper> scriptsMap;
    private final BlockingQueue<String> messagesToMaster;

    private String masterIp;
    private int masterPort;

    /**
     * Creates a new instance of {@link edu.cmu.cs.akv.ds.proj1.akv.command.SlaveCommandsExecutor}.
     *
     * @param id - The id of the slave.
     * @param scriptsPath - The path of the directory containing the groovy scripts.
     * @param threadPool - The thread pool to run jobs.
     * @param mapper - The {@link org.codehaus.jackson.map.ObjectMapper} to serialize and/or
     *                 deserialize scripts.
     */
    public SlaveCommandsExecutor(final String id,
                                 final String scriptsPath,
                                 final ExecutorService threadPool,
                                 final ObjectMapper mapper) {
        this.id = id;
        this.scriptLoader = new ScriptLoader(scriptsPath, mapper);
        this.threadPool = threadPool;
        this.mapper = mapper;
        this.lock = new Object();
        this.messagesToMaster = new LinkedBlockingQueue<String>();
        this.scriptsMap = new ConcurrentHashMap<Integer, ScriptWrapper>();
    }

    /**
     * {@inheritDoc}
     */
    public String execute(final Map<String, Object> context, final String command) {
        try {
            final String[] split = command.split(" ", 2);
            final String[] args = split.length == 2 ? splitArgs(split[1]) : new String[]{};

            if (HANDSHAKE.equals(split[0])) {
                return processHandshake(context, args);
            } else if (HEALTH_CHECK.equals(split[0])) {
                return HEALTHY;
            } else if (START_JOB.equals(split[0])) {
                return processStartJobCommand(args);
            } else if (INITIATE_MIGRATION.equals(split[0])) {
                return processInitiateMigrateCommand(args);
            } else if (ACCEPT_MIGRATION.equals(split[0])) {
                return processAcceptMigrationCommand(args);
            } else if (DO_JOB_KILL.equals(split[0])) {
                return processJobKillCommand(args);
            } else {
                return BAD_COMMAND;
            }
        } catch (IOException e) {
            LOG.error("Received exception while processing command, ignoring", e);
            return BAD_COMMAND;
        } catch (ScriptCompilationFailedException e) {
            LOG.error("Received exception while processing command, ignoring", e);
            return BAD_COMMAND;
        }
    }

    /**
     * Splits arguments into an array of Strings using space delimiter (except when the arg is
     * surrounded by single quotes).
     *
     * @param unsplit - The flat String containing the space separated arguments.
     * @return the array of String with each element corresponding to an argument.
     */
    private String[] splitArgs(final String unsplit) {
        final List<String> list = new ArrayList<String>();
        final Matcher m = Pattern.compile("([^']\\S*|'.+?')\\s*").matcher(unsplit);
        while (m.find()) {
            String arg = m.group(1);
            if (arg.length() >= 2) {
                if (arg.charAt(0) == '\'') {
                    arg = arg.substring(1);
                }
                if (arg.charAt(arg.length() - 1) == '\'') {
                    arg = arg.substring(0, arg.length() - 1);
                }
            }
            list.add(arg);
        }

        final String[] args = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            args[i] = list.get(i);
        }
        return args;
    }

    /**
     * Spawns a thread that polls the messages queue and publishes messages to the master.
     */
    public void launchMasterPublisherAsync() {
        threadPool.submit(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        final String message = messagesToMaster.take();

                        final Socket socket;
                        try {
                            socket = new Socket(masterIp, masterPort);

                            final DataOutputStream out =
                                    new DataOutputStream(socket.getOutputStream());
                            out.writeBytes(message + "\n");

                            final Scanner scanner = new Scanner(socket.getInputStream());
                            final String response = scanner.nextLine();

                            LOG.info(String.format("Received response from master [%s]",
                                    response));
                            if (!OK.equals(response)) {
                                LOG.error(String.format("Error response from master [%s] " +
                                        "for message [%s]", response, message));
                                messagesToMaster.offer(message);

                                // back-off for 5 seconds
                                try {
                                    Thread.sleep(5000);
                                } catch (final InterruptedException e) {
                                    LOG.error(e);
                                }
                            }
                            socket.close();
                        } catch (IOException e) {
                            LOG.error(String.format("Received exception while trying to publish " +
                                            "message [%s] to master [%s:%d]", message, masterIp,
                                    masterPort), e);
                            messagesToMaster.offer(message);

                            // back-off for 5 seconds
                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e1) {
                                LOG.error(e);
                            }
                        }
                    } catch (final InterruptedException e) {
                        LOG.error("Received exception while polling master queue", e);
                    }
                }
            }
        });
    }

    /**
     * Processes an incoming handshake request from the master and responds after checking
     * if the slave-id is correct.
     *
     * @param context - The IP address of the host in which the master is running.
     * @param args - The slave-id and the port the master is listening to.
     * @return {@link edu.cmu.cs.akv.ds.proj1.akv.command.Constants#OK} if successful or
     *         an error message otherwise.
     */
    private String processHandshake(final Map<String, Object> context, final String[] args) {
        final Option slaveIdOption = new Option("s", "slave-id", true, "slave-id");
        slaveIdOption.setRequired(true);

        final Option portOption = new Option("p", "master-port", true, "master-port");
        portOption.setRequired(true);

        final Options options = new Options();
        options.addOption(slaveIdOption);
        options.addOption(portOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final String slaveId = line.getOptionValue('s');
            final int port = Integer.parseInt(line.getOptionValue('p'));

            if (id.equals(slaveId)) {
                synchronized (lock) {
                    if (masterIp == null) {
                        masterIp = (String) context.get(CONTEXT_IP);
                        masterPort = port;
                        launchMasterPublisherAsync();
                    } else {
                        LOG.error("Received handshake more than once");
                        return BAD_COMMAND;
                    }
                }
                LOG.info(String.format("Discovered master at ip [%s], port [%d]", masterIp,
                        masterPort));
                return OK;
            } else {
                LOG.error(String.format("Received bad id [%s] during handshake, expected [%s]",
                        slaveId, id));
                return BAD_ID;
            }
        } catch (final ParseException e) {
            LOG.error("Received exception during handshake", e);
            return BAD_COMMAND;
        } catch (final NumberFormatException e) {
            LOG.error("Received exception during handshake", e);
            return BAD_COMMAND;
        }
    }

    /**
     * Processes a job kill command by killing the running job (thread).
     *
     * @param args - The job-id of the job to kill.
     * @return {@link edu.cmu.cs.akv.ds.proj1.akv.command.Constants#OK} if successful or
     *         an error message otherwise.
     */
    private String processJobKillCommand(final String[] args) {
        final Option jobIdOption = new Option("j", "job-id", true, "job-id");
        jobIdOption.setRequired(true);

        final Options options = new Options();
        options.addOption(jobIdOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final int jobId = Integer.parseInt(line.getOptionValue('j'));

            final ScriptWrapper scriptWrapper = scriptsMap.get(jobId);
            if (scriptWrapper == null) {
                LOG.error(String.format("Job %d not found", jobId));
                return String.format("Job %d not found", jobId);
            }

            scriptsMap.remove(jobId);
            scriptWrapper.kill();

            return OK;
        } catch (final ParseException e) {
            LOG.error("Received exception while parsing", e);
            return BAD_COMMAND;
        } catch (final NumberFormatException e) {
            LOG.error("Received exception while parsing", e);
            return BAD_COMMAND;
        }
    }

    /**
     * Processes a request to launch a job.
     *
     * @param args - The job-id of the job to launch, the name of the script to launch and
     *               arguments to the job.
     * @return {@link edu.cmu.cs.akv.ds.proj1.akv.command.Constants#OK} if successful or
     *         an error message otherwise.
     */
    private String processStartJobCommand(final String[] args) {
        final Option jobIdOption = new Option("j", "job-id", true, "job-id");
        jobIdOption.setRequired(true);

        final Option scriptNameOption = new Option("s", "script-name", true, "script-name");
        scriptNameOption.setRequired(true);

        final Option argsOption = new Option("a", "args", true, "args");
        argsOption.setRequired(false);

        final Options options = new Options();
        options.addOption(jobIdOption);
        options.addOption(scriptNameOption);
        options.addOption(argsOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final int jobId = Integer.parseInt(line.getOptionValue('j'));
            final String scriptName = line.getOptionValue('s');
            final String jobArgs = line.hasOption('a') ? line.getOptionValue('a') : null;

            try {
                if (jobArgs != null) {
                    launchProcessAsync(jobId, scriptName, jobArgs.split(" "));
                } else {
                    launchProcessAsync(jobId, scriptName, new String[]{});
                }
                return OK;
            } catch (final ScriptCompilationFailedException e) {
                LOG.error(String.format("Received exception while running [%d] [%s]",
                        jobId, scriptName + " " + jobArgs), e);
                return BAD_COMMAND;
            }
        } catch (final ParseException e) {
            LOG.error("Received exception while parsing", e);
            return BAD_COMMAND;
        } catch (final NumberFormatException e) {
            LOG.error("Received exception while parsing", e);
            return BAD_COMMAND;
        }
    }

    /**
     * Processes a request to initiate migration of a job that is running in this slave with
     * the destination slave.
     *
     * @param args - The job-id of the job to launch, the ip address of the host in which the
     *               destination slave is running and the port to which the destination slave is
     *               listening.
     * @return {@link edu.cmu.cs.akv.ds.proj1.akv.command.Constants#OK} if successful or
     *         an error message otherwise.
     */
    private String processInitiateMigrateCommand(final String[] args) {
        final Option jobIdOption = new Option("j", "job-id", true, "job-id");
        jobIdOption.setRequired(true);

        final Option destinationIpOption =
                new Option("i", "destination-ip", true, "destination-ip");
        destinationIpOption.setRequired(true);

        final Option destinationPortOption =
                new Option("p", "destination-port", true, "destination-port");
        destinationPortOption.setRequired(true);

        final Options options = new Options();
        options.addOption(jobIdOption);
        options.addOption(destinationIpOption);
        options.addOption(destinationPortOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final int jobId = Integer.parseInt(line.getOptionValue('j'));
            final String destinationIp = line.getOptionValue('i');
            final int destinationPort = Integer.parseInt(line.getOptionValue('p'));
            final ScriptWrapper scriptWrapper = scriptsMap.get(jobId);
            if (scriptWrapper == null) {
                LOG.error(String.format("Job %d not found", jobId));
                return String.format("Job %d not found", jobId);
            }

            try {
                // connecting socket first to make sure that a connection can be established
                // before suspending the job
                final Socket socket;
                socket = new Socket(destinationIp, destinationPort);

                LOG.info(String.format("Suspending job [%d] to begin migration", jobId));
                scriptsMap.remove(jobId);
                scriptWrapper.suspend();

                LOG.info(String.format("Starting migration of job [%d]", jobId));
                final String objectSer = mapper.writeValueAsString(scriptWrapper.getScript());
                final DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                out.writeBytes(String.format("%s -j %d -s %s -o '%s'\n", ACCEPT_MIGRATION, jobId,
                        scriptWrapper.getScriptName(), objectSer));

                final Scanner in = new Scanner(socket.getInputStream());
                final String response = in.nextLine();
                LOG.info(String.format("Received response [%s] for migration from destination" +
                                " slave [%s:%d]", response, destinationIp, destinationPort));
                socket.close();

                return response;
            } catch (final IOException e) {
                LOG.error("Received exception while processing initiate migration command", e);
                return String.format("Unable to connect to %s:%d", destinationIp, destinationPort);
            }
        } catch (final ParseException e) {
            LOG.error("Received exception while processing initiate migration command", e);
            return BAD_COMMAND;
        } catch (final NumberFormatException e) {
            LOG.error("Received exception while processing initiate migration command", e);
            return BAD_COMMAND;
        }
    }

    /**
     * Processes a request to accept migration of a job that is running in the slave sending the
     * request. It deserialized the job state and resumes running it in this slave.
     *
     * @param args - The job-id of the job to launch, the name of the script and the serialized
     *               job state.
     * @return {@link edu.cmu.cs.akv.ds.proj1.akv.command.Constants#OK} if successful or
     *         an error message otherwise.
     */
    private String processAcceptMigrationCommand(final String[] args)
            throws IOException, ScriptCompilationFailedException {
        final Option jobIdOption = new Option("j", "job-id", true, "job-id of the job to migrate");
        jobIdOption.setRequired(true);

        final Option scriptNameOption = new Option("s", "script-name", true, "script-name");
        scriptNameOption.setRequired(true);

        final Option objectOption = new Option("o", "object", true, "object");
        objectOption.setRequired(true);

        final Options options = new Options();
        options.addOption(jobIdOption);
        options.addOption(scriptNameOption);
        options.addOption(objectOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final int jobId = Integer.parseInt(line.getOptionValue('j'));
            final String scriptName = line.getOptionValue('s');
            final String objectSer = line.getOptionValue('o');

            final JsonNode scriptJson = mapper.readValue(objectSer, JsonNode.class);

            resumeProcessAsync(jobId, scriptName, scriptJson);

            return OK;
        } catch (final ParseException e) {
            LOG.error("Received exception while processing accept migration command", e);
            return BAD_COMMAND;
        }
    }

    /**
     * Resumes execution of a process in a new thread.
     *
     * @param jobId - The job-id of the job to resume.
     * @param scriptName - The name of the script used to spawn the job/process.
     * @param scriptJson - The serialized state of the job in json format.
     * @throws IOException if the script corresponding to the name was not found.
     * @throws ScriptCompilationFailedException if there was a problem compiling the groovy script.
     */
    public void resumeProcessAsync(final int jobId, final String scriptName,
                                   final JsonNode scriptJson)
            throws IOException, ScriptCompilationFailedException {
        final ScriptWrapper scriptWrapper = scriptLoader.recreateInstance(scriptName, scriptJson);
        final Future<?> f = threadPool.submit(new Runnable() {
            public void run() {
                String errorMessage = null;
                try {
                    errorMessage = scriptWrapper.execute();
                } finally {
                    if (!scriptWrapper.isSuspended()) {
                        if (errorMessage == null) {
                            messagesToMaster.offer(
                                    String.format("%s -j %s", JOB_COMPLETED, jobId));
                        } else {
                            messagesToMaster.offer(
                                    String.format("%s -j %s -e '%s'", JOB_FAILED,
                                            jobId, errorMessage));
                        }
                        scriptsMap.remove(jobId);
                    }
                }
            }
        });
        scriptWrapper.setExecutingFuture(f);
        scriptsMap.put(jobId, scriptWrapper);
    }

    /**
     * Launches a new process/job in a separate thread.
     *
     * @param jobId - The job-id of the job to launch.
     * @param scriptName - The name of the script to use to launch the job.
     * @param scriptArgs - The arguments to the script to use to launch the job.
     * @return a {@link edu.cmu.cs.akv.ds.proj1.akv.groovy.ScriptWrapper} representing the
     *         launched job.
     * @throws ScriptCompilationFailedException if there was a problem compiling the script.
     */
    public ScriptWrapper launchProcessAsync(final int jobId,
                                            final String scriptName,
                                            final String[] scriptArgs)
            throws ScriptCompilationFailedException {
        final ScriptWrapper scriptWrapper = scriptLoader.createInstance(scriptName);
        final Future<?> f = threadPool.submit(new Runnable() {
            public void run() {
                String errorMessage = null;
                try {
                    errorMessage = scriptWrapper.init(scriptArgs);
                    if (errorMessage == null) {
                        errorMessage = scriptWrapper.execute();
                    }
                } finally {
                    if (!scriptWrapper.isSuspended()) {
                        if (errorMessage == null) {
                            messagesToMaster.offer(
                                    String.format("%s -j %s", JOB_COMPLETED, jobId));
                        } else {
                            messagesToMaster.offer(
                                    String.format("%s -j %s -e '%s'", JOB_FAILED,
                                            jobId, errorMessage));
                        }
                        scriptsMap.remove(jobId);
                    }
                }
            }
        });
        scriptWrapper.setExecutingFuture(f);
        scriptsMap.put(jobId, scriptWrapper);

        return scriptWrapper;
    }
}

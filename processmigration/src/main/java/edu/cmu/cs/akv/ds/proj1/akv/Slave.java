package edu.cmu.cs.akv.ds.proj1.akv;

import java.io.IOException;
import java.net.Socket;

/**
 * Encapsulates metadata about a slave.
 */
public class Slave {
    private final String id;
    private final String ip;
    private final int port;

    /**
     * Creates a new {@link Slave} instance.
     *
     * @param id - The id of the slave.
     * @param ip - The IP address of the host in which this slave is running.
     * @param port - The port the slave should listen to.
     */
    public Slave(final String id, final String ip, final int port) {
        this.id = id;
        this.ip = ip;
        this.port = port;
    }

    /**
     * Returns the id of the slave.
     *
     * @return the id of the slave.
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the port the slave is listening to.
     *
     * @return the port the slave is listening to.
     */
    public int getPort() {
        return port;
    }

    /**
     * Returns the IP address of the host in which the slave is running.
     *
     * @return the IP address of the host in which the slave is running.
     */
    public String getIp() {
        return ip;
    }

    /**
     * Creates a socket connection with this slave.
     *
     * @return the {@link java.net.Socket} connection to this slave.
     * @throws IOException if there was a problem creating the {@link java.net.Socket}.
     */
    public Socket connect() throws IOException {
        return new Socket(ip, port);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Slave{" +
                "id='" + id + '\'' +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                '}';
    }
}

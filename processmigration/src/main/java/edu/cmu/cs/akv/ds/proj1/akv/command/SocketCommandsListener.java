package edu.cmu.cs.akv.ds.proj1.akv.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static edu.cmu.cs.akv.ds.proj1.akv.command.Constants.CONTEXT_IP;
import static edu.cmu.cs.akv.ds.proj1.akv.command.Constants.CONTEXT_PORT;

/**
 * Has methods to listen to commands from a network socket.
 */
public class SocketCommandsListener implements Runnable {
    private static final Logger LOG = LogManager.getLogger(SocketCommandsListener.class);

    private final ServerSocket commandsSocket;
    private final CommandsExecutor executor;

    /**
     * Creates a new instance of a
     * {@link edu.cmu.cs.akv.ds.proj1.akv.command.SocketCommandsListener}.
     *
     * @param port - The port to listen to.
     * @param executor - The {@link edu.cmu.cs.akv.ds.proj1.akv.command.CommandsExecutor} that
     *                   should be used to execute the commands.
     * @throws IOException if there was a problem listening to the socket.
     */
    public SocketCommandsListener(final int port, final CommandsExecutor executor)
            throws IOException {
        this.commandsSocket = new ServerSocket(port);
        this.executor = executor;
    }

    /**
     * Listens to the socket for commands and has them executed by the
     * {@link edu.cmu.cs.akv.ds.proj1.akv.command.CommandsExecutor}.
     */
    public void run() {
        while (true) {
            try {
                final Socket connectionSocket = commandsSocket.accept();
                final Scanner socketScanner = new Scanner(connectionSocket.getInputStream());
                final String command = socketScanner.nextLine();
                LOG.info(String.format("Received command from socket [%s]", command));

                final Map<String, Object> context = new HashMap<String, Object>();
                context.put(CONTEXT_IP,
                        ((InetSocketAddress) connectionSocket.getRemoteSocketAddress())
                                .getAddress().getHostAddress());
                context.put(CONTEXT_PORT,
                        ((InetSocketAddress) connectionSocket.getRemoteSocketAddress()).getPort());

                final String response = executor.execute(context, command);
                if (response != null) {
                    final DataOutputStream out =
                            new DataOutputStream(connectionSocket.getOutputStream());
                    out.writeBytes(response + "\n");
                }
                connectionSocket.close();
            } catch (final IOException e) {
                LOG.fatal("Unable to listen to port", e);
                break;
            }
        }
    }
}

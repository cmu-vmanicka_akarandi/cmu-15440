package edu.cmu.cs.akv.ds.proj1.akv;

import org.apache.commons.cli.*;

import java.io.IOException;

import static edu.cmu.cs.akv.ds.proj1.akv.command.Constants.START_MASTER_SCRIPT;

/**
 * The starting point of execution for the master.
 */
public class MasterMain {

    /**
     * The starting point of exeution for the master.
     *
     * @param args - The path to the directory containing the scripts and the
     *               port the master should listen to.
     * @throws IOException if there was a problem while performing network or file I/O.
     */
    public static void main(final String[] args) throws IOException {
        final Option scriptsPathOption = new Option("s", "scripts-path", true,
                "path of the directory containing the scripts");
        scriptsPathOption.setRequired(true);

        final Option portOption = new Option("p", "port", true,
                "port the master should listen to");
        portOption.setRequired(true);

        final Options options = new Options();
        options.addOption(scriptsPathOption);
        options.addOption(portOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            final String scriptsPath = line.getOptionValue('s');
            final int port = Integer.parseInt(line.getOptionValue('p'));

            new MasterProcessManager(scriptsPath, port).start();
        } catch (final ParseException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(START_MASTER_SCRIPT, options);
        } catch (final NumberFormatException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(START_MASTER_SCRIPT, options);
        }
    }
}

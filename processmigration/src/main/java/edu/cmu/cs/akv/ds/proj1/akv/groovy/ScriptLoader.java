package edu.cmu.cs.akv.ds.proj1.akv.groovy;

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * {@code ScriptLoader} provides methods to load a groovy script from file into memory, compile
 * it into bytecode and create instances of it.
 */
public class ScriptLoader {
    private final GroovyClassLoader groovyClassLoader;
    private final String scriptsPath;
    private final ObjectMapper mapper;

    /**
     * Creates a new instance of {@link edu.cmu.cs.akv.ds.proj1.akv.groovy.ScriptLoader}.
     *
     * @param scriptsPath - The path of the directory containing the scripts.
     * @param mapper - The {@link org.codehaus.jackson.map.ObjectMapper} to use to deserialize
     *                 serialized object state.
     */
    public ScriptLoader(final String scriptsPath, final ObjectMapper mapper) {
        this.groovyClassLoader = new GroovyClassLoader();
        this.scriptsPath = scriptsPath;
        this.mapper = mapper;
    }

    /**
     * Lists the valid scripts that can be instantiated and run.
     *
     * @return the valid scripts that can be instantiated and run.
     */
    public String listScripts() {
        final File scriptsDir = new File(scriptsPath);
        if (scriptsDir.exists() && scriptsDir.isDirectory()) {
            final StringBuilder scriptNames = new StringBuilder();
            final File[] files = scriptsDir.listFiles();
            if (files != null) {
                for (final File scriptFile : files) {
                    final String scriptFullName = scriptFile.getName();
                    if (scriptFullName.endsWith(".groovy")) {
                        scriptNames.append(
                                scriptFullName.substring(0, scriptFullName.length() - 7));
                        scriptNames.append('\n');
                    }
                }
            }
            return scriptNames.toString();
        } else {
            return "Bad scripts directory";
        }
    }

    /**
     * Reads the script from file and compiles it into bytecode.
     *
     * @param scriptName - The name of the script.
     * @return the bytecode
     * @throws ScriptCompilationFailedException if there was a failure while compiling the script.
     */
    public Class<?> loadBytecode(final String scriptName)
            throws ScriptCompilationFailedException {
        final File scriptFile = new File(scriptsPath, scriptName + ".groovy");
        if (!scriptFile.exists() || !scriptFile.isFile()) {
            throw new ScriptCompilationFailedException(
                    String.format("%s not found in %s", scriptName, scriptsPath));
        } else {
            try {
                final String scriptSrc = transformScript(readFile(scriptFile));
                final Class<?> scriptBytecode = groovyClassLoader.parseClass(scriptSrc);
                ScriptValidator.validate(scriptBytecode);
                return scriptBytecode;
            } catch (final IOException e) {
                throw new ScriptCompilationFailedException(
                        String.format("%s not found in %s", scriptName, scriptsPath));
            }
        }
    }

    /**
     * Creates a new instance of a script by reading it from disk and compiling it.
     *
     * @param scriptName - The name of the script to create a new instance of.
     * @return a {@link edu.cmu.cs.akv.ds.proj1.akv.groovy.ScriptWrapper} representing the
     *         instance.
     * @throws ScriptCompilationFailedException if there was a failure while compiling the script.
     */
    public ScriptWrapper createInstance(final String scriptName)
            throws ScriptCompilationFailedException {
        try {
            final Class<?> scriptBytecode = loadBytecode(scriptName);
            final GroovyObject compiledScript = (GroovyObject) scriptBytecode.newInstance();
            return new ScriptWrapper(scriptName, compiledScript);
        } catch (final InstantiationException e) {
            throw new ScriptCompilationFailedException(e);
        } catch (final IllegalAccessException e) {
            throw new ScriptCompilationFailedException(e);
        }
    }

    /**
     * Recreates an existing instance of a script from its serialized state.
     *
     * @param scriptName - The name of the script that was used to create the instance.
     * @param scriptJson - The serialized json representation of the instance's state.
     * @return a {@link edu.cmu.cs.akv.ds.proj1.akv.groovy.ScriptWrapper} representing the
     *         instance.
     * @throws IOException if there was a failure while reading the serialized state.
     * @throws ScriptCompilationFailedException if there a failure while compiling the script.
     */
    public ScriptWrapper recreateInstance(final String scriptName, final JsonNode scriptJson)
            throws IOException, ScriptCompilationFailedException {
        final Class<?> scriptBytecode = loadBytecode(scriptName);
        final GroovyObject compiledScript =
                (GroovyObject) mapper.readValue(scriptJson, scriptBytecode);
        return new ScriptWrapper(scriptName, compiledScript);
    }

    /**
     * Reads the contents of a file into a String.
     *
     * @param file - The file to read from.
     * @return the contents of the file
     * @throws IOException if there was a problem reading the file.
     */
    private static String readFile(final File file) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
        return new String(encoded, Charset.defaultCharset());
    }

    /**
     * Adds the {@link groovy.transform.ThreadInterrupt} annotation to the beginning of the script
     * to make sure the script can be interrupted at anytime.
     *
     * @param scriptSrc - The source code of the script.
     * @return the source code of the script with the {@link groovy.transform.ThreadInterrupt}
     *         annotation added to the beginning.
     */
    private static String transformScript(final String scriptSrc) {
        return "@groovy.transform.ThreadInterrupt\n" + scriptSrc;
    }
}

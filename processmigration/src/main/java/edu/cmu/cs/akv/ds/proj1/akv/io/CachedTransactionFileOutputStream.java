package edu.cmu.cs.akv.ds.proj1.akv.io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

/**
 * {@code CachedTransactionFileOutputStream} is an {@link java.io.OutputStream} that can be
 * suspended and migrated to a different process with access to the same file system.
 * It caches the file descriptor so that it needs to be reopened only across migrations.
 */
public class CachedTransactionFileOutputStream extends OutputStream {
    private static final Logger LOG =
            LogManager.getLogger(CachedTransactionFileOutputStream.class);

    private String path;
    private RandomAccessFile raf;
    private long pos;

    /**
     * Creates a new instance of
     * {@link edu.cmu.cs.akv.ds.proj1.akv.io.CachedTransactionFileOutputStream}
     */
    public CachedTransactionFileOutputStream() {
        this.path = null;
        this.raf = null;
        this.pos = 0;
    }

    /**
     * Sets the path of the file.
     *
     * @param path - The path of the file to set.
     * @return this {@link edu.cmu.cs.akv.ds.proj1.akv.io.CachedTransactionFileOutputStream}
     *         instance to allow for chaining.
     */
    public CachedTransactionFileOutputStream forPath(final String path) {
        if (this.path == null) {
            this.path = path;
        } else {
            throw new IllegalStateException("forFile should be invoked only once");
        }
        this.raf = null;
        this.pos = 0;

        return this;
    }

    /**
     * Writes a single byte to the file. It initializes the stream if it is null and seeks
     * to the position that was last written to.
     *
     * @param b - The byte to write to the file.
     * @throws IOException if there was a problem writing to the file.
     */
    @Override
    public void write(int b) throws IOException {
        synchronized(this) {
            if (raf == null) {
                raf = new RandomAccessFile(new File(path), "rw");
                raf.seek(pos);
            }
            raf.write(b);
            pos++;
        }
    }

    /**
     * Clears the stream to prepare it for migration. This method is expected to be invoked
     * by the user before migration.
     */
    public void clearCache() {
        synchronized (this) {
            try {
                raf.close();
            } catch (final IOException e) {
                LOG.error("Received exception while trying to close RandomAccessFile", e);
            } finally {
                raf = null;
            }
        }
    }
}

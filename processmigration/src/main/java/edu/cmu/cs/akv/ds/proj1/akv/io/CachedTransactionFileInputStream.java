package edu.cmu.cs.akv.ds.proj1.akv.io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

/**
 * {@code CachedTransactionFileInputStream} is an {@link java.io.InputStream} that can be
 * suspended and migrated to a different process with access to the same file system.
 * It caches the file descriptor so that it needs to be reopened only across migrations.
 */
public class CachedTransactionFileInputStream extends InputStream {
    private static final Logger LOG = LogManager.getLogger(CachedTransactionFileInputStream.class);

    private String path;
    private RandomAccessFile raf;
    private long pos;

    /**
     * Creates a new instance of
     * {@link edu.cmu.cs.akv.ds.proj1.akv.io.CachedTransactionFileInputStream}
     */
    public CachedTransactionFileInputStream() {
        this.path = null;
        this.raf = null;
        this.pos = 0;
    }

    /**
     * Sets the path of the file.
     *
     * @param path - The path of the file to set.
     * @return this {@link edu.cmu.cs.akv.ds.proj1.akv.io.CachedTransactionFileInputStream}
     *         instance to allow for chaining.
     */
    public CachedTransactionFileInputStream forPath(final String path) {
        if (this.path == null) {
            this.path = path;
        } else {
            throw new IllegalStateException("forFile should be invoked only once");
        }
        this.raf = null;
        this.pos = 0;

        return this;
    }

    /**
     * Reads a single byte from the file. It initializes the stream if it is null and seeks
     * to the position that was last read.
     *
     * @return the byte read.
     * @throws IOException if there was a problem reading from the file.
     */
    @Override
    public int read() throws IOException {
        synchronized (this) {
            if (raf == null) {
                raf = new RandomAccessFile(new File(path), "r");
                raf.seek(pos);
            }
            int read = raf.read();
            pos++;
            return read;
        }
    }

    /**
     * Clears the stream to prepare it for migration. This method is expected to be invoked
     * by the user before migration.
     */
    public void clearCache() {
        synchronized (this) {
            try {
                raf.close();
            } catch (final IOException e) {
                LOG.error("Received exception while trying to close RandomAccessFile", e);
            } finally {
                raf = null;
            }
        }
    }
}

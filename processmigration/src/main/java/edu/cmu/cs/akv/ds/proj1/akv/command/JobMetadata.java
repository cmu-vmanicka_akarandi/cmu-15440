package edu.cmu.cs.akv.ds.proj1.akv.command;

import edu.cmu.cs.akv.ds.proj1.akv.Slave;

/**
 * Metadata about a single job.
 */
public class JobMetadata {
    private final int jobId;
    private final Slave executingSlave;
    private final String scriptName;
    private final String args;

    /**
     * Creates a new instance of a {@link edu.cmu.cs.akv.ds.proj1.akv.command.JobMetadata}.
     *
     * @param jobId - The job-id of the job.
     * @param executingSlave - The {@link edu.cmu.cs.akv.ds.proj1.akv.Slave} executing the job.
     * @param scriptName - The name of the script that was used to create the job.
     * @param args - The arguments to the job.
     */
    public JobMetadata(final int jobId, final Slave executingSlave, final String scriptName,
                       final String args) {
        this.jobId = jobId;
        this.executingSlave = executingSlave;
        this.scriptName = scriptName;
        this.args = args;
    }

    /**
     * Returns the name of the script.
     *
     * @return the name of the script.
     */
    public String getScriptName() {
        return scriptName;
    }

    /**
     * Returns the job-id of the script.
     *
     * @return the job-id of the script.
     */
    public int getJobId() {
        return jobId;
    }

    /**
     * Returns the {@link edu.cmu.cs.akv.ds.proj1.akv.Slave} running the job.
     *
     * @return the {@link edu.cmu.cs.akv.ds.proj1.akv.Slave} running the job.
     */
    public Slave getExecutingSlave() {
        return executingSlave;
    }

    /**
     * Returns the arguments to the job.
     *
     * @return the arguments to the job.
     */
    public String getArgs() {
        return args;
    }
}

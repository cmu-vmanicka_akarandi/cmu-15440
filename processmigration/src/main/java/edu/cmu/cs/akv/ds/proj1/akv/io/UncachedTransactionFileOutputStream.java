package edu.cmu.cs.akv.ds.proj1.akv.io;

import java.io.*;

/**
 * {@code UncachedTransactionFileOutputStream} is an {@link java.io.OutputStream} that can be
 * suspended and migrated to a different process with access to the same file system.
 * It reopens the file descriptor every time it writes a byte to the file.
 */
public class UncachedTransactionFileOutputStream extends OutputStream {
    private String path;
    private RandomAccessFile raf;
    private long pos;

    /**
     * Creates a new instance of
     * {@link edu.cmu.cs.akv.ds.proj1.akv.io.UncachedTransactionFileOutputStream}
     */
    public UncachedTransactionFileOutputStream() {
        this.path = null;
        this.raf = null;
        this.pos = 0;
    }

    /**
     * Sets the path of the file.
     *
     * @param path - The path of the file to set.
     * @return this {@link edu.cmu.cs.akv.ds.proj1.akv.io.UncachedTransactionFileOutputStream}
     *         instance to allow for chaining.
     */
    public UncachedTransactionFileOutputStream forPath(final String path) {
        if (this.path == null) {
            this.path = path;
        } else {
            throw new IllegalStateException("forFile should be invoked only once");
        }
        this.raf = null;
        this.pos = 0;

        return this;
    }

    /**
     * Writes a single byte to the file. It reopens the stream to the file.
     * @param b - The byte to write to the file.
     * @throws IOException if there was a problem writing to the file.
     */
    @Override
    public void write(int b) throws IOException {
        synchronized(this) {
            raf = new RandomAccessFile(new File(path), "rw");
            raf.seek(pos);
            raf.write(b);
            raf.close();
            raf = null;
            pos++;
        }
    }
}

package edu.cmu.cs.akv.ds.proj1.akv.command;

import java.util.Map;

/**
 * Executes commands read from any source (stdin or sockets)
 */
public interface CommandsExecutor {
    /**
     * Executes a single command from any source (stdin or sockets)
     *
     * @param context - Metadata about the command
     * @param command - The command to execute
     * @return the response to the command that is to be published to stdout or to the socket
     */
    String execute(Map<String, Object> context, String command);
}

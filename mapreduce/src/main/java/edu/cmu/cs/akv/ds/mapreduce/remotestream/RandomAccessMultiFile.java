package edu.cmu.cs.akv.ds.mapreduce.remotestream;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 * {@code RandomAccessMultiFile} provides an abstraction of a collection of multiple files into a single MultiFile which can be randomly accessed.
 */
public class RandomAccessMultiFile {
    private final List<RandomAccessFile> raFiles;
    private final List<File> files;
    private final int nFiles;
    private final int totalSize;
    private final List<Integer> filePrefixSizes;

    private int p;
    private int f;
    private RandomAccessFile file;

    /**
     * The {@code RandomAccessMultiFile} constructor which takes the underlying list of files as input.
     *
     * @param files The underlying list of files.
     * @throws IOException if the read fails.
     */
    public RandomAccessMultiFile(final List<File> files) throws IOException {
        this.nFiles = files.size();
        this.files = files;
        this.filePrefixSizes = new ArrayList<>(nFiles +1);
        this.filePrefixSizes.add(0, 0);
        this.raFiles = new ArrayList<>(nFiles);
        for(int i = 0; i < nFiles; ++i) {
            final File cFile= files.get(i);
            filePrefixSizes.add(i + 1, filePrefixSizes.get(i) + (int) cFile.length());
            this.raFiles.add(i, new RandomAccessFile(cFile, "r"));
        }

        totalSize=filePrefixSizes.get(nFiles);
        file=this.raFiles.get(0); file.seek(0);
        p=0; f=0;
    }

    /**
     * Return the length of the {@code RandomAccessMultiFile}.
     *
     * @return the length of the {@code RandomAccessMultiFile}.
     */
    public int length() {
        return filePrefixSizes.get(nFiles);
    }

    /**
     * Return the file which contains a given offset.
     * @param p - The offset.
     * @return the file which contains the offset.
     */
    public int getF(final int p)
    {
        if(p >= totalSize) {
            return -1;
        }

        for(int i = 0; i <= nFiles; ++i) {
            if (filePrefixSizes.get(i + 1) > p) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Seeks to the location p within the {@code RandomAccessMultiFile}.
     * @param p - The location within the {@code RandomAccessMultiFile} to which we want to seek.
     * @throws IOException if the seek fails.
     */
    public void seek(final int p) throws IOException {
        if(p >= totalSize) {
            this.p = p;
        } else {
            this.p = p;
            f = getF(p);
            file = raFiles.get(f);
            file.seek(p - filePrefixSizes.get(f));
        }
    }

    /**
     * Read the next byte from the {@code RandomAccessMultiFile} or -1 if there is no such byte.
     *
     * @return the next byte in the {@code RandomAccessMultiFile}.
     * @throws IOException if the read fails.
     */
    public int read() throws IOException {
        if(p >= totalSize) {
            return -1;
        }

        if(p < filePrefixSizes.get(f + 1)) {
            final int ret=file.read();
            ++p;
            return ret;
        } else {
            ++f;
            file = raFiles.get(f);
            file.seek(0);
            int ret = file.read();
            ++p;
            return ret;
        }
    }

    /**
     * Return a list of FileChunks corresponding to a contiguous range in the {@code RandomAccessMultiFile}.
     * @param start the starting offset of the range in the {@code RandomAccessMultiFile}
     * @param end the ending offset of the range in the {@code RandomAccessMultiFile}
     * @return the list of FileChunks corresponding to a contiguous range in the {@code RandomAccessMultiFile}.
     * @throws IOException if the call to the FileChunk constructor fails.
     */
    public List<FileChunk> getChunksForRange(int start, int end) throws IOException {
        final List<FileChunk> ret = new ArrayList<FileChunk>();

        start = Math.max(start,0);
        end = Math.min(end,totalSize-1);

        if(start > end) {
            return ret;
        }

        final int fstart= getF(start);
        final int fend= getF(end);

        if(fstart == fend) {
            ret.add(new FileChunk(files.get(fstart).getAbsolutePath(),
                    start - filePrefixSizes.get(fstart),
                    end - filePrefixSizes.get(fstart)));
        } else {
            ret.add(new FileChunk(files.get(fstart).getAbsolutePath(),
                    start - filePrefixSizes.get(fstart),
                    filePrefixSizes.get(fstart + 1) - 1 - filePrefixSizes.get(fstart)));

            for(int f = fstart + 1; f < fend; ++f) {
                ret.add(new FileChunk(files.get(f).getAbsolutePath(), 0,
                        filePrefixSizes.get(f + 1) - 1 - filePrefixSizes.get(f)));
            }

            ret.add(new FileChunk(files.get(fend).getAbsolutePath(), 0,
                    end - filePrefixSizes.get(fend)));
        }
        return ret;
    }
}
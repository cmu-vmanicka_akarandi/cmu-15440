package edu.cmu.cs.akv.ds.mapreduce.remotestream;

import edu.cmu.cs.akv.ds.mapreduce.Pair;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * {@code RemoteInputStream} can be used to stream bytes over the network
 * using Java's RMI framework.
 */
public interface RemoteInputStream extends Remote {
    /**
     * Reads the requested number of bytes into an array and specifies the
     * number of read bytes.
     *
     * @param size - The number of bytes to read.
     * @return a Pair containing the number of bytes read (or -1 if no more bytes
     *         are available) and a byte array containing the read bytes.
     * @throws IOException if there was an issue reading the bytes.
     */
    Pair<Integer, byte[]> read(int size) throws IOException;

    /**
     * Closes the stream. No more bytes can be read from this stream.
     *
     * @throws IOException if there was a problem closing the stream.
     */
    void close() throws IOException;
}

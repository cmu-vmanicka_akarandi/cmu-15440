package edu.cmu.cs.akv.ds.mapreduce;

import java.io.Serializable;

/**
 * {@code Mapper} interface describes the methods that a framework user must implement
 * to define the mapping logic for his use case/
 *
 * @param <IK> - The type of input key.
 * @param <IV> - The type of input value.
 * @param <OK> - The type of output key.
 * @param <OV> - The type of output value.
 */
public interface Mapper<IK extends Serializable, IV extends Serializable,
        OK extends Comparable & Serializable, OV extends Serializable>
{
    /**
     * Maps the given input key and input value and updates the mapped map to reflect
     * the results.
     *
     * @param key - The input key to be mapped.
     * @param value - The input value to be mapped.
     * @param mapped - The mapped map to be updated to reflect the results.
     * @throws Exception if there was an issue performing the mapping.
     */
    void map(IK key, IV value, KeyMap<OK, OV> mapped) throws Exception;
}

package edu.cmu.cs.akv.ds.mapreduce.remotestream;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;

/**
 * A {@code FileChunk} represents a contiguous part of a file.
 */

public class FileChunk implements Serializable {
    private final String filePath;
    private final int start;
    private final int end;

    /**
     * Initializes the FileChunk with the file path, the starting byte offset and the ending byte offset,
     *
     * @param filePath - path of the file.
     * @param start - starting byte offset (inclusive).
     * @param end - ending byte offset (inclusive).
     */
    public FileChunk(final String filePath, final int start, final int end)
    {
        this.filePath = filePath;
        this.start = Math.max(start, 0);
        this.end = Math.min(end, (int) new File(filePath).length() - 1);
    }

    /**
     * Returns the file path.
     *
     * @return the file path
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Returns the starting offset.
     *
     * @return the starting offset
     */
    public Integer getStart() {
        return start;
    }

    /**
     * Returns the ending offset.
     *
     * @return the ending offset
     */
    public Integer getEnd() {
        return end;
    }
}

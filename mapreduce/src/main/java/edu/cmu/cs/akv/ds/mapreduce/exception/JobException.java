package edu.cmu.cs.akv.ds.mapreduce.exception;

/**
 * {@code JobException} represents an {@link java.lang.Exception} that
 * will recur even if the job is retried again.
 */
public class JobException extends Exception {

    /**
     * {@inheritDoc}
     */
    public JobException(final String message) {
        super(message);
    }

    /**
     * {@inheritDoc}
     */
    public JobException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * {@inheritDoc}
     */
    public JobException(final Throwable cause) {
        super(cause);
    }
}

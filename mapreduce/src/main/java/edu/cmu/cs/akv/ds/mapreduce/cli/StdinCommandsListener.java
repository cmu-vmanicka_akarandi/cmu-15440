package edu.cmu.cs.akv.ds.mapreduce.cli;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Has methods to listen to commands from the standard input.
 */
public class StdinCommandsListener implements Runnable {
    private static final Logger LOG = LogManager.getLogger(StdinCommandsListener.class);

    private final CommandsExecutor executor;
    private final Scanner stdinScanner;

    /**
     * Creates a new instance of the
     * {@link edu.cmu.cs.akv.ds.mapreduce.cli.StdinCommandsListener}
     *
     * @param executor - The executor that will run the commands.
     */
    public StdinCommandsListener(final CommandsExecutor executor) {
        this.executor = executor;
        stdinScanner = new Scanner(System.in);
    }

    /**
     * Prints prompt to accept commands from the standard input, forward commands for processing
     * to the {@link edu.cmu.cs.akv.ds.mapreduce.cli.CommandsExecutor} and prints response
     * to the standard output.
     */
    public void run() {
        try {
            while (true) {
                System.out.print(">> ");
                final String command = stdinScanner.nextLine();
                LOG.info(String.format("Received command from stdin [%s]", command));
                final String response = executor.execute(command);
                if (response != null) {
                    LOG.info(String.format("Received response from executor [%s]", response));
                    System.out.println(response);
                }
            }
        } catch (final NoSuchElementException e) {
            LOG.error("Exception while waiting for line from stdin", e);
            System.out.println("Bye!");
        }
    }
}

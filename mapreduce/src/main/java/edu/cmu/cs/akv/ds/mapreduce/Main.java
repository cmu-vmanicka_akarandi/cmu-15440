package edu.cmu.cs.akv.ds.mapreduce;

import edu.cmu.cs.akv.ds.mapreduce.cli.StdinCommandsListener;
import edu.cmu.cs.akv.ds.mapreduce.exception.ConfigFormatException;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

import static edu.cmu.cs.akv.ds.mapreduce.Constants.START_NODE_SCRIPT;

/**
 * {@code Main} class is the starting point of execution of a node instance.
 */
public class Main {
    private static final Logger LOG = LogManager.getLogger(Main.class);

    private Main() {
    }

    /**
     * The starting point of execution of a node instance.
     *
     * @param args - The arguments to the program.
     */
    public static void main(final String[] args) {
        final Option configFileOption = new Option("n", "node-config-file", true,
                "path of the nodes configuration file");
        configFileOption.setRequired(true);

        final Option ipOption = new Option("i", "ip", true,
                "the ip address of the host in which the node is running");
        ipOption.setRequired(true);

        final Option portOption = new Option("p", "port", true,
                "the port this node can use to communicate with other nodes");
        portOption.setRequired(true);

        final Options options = new Options();
        options.addOption(configFileOption);
        options.addOption(ipOption);
        options.addOption(portOption);

        final CommandLineParser parser = new BasicParser();
        try {
            final CommandLine line = parser.parse(options, args);

            // Read the configuration files path, ip and port
            final String nodesConfigFilePath = line.getOptionValue('n');
            final String ip = line.getOptionValue('i');
            final int port = Integer.parseInt(line.getOptionValue('p'));

            final Node thisNode = new Node(ip, port);
            final Pair<List<Node>, File> p =
                    ConfigUtils.parseNodeConfigs(new File(nodesConfigFilePath));
            final List<Node> nodes = p.getFirst();
            final File workingDir = p.getSecond();

            // Bind the task tracker of the node to its registry service
            // so that other nodes can look it up for communication.
            final TaskTracker taskTrackerStub =
                    (TaskTracker) UnicastRemoteObject.exportObject(
                            new TaskTrackerImpl(workingDir), 0);
            final Registry registry = LocateRegistry.createRegistry(port);
            registry.rebind("taskTracker", taskTrackerStub);

            // Spawn a listener to the stdin to print the prompt and accept commands
            // from the system administrator.
            new StdinCommandsListener(new MapReduceCommandsExecutor(new JobTracker(),
                    thisNode, nodes, workingDir)).run();
        } catch (final ParseException | NumberFormatException e) {
            LOG.error("Exception while parsing command line args", e);
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(START_NODE_SCRIPT, options);
        } catch (final ConfigFormatException e) {
            LOG.error("Exception while parsing configurations", e);
            System.out.println("Exception while trying to parse configurations, '" +
                    e.getMessage() + "'");
        } catch (final IOException e) {
            LOG.error("Exception while reading config file", e);
            System.out.println("Exception while attempting to read nodes config file, '" +
                    e.getMessage() + "'");
        }
    }
}

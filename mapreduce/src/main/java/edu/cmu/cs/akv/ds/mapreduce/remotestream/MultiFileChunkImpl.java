package edu.cmu.cs.akv.ds.mapreduce.remotestream;

import edu.cmu.cs.akv.ds.mapreduce.Pair;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 * The {@code MultiFileChunkImpl} class implements the MultiFileChunk interface.
 */

public class MultiFileChunkImpl extends UnicastRemoteObject implements MultiFileChunk {
    private final List<FileChunk> fileChunks;
    private final int numFileChunks;
    private final int totalSize;

    private int p;
    private int c;
    private RandomAccessFile file;

    /**
     * The MultiFileChunkImpl constructor which takes the underlying list of FileChunks as parameter.
     *
     * @param fileChunks The underlying list of FileChunks.
     * @throws RemoteException if there is a problem in sending information over the network.
     */
    public MultiFileChunkImpl(final List<FileChunk> fileChunks) throws RemoteException {
        super();
        this.numFileChunks = fileChunks.size();
        this.fileChunks = fileChunks;
        int totalSize = 0;
        for(int i=0; i < numFileChunks; ++i) {
            totalSize += (fileChunks.get(i).getEnd() - fileChunks.get(i).getStart() + 1);
        }
        this.totalSize = totalSize;
        this.p = 0;
        this.c = 0;
    }

    /**
     * {@inheritDoc}
     */
    public int length() {
        return totalSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pair<Integer, byte[]> read(int size) throws IOException {
        if (size < 0) {
            throw new IndexOutOfBoundsException();
        } else if (size == 0) {
            return new Pair<>(0, new byte[1]);
        }

        int c = read();
        if (c == -1) {
            return new Pair<>(-1, new byte[1]);
        }

        byte[] b = new byte[size];
        b[0] = (byte) c;

        int i = 1;
        try {
            for (; i < size ; i++) {
                c = read();
                if (c == -1) {
                    break;
                }
                b[i] = (byte)c;
            }
        } catch (IOException ee) {
            // Ignore
        }
        return new Pair<>(i, b);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FileChunk> getFileChunks() throws RemoteException {
        return fileChunks;
    }

    /**
     * {@inheritDoc}
     */
    public int read() throws IOException {
        if(p == 0) {
            if(c < numFileChunks) {
                file = new RandomAccessFile(fileChunks.get(c).getFilePath(), "r");
                file.seek(fileChunks.get(c).getStart());
            }
            else {
                return -1;
            }
        }

        final int ret = file.read();
        ++p;

        if(p == fileChunks.get(c).getEnd() - fileChunks.get(c).getStart() + 1) {
            p = 0;
            ++c;
        }

        return ret;
    }
}

package edu.cmu.cs.akv.ds.mapreduce;

import java.io.Serializable;
import java.util.Objects;

/**
 * {@code Node} represents a node in the map reduce cluster.
 */
public class Node implements Serializable {
    private final String nodeId;
    private final String ip;
    private final Integer port;

    /**
     * Creates an instance of a node with the given parameters.
     *
     * @param ip - The IP address of the host in which the node is running.
     * @param port - The port to which the node's registry service is listening.
     */
    public Node(final String ip, final Integer port) {
        this.ip = ip;
        this.port = port;
        this.nodeId = ip + '-' + Integer.toString(port);
    }

    /**
     * Returns the IP address of the node.
     *
     * @return the IP address of the node.
     */
    public String getIp() {
        return ip;
    }

    /**
     * Returns the port to which the node's registry service is listening.
     *
     * @return the port to which the node's registry service is listening.
     */
    public Integer getPort() {
        return port;
    }

    /**
     * Returns a unique identifier for the node.
     *
     * @return a unique identifier for the node.
     */
    public String getNodeId() {
        return nodeId;
    }

    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        return Objects.hash(23, ip, port);
    }

    /**
     * {@inheritDoc}
     */
    public boolean equals(final Object other) {
        if (other == null) {
            return false;
        } else if (other instanceof Node) {
            final Node otherNode = (Node) other;
            return Objects.equals(ip, otherNode.ip) && Objects.equals(port, otherNode.port);
        } else {
            return false;
        }
    }
}

package edu.cmu.cs.akv.ds.mapreduce.cli;

/**
 * Executes commands read from any source (stdin or sockets)
 */
public interface CommandsExecutor {
    /**
     * Executes a single command from any source (stdin or sockets)
     *
     * @param command - The command to execute
     * @return the response to the command that is to be published to stdout or to the socket
     */
    String execute(String command);
}

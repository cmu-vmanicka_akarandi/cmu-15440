package edu.cmu.cs.akv.ds.mapreduce.remotestream;

import edu.cmu.cs.akv.ds.mapreduce.Pair;

import java.io.*;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * {@code RemoteFileInputStream} can be used to stream bytes from a local file over the
 * netwok.
 */
public class RemoteFileInputStream extends UnicastRemoteObject implements RemoteInputStream {
    private final FileInputStream fis;
    private final File file;

    /**
     * Initializes the instance with the name of the file to stream.
     *
     * @param name - The name of the file to stream.
     * @throws FileNotFoundException if the file with the given name could not be found.
     * @throws RemoteException if there was a problem opening a file stream.
     */
    public RemoteFileInputStream(String name) throws FileNotFoundException, RemoteException {
        super();
        this.file = new File(name);
        this.fis = new FileInputStream(name);
    }

    /**
     * Initializes the instance with the file to stream.
     *
     * @param file - The file to stream over the network.
     * @throws FileNotFoundException if the provided file could not be found.
     * @throws RemoteException if there was a problem opening a file stream.
     */
    public RemoteFileInputStream(File file) throws FileNotFoundException, RemoteException {
        super();
        this.file = file;
        this.fis = new FileInputStream(file);
    }

    /**
     * Reads the requested number of bytes from the file and returns it as a byte
     * array along with the number of bytes read.
     *
     * @param size - The number of bytes to read.
     * @return a Pair containing the number of bytes read and an array containing the
     *         read bytes.
     * @throws IOException if there was a problem reading the bytes or if there was
     *                     a problem transferring the bytes over the network.
     */
    @Override
    public Pair<Integer, byte[]> read(int size) throws IOException {
        final byte[] buff = new byte[size];
        int read = fis.read(buff);
        return new Pair<>(read, buff);
    }

    /**
     * Closes the file stream. Once this method is invoked, no more bytes can be read
     * from the file.
     *
     * @throws IOException if there was an issue closing the stream.
     */
    @Override
    public void close() throws IOException {
        this.fis.close();
        this.file.delete();
    }
}

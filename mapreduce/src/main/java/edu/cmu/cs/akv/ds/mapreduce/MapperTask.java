package edu.cmu.cs.akv.ds.mapreduce;

import edu.cmu.cs.akv.ds.mapreduce.MapReduceJob.Status;
import edu.cmu.cs.akv.ds.mapreduce.exception.JobException;
import edu.cmu.cs.akv.ds.mapreduce.parser.InputFormat;
import edu.cmu.cs.akv.ds.mapreduce.remotestream.FileChunk;
import edu.cmu.cs.akv.ds.mapreduce.remotestream.RemoteInputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * {@code MapperTask} represents a single mapper task that parses the input files,
 * provides the inputs to the map method and writes the output to a file.
 *
 * @param <IK> - The input key type for the mapper.
 * @param <IV> - The input value type for the mapper.
 * @param <OK> - The output key type for the mapper.
 * @param <OV> - The output value type for the mapper.
 */
public class MapperTask<IK extends Serializable, IV extends Serializable,
        OK extends Comparable & Serializable, OV extends Serializable>
{
    private static final int BUFFER_SIZE = 1000;
    private static final AtomicInteger TASK_ID_PROVIDER = new AtomicInteger(0);

    private final Node masterNode;
    private final String taskId;
    private final Node slaveNode;
    private final Class<InputFormat<IK, IV>> inputFormatClazz;
    private final Class<Mapper<IK, IV, OK, OV>> mapperClazz;
    private final List<FileChunk> inputChunks;
    private final Object lock;
    private final File workingDir;

    private Status status;
    private String failureReason;

    /**
     * Creates an instance of a {@code MapperTask} using the provided
     * configuration.
     *
     * @param masterNode - The node in which the associated job was created.
     * @param jobId - A unique identifier for the associated job.
     * @param slaveNode - The slave in which the task is executed.
     * @param inputFormatClazz - The type of the
     *                           {@link edu.cmu.cs.akv.ds.mapreduce.parser.InputFormat}.
     * @param mapperClazz - The type of the
     *                      {@link edu.cmu.cs.akv.ds.mapreduce.Mapper}.
     * @param inputChunks - The input chunks to be mapped.
     * @param workingDir - The directory where files to store intermediate results can be
     *                     stored.
     */
    public MapperTask(final Node masterNode, final String jobId, final Node slaveNode,
                      final Class<InputFormat<IK, IV>> inputFormatClazz,
                      final Class<Mapper<IK, IV, OK, OV>> mapperClazz,
                      final List<FileChunk> inputChunks, final File workingDir)
    {
        this.masterNode = masterNode;
        this.taskId = jobId + '-' + Integer.toString(TASK_ID_PROVIDER.incrementAndGet());
        this.slaveNode = slaveNode;
        this.inputFormatClazz = inputFormatClazz;
        this.mapperClazz = mapperClazz;
        this.inputChunks = inputChunks;
        this.lock = new Object();
        this.status = Status.NOT_STARTED;
        this.failureReason = null;
        this.workingDir = workingDir;
    }

    /**
     * Launches a mapper task that reads all the inputs, maps them using the map method
     * and writes the output to a file.
     *
     * @return a {@link java.io.File} that contains the output.
     * @throws RemoteException if there was an issue delegating the task to a slave.
     * @throws JobException if there was a non-retriable exception.
     */
    public File launch() throws RemoteException, JobException {
        // Update status to in progress.
        synchronized (lock) {
            if (status != Status.NOT_STARTED) {
                throw new IllegalStateException(
                        "Cannot launch when status is " + status.toString());
            } else {
                status = Status.INPROGRESS;
            }
        }

        try {
            // Look up the slave's task tracker from its registry.
            final Registry slaveRegistry =
                    LocateRegistry.getRegistry(slaveNode.getIp(), slaveNode.getPort());
            final TaskTracker slaveLiaison;
            try {
                slaveLiaison = (TaskTracker) slaveRegistry.lookup("taskTracker");
            } catch (final NotBoundException e) {
                throw new RemoteException("Slave has not bound its task tracker", e);
            }
            // Delegate the mapping task to the slave.
            final RemoteInputStream slaveInputStream;
            slaveInputStream = slaveLiaison.acceptMapperTask(masterNode, taskId, inputFormatClazz,
                    mapperClazz, inputChunks);

            // Stream the mapped output from the slave to a local file.
            try {
                final File outputFile = new File(workingDir, taskId + "-streamedmapperoutput");
                writeToFile(outputFile, slaveInputStream);

                synchronized (lock) {
                    if (status == Status.INPROGRESS) {
                        status = Status.COMPLETED;
                    }
                }

                return outputFile;
            } catch (final IOException e) {
                throw new RemoteException("Exception while reading mapper output", e);
            }
        } catch (final RemoteException | JobException e) {
            // Mark the status as failed if an exception occurred.
            synchronized (lock) {
                if (status == Status.INPROGRESS) {
                    failureReason = e.getMessage();
                    status = Status.FAILED;
                }
            }
            throw e;
        }
    }

    /**
     * Returns the status of the task
     *
     * @return the status of the task.
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Returns the reason why the task failed if it failed.
     * @return the reason why the task failed if it failed.
     */
    public String getFailureReason() {
        return failureReason;
    }

    /**
     * Returns the unique identifier for the task.
     * @return the unique identifier for the task.
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * Returns the slave node that executed or is to execute this task.
     * @return the slave node that executed or is to execute this task.
     */
    public Node getSlaveNode() {
        return slaveNode;
    }

    /**
     * Streams the file from the remote master to the local disk.
     *
     * @param file - The file to stream from the remote master.
     * @param slaveInputStream - The stream to the file in the remote master.
     * @throws IOException if there was a problem reading from the remote stream.
     */
    private void writeToFile(final File file, final RemoteInputStream slaveInputStream)
            throws IOException
    {
        final FileOutputStream fos = new FileOutputStream(file);
        Pair<Integer, byte[]> readPair;

        while((readPair = slaveInputStream.read(BUFFER_SIZE)).getFirst() != -1) {
            fos.write(readPair.getSecond(), 0, readPair.getFirst());
        }

        fos.close();
        slaveInputStream.close();
    }
}

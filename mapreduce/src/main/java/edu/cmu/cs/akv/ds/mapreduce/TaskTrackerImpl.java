package edu.cmu.cs.akv.ds.mapreduce;

import edu.cmu.cs.akv.ds.mapreduce.exception.JobException;
import edu.cmu.cs.akv.ds.mapreduce.parser.InputFormat;
import edu.cmu.cs.akv.ds.mapreduce.remotestream.*;

import java.io.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.Scanner;

/**
 * {@code TaskTrackerImpl} tracks all the tasks running in a node and communicates
 * the output to remote nodes.
 */
public class TaskTrackerImpl implements TaskTracker {
    private static final int BUFFER_SIZE = 1000;

    private final File workingDir;

    /**
     * Creates an instance of a task tracker.
     *
     * @param workingDir - The directory where the tasks can store temporary files.
     */
    public TaskTrackerImpl(final File workingDir) {
        this.workingDir = workingDir;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <IK extends Serializable, IV extends Serializable, OK extends Comparable & Serializable,
            OV extends Serializable> RemoteInputStream acceptMapperTask(
            final Node masterNode, final String taskId,
            final Class<InputFormat<IK, IV>> inputFormatClazz,
            final Class<Mapper<IK, IV, OK, OV>> mapperClazz,
            final List<FileChunk> inputChunks) throws RemoteException, JobException
    {
        System.out.println("Accepting mapper task " + taskId);

        // Get a remote reference to the input
        final Registry registry =
                LocateRegistry.getRegistry(masterNode.getIp(), masterNode.getPort());
        final TaskTracker remoteTaskTracker;
        try {
            remoteTaskTracker = (TaskTracker) registry.lookup("taskTracker");
        } catch (final NotBoundException e) {
            throw new RemoteException("Unable to lookup taskTracker", e);
        }
        final MultiFileChunk remoteChunkStream = remoteTaskTracker.getMultiFileChunk(inputChunks);

        // Stream the input to a local file for fast access
        final File inputFile = new File(workingDir, taskId + '-' + "streamedmapperinput");
        try {
            writeToFile(inputFile, remoteChunkStream);
        } catch (final IOException e) {
            throw new RemoteException("Exception while accessing RemoteInputStream", e);
        }

        // Initialize parser to parse the input file
        final InputFormat<IK, IV> inputFormat;
        try {
            inputFormat = inputFormatClazz.newInstance();
        } catch (final IllegalAccessException | InstantiationException e) {
            throw new JobException(e);
        }
        try {
            inputFormat.initialize(new FileInputStream(inputFile));
        } catch (final FileNotFoundException e) {
            throw new RemoteException("Exception while accessing copied local file", e);
        }

        // Initialize the job's mapper class and map the input
        final Mapper<IK, IV, OK, OV> mapper;
        try {
            mapper = mapperClazz.newInstance();
        } catch (final IllegalAccessException | InstantiationException e) {
            throw new JobException(e);
        }
        final KeyMap<OK, OV> mapped = new KeyMap<>();
        while(inputFormat.hasNext()) {
            Pair<IK, IV> p = inputFormat.next();
            try {
                mapper.map(p.getFirst(), p.getSecond(), mapped);
            } catch (final Exception e) {
                throw new JobException(e);
            }
        }
        inputFile.delete();

        // Write the mapper output to a local file
        final File outputFile = new File(workingDir, taskId + '-' + "mapperoutput");
        try {
            sortAndWrite(outputFile, mapped);
        } catch (final IOException e) {
            throw new JobException(e);
        }

        // Create remote reference for the file and return
        try {
            return new RemoteFileInputStream(outputFile);
        } catch (FileNotFoundException e) {
            throw new RemoteException("Unable to find file that was just created", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <K extends Comparable & Serializable,
            V extends Serializable> RemoteInputStream acceptReducerTask(
            final Node masterNode, final String taskId, final Class<Reducer<K, V>> reducerClazz,
            final String inputFilePath) throws IOException, JobException {
        System.out.println("Accepting reducer task " + taskId);

        // Get a remote reference to the input
        final Registry registry =
                LocateRegistry.getRegistry(masterNode.getIp(), masterNode.getPort());
        final TaskTracker remoteTaskTracker;
        try {
            remoteTaskTracker = (TaskTracker) registry.lookup("taskTracker");
        } catch (final NotBoundException e) {
            throw new RemoteException("Unable to lookup taskTracker", e);
        }
        final RemoteInputStream remoteInputStream =
                remoteTaskTracker.getFileInputStream(inputFilePath);

        // Stream the input to a local file for fast access
        final File inputFile = new File(workingDir, taskId + '-' + "streamedreducerinput");
        try {
            writeToFile(inputFile, remoteInputStream);
        } catch (final IOException e) {
            throw new RemoteException("Exception while accessing RemoteInputStream", e);
        }

        // Initialize the job's reducer class and reduce the input
        final Reducer<K, V> reducer;
        try {
            reducer = reducerClazz.newInstance();
        } catch (final IllegalAccessException | InstantiationException e) {
            throw new JobException(e);
        }
        final Scanner inputScanner = new Scanner(inputFile);
        final KeyMap<K, V> reduced = new KeyMap<>();
        while(inputScanner.hasNextLine()) {
            final String line = inputScanner.nextLine();
            if (line.startsWith("$")) {
                continue;
            } else {
                Pair<K, List<V>> p = Utils.pairFromLine(line);
                try {
                    reducer.reduce(p.getFirst(), p.getSecond(), reduced);
                } catch (final Exception e) {
                    throw new JobException(e);
                }
            }
        }
        inputScanner.close();
        inputFile.delete();

        // Write the reducer output to a local file
        final File outputFile = new File(workingDir, taskId + '-' + "reduceroutput");
        try {
            writeToFile(outputFile, reduced);
        } catch (final IOException e) {
            throw new JobException(e);
        }

        // Create remote reference for the file and return
        try {
            return new RemoteFileInputStream(outputFile);
        } catch (FileNotFoundException e) {
            throw new RemoteException("Unable to find file that was just created", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MultiFileChunk getMultiFileChunk(List<FileChunk> chunks) throws RemoteException {
        return new MultiFileChunkImpl(chunks);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RemoteInputStream getFileInputStream(String filePath) throws IOException {
        return new RemoteFileInputStream(filePath);
    }

    /**
     * Streams bytes from the multi file remote stream and store it locally.
     *
     * @param file - The file to store the contents in locally.
     * @param multiFileChunk - The multi file remote stream to read bytes from.
     * @throws IOException if there was a problem reading from the stream or writing to the file.
     */
    private void writeToFile(final File file, final MultiFileChunk multiFileChunk)
            throws IOException
    {
        final FileOutputStream fos = new FileOutputStream(file);
        Pair<Integer, byte[]> readPair;

        while((readPair = multiFileChunk.read(BUFFER_SIZE)).getFirst() != -1) {
            fos.write(readPair.getSecond(), 0, readPair.getFirst());
        }

        fos.close();
    }

    /**
     * Sorts the given {@link edu.cmu.cs.akv.ds.mapreduce.KeyMap} and writes the output
     * to a file.
     *
     * @param file - The file to write the serialized output to.
     * @param mapped - The mapped output to sort and write.
     * @param <K> - The type of key.
     * @param <V> - The type of value.
     * @throws IOException if there was a problem writing to the file.
     */
    private <K extends Comparable & Serializable, V extends Serializable> void sortAndWrite(
            final File file, final KeyMap<K, V> mapped) throws IOException {
        final List<Pair<K, List<V>>> sortedList = mapped.getSortedList();
        final PrintWriter pw = new PrintWriter(file);

        for (final Pair<K, List<V>> p : sortedList) {
            pw.println(Utils.pairToLine(p));
        }

        pw.close();
    }

    /**
     * Reads bytes from a remote node using the remote stream and stores the contents
     * locally.
     *
     * @param file - The file in which the contents are to be stored locally.
     * @param slaveInputStream - The remote stream to read from.
     * @throws IOException if there was an exception reading from the stream or writing to the
     *                     file.
     */
    private void writeToFile(final File file, final RemoteInputStream slaveInputStream)
            throws IOException
    {
        final FileOutputStream fos = new FileOutputStream(file);
        Pair<Integer, byte[]> readPair;

        while((readPair = slaveInputStream.read(BUFFER_SIZE)).getFirst() != -1) {
            fos.write(readPair.getSecond(), 0, readPair.getFirst());
        }

        fos.close();
        slaveInputStream.close();
    }

    /**
     * Writes the reduced output to a file.
     *
     * @param file - The file to write the reduced output to.
     * @param reduced - The reduced output.
     * @param <K> - The type of key.
     * @param <V> - The type of value.
     * @throws IOException if there was a problem writing to a file.
     */
    private <K extends Comparable & Serializable, V extends Serializable> void writeToFile(
            final File file, final KeyMap<K, V> reduced) throws IOException
    {
        final PrintWriter pw = new PrintWriter(file);
        for (final Pair<K, List<V>> p : reduced.getAsList()) {
            pw.println(Utils.pairToSimpleLine(p));
        }
        pw.close();
    }

}

package edu.cmu.cs.akv.ds.mapreduce.parser;

import edu.cmu.cs.akv.ds.mapreduce.Pair;

import java.io.InputStream;
import java.util.Scanner;

/**
 * {@code CharInputFormat} splits a bytes in the input stream to a stream of
 * single byte ASCII characters.
 */
public class CharInputFormat implements InputFormat<Integer, String> {
    private Scanner scanner;
    private Integer id;

    /**
     * Initializes the {@code CharInputFormat} with the provided input stream.
     *
     * @param inputStream - The inputStream to split into a stream of characters.
     */
    @Override
    public void initialize(final InputStream inputStream) {
        id = 1;
        this.scanner = new Scanner(inputStream);
        this.scanner.useDelimiter("");
    }

    /**
     * Returns true if there is a character yet to be read or false otherwise.
     *
     * @return true if there is a character yet to be read or false otherwise.
     */
    @Override
    public Boolean hasNext() {
        return scanner.hasNext();
    }

    /**
     * Returns the next character in the stream and a unique id to that character.
     *
     * @return the next character in the stream and a unique id to that character.
     */
    @Override
    public Pair<Integer, String> next() {
        return new Pair<>(id++, Character.toString(scanner.next().charAt(0)));
    }
}

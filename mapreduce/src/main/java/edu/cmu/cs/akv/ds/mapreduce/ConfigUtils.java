package edu.cmu.cs.akv.ds.mapreduce;

import edu.cmu.cs.akv.ds.mapreduce.exception.ConfigFormatException;
import edu.cmu.cs.akv.ds.mapreduce.parser.InputFormat;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * {@code ConfigUtils} contains a set of utility methods useful for parsing
 * the configuration files provided to the MapReduce framework. This class
 * cannot be instantiated.
 */
public class ConfigUtils {
    private ConfigUtils() {
    }

    /**
     * Parses the configuration file that contains information about the number
     * of nodes and the information about individual nodes.
     *
     * @param nodesConfigFile - The configuration file to parse.
     * @return a Pair containing the list of {@link edu.cmu.cs.akv.ds.mapreduce.Node}s
     *         and the path of the working directory as parsed from the configuration
     *         file.
     * @throws IOException if there was a problem reading the file.
     * @throws ConfigFormatException if the configuration file had entries with bad format.
     */
    public static Pair<List<Node>, File> parseNodeConfigs(final File nodesConfigFile)
            throws IOException, ConfigFormatException {
        final Properties properties = new Properties();
        properties.load(new FileInputStream(nodesConfigFile));

        try {
            // Read the number of nodes.
            final int noOfNodes = Integer.parseInt(properties.getProperty("numberOfNodes"));
            final List<Node> nodes = new ArrayList<>(noOfNodes);
            for (int i = 0; i < noOfNodes; i++) {
                // Read the node's ip address and port where its registry service is listening.
                final String nodeConfig = properties.getProperty("node" + (i + 1));
                if (nodeConfig == null) {
                    throw new ConfigFormatException("Key node" + (i + 1) + " missing!");
                }

                final String[] splitNodeConfig = nodeConfig.split(",");
                if (splitNodeConfig.length != 2) {
                    throw new ConfigFormatException(
                            "Entry for node" + (i + 1) + " must be of the format <ip>,<port>");
                }

                final int port = Integer.parseInt(splitNodeConfig[1]);
                nodes.add(new Node(splitNodeConfig[0], port));
            }

            // Read the path of the working directory where the tasks can create
            // intermediate files.
            final String workingDirPath = properties.getProperty("workingDir");
            if (workingDirPath == null) {
                throw new ConfigFormatException("Key workingDir missing");
            }

            final File workingDir = new File(workingDirPath);
            if (!workingDir.mkdirs() && !workingDir.exists() && !workingDir.isDirectory()) {
                throw new ConfigFormatException(
                        "workingDir either doesn't exist or is not a directory");
            }

            return new Pair<>(nodes, workingDir);
        } catch (final NumberFormatException e) {
            throw new ConfigFormatException(
                    "Either numberOfNodes or one of the nodes' port wasn't an integer", e);
        }
    }

    /**
     * Parses the configuration file that contains information about the job and its
     * configurable parameters.
     *
     * @param masterNode - The node where the job was issued.
     * @param slaveNodes - The slave nodes that can serve as mappers/reducers for the job.
     * @param jobConfigFile - The job configuration file to parse.
     * @param workingDir - The directory where the tasks can create intermediate files.
     * @return a {@link edu.cmu.cs.akv.ds.mapreduce.MapReduceJob} from the information
     *         provided in the configuration file.
     * @throws IOException if there was a problem reading the configuration file.
     * @throws ConfigFormatException if configuration file was incorrectly formatted.
     */
    public static MapReduceJob parseJobConfig(final Node masterNode,
                                              final List<Node> slaveNodes,
                                              final File jobConfigFile,
                                              final File workingDir)
            throws IOException, ConfigFormatException {
        final Properties properties = new Properties();
        properties.load(new FileInputStream(jobConfigFile));

        // Read the mapper class name associated with this job.
        final String mapperClazzName = properties.getProperty("mapperClass");
        if (mapperClazzName == null) {
            throw new ConfigFormatException("Key mapperClass is missing");
        }

        final Class<Mapper<?, ?, ?, ?>> mapperClazz;
        try {
            mapperClazz = (Class<Mapper<?, ?, ?, ?>>) Class.forName(mapperClazzName);
        } catch (final ClassNotFoundException e) {
            throw new ConfigFormatException("Unable to load class " + mapperClazzName);
        } catch (final ClassCastException e) {
            throw new ConfigFormatException(mapperClazzName +
                    " must implement the Mapper interface");
        }

        // Read the reducer class name associated with this job.
        final String reducerClazzName = properties.getProperty("reducerClass");
        if (reducerClazzName == null) {
            throw new ConfigFormatException("Key reducerClass is missing");
        }

        final Class<Reducer<?, ?>> reducerClazz;
        try {
            reducerClazz = (Class<Reducer<?, ?>>) Class.forName(reducerClazzName);
        } catch (final ClassNotFoundException e) {
            throw new ConfigFormatException("Unable to load class " + reducerClazzName);
        } catch (final ClassCastException e) {
            throw new ConfigFormatException(reducerClazzName +
                    " must implement the Reducer interface");
        }

        // Read the input format parser to be used to parse the input files provided
        // to the job.
        final String inputFormatClazzName = properties.getProperty("inputFormatClass");
        if (inputFormatClazzName == null) {
            throw new ConfigFormatException("Key inputFormatClass is missing");
        }

        final Class<InputFormat<?, ?>> inputFormatClazz;
        try {
            inputFormatClazz = (Class<InputFormat<?, ?>>) Class.forName(inputFormatClazzName);
        } catch (final ClassNotFoundException e) {
            throw new ConfigFormatException("Unable to load class " + inputFormatClazzName);
        } catch (final ClassCastException e) {
            throw new ConfigFormatException(inputFormatClazzName +
                    " must implement the InputFormat interface");
        }

        // Read the paths to the list of input files to the job.
        final String inputFilesPath = properties.getProperty("inputFiles");
        if (inputFilesPath == null) {
            throw new ConfigFormatException("Key inputFiles is missing");
        }

        final String[] inputFilesPathArray = inputFilesPath.split(",");
        final List<File> inputFiles = new ArrayList<>(inputFilesPathArray.length);
        for (final String inputFilePath : inputFilesPathArray) {
            final File inputFile = new File(inputFilePath);
            if (inputFile.exists() && !inputFile.isDirectory()) {
                inputFiles.add(inputFile);
            } else {
                throw new ConfigFormatException(inputFile.getAbsolutePath() +
                        " either does not exist or is a directory!");
            }
        }

        // Reads the minSplitSize parameter that controls the minimum number of bytes
        // allocated to a single mapper for processing.
        final int minSplitSize;
        try {
            minSplitSize = Integer.parseInt(properties.getProperty("minSplitSize"));
        } catch (final NumberFormatException e) {
            throw new ConfigFormatException("minSplitSize is missing or is not an int");
        }

        // The maximum number of retries during the event that a mapper/reducer fails.
        final int maxRetries;
        try {
            maxRetries = Integer.parseInt(properties.getProperty("maxRetries"));
        } catch (final NumberFormatException e) {
            throw new ConfigFormatException("maxRetries is missing or is not an int");
        }

        // Create the job instance from the parsed information.
        final MapReduceJob job;
        try {
            job = new MapReduceJob(masterNode, slaveNodes, mapperClazz, reducerClazz,
                    inputFormatClazz, inputFiles, minSplitSize, maxRetries, workingDir);
        } catch (final ClassCastException e) {
            throw new ConfigFormatException("The mapper and reducer are not " +
                    "compatible with each other");
        }

        return job;
    }
}

package edu.cmu.cs.akv.ds.mapreduce;

import edu.cmu.cs.akv.ds.mapreduce.exception.JobException;
import edu.cmu.cs.akv.ds.mapreduce.parser.InputFormat;
import edu.cmu.cs.akv.ds.mapreduce.remotestream.FileChunk;
import edu.cmu.cs.akv.ds.mapreduce.remotestream.MultiFileChunk;
import edu.cmu.cs.akv.ds.mapreduce.remotestream.RemoteInputStream;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * {@code TaskTracker} interface defines the methods to implement by a task tracker
 * that will be used by other nodes to communicate with the given node.
 */
public interface TaskTracker extends Remote {
    /**
     * Accepts a request from the remote node to execute a mapper task defined by the
     * given parameters.
     *
     * @param masterNode - The node where the associated job was spawned.
     * @param taskId - The unique identifier of the task.
     * @param inputFormatClazz - The input format type.
     * @param mapperClazz - The mapper type.
     * @param inputChunks - The input chunks to the mapper.
     * @param <IK> - The type of input key.
     * @param <IV> - The type of input value.
     * @param <OK> - The type of output key.
     * @param <OV> - The type of output value.
     * @return a remote stream to read the output from a local file.
     * @throws RemoteException if there was a problem communicating with the caller.
     * @throws JobException if there was a non-retriable failure to the task.
     */
    <IK extends Serializable, IV extends Serializable, OK extends Comparable & Serializable,
            OV extends Serializable> RemoteInputStream acceptMapperTask(
            Node masterNode, String taskId, Class<InputFormat<IK, IV>> inputFormatClazz,
            Class<Mapper<IK, IV, OK, OV>> mapperClazz,
            List<FileChunk> inputChunks
    ) throws RemoteException, JobException;

    /**
     * Accepts a request from the remote node to execute a reducer task defined by the
     * given parameters.
     *
     * @param masterNode - The node where the associated job was spawned.
     * @param taskId - The unique identifier of the task.
     * @param reducerClazz - The type of the reducer.
     * @param inputFilePath - The path to the input file.
     * @param <K> - The type of key.
     * @param <V> - The type of value.
     * @return a remote stream to read the output from a local file.
     * @throws RemoteException if there was a problem communicating with the caller.
     * @throws JobException if there was a non-retriable failure to the task.
     */
    <K extends Comparable & Serializable,
            V extends Serializable> RemoteInputStream acceptReducerTask(
            Node masterNode, String taskId, Class<Reducer<K, V>> reducerClazz,
            final String inputFilePath
    ) throws IOException, JobException;

    /**
     * Given a list of chunks, returns a multi file chunk remote stream to read the chunks
     * from this node.
     *
     * @param chunks - The chunks to read from this node.
     * @return a remote stream to read the chunks from this node.
     * @throws RemoteException if there was a problem communicating with the caller.
     */
    MultiFileChunk getMultiFileChunk(List<FileChunk> chunks) throws RemoteException;

    /**
     * Given a file path, returns a remote stream to read the file from this node.
     *
     * @param filePath - The path of the file to create an remote stream for.
     * @return a remote stream to read the file from this node.
     * @throws IOException if there was a creating a remote stream for the file.
     */
    RemoteInputStream getFileInputStream(final String filePath) throws IOException;
}

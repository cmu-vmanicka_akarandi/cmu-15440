package edu.cmu.cs.akv.ds.mapreduce;

import java.util.ArrayList;
import java.util.List;

/**
 * {@code JobTracker} is responsible for launching and tracking the status of each job.
 */
public class JobTracker {
    private final List<MapReduceJob<?, ?, ?, ?>> jobs;

    /**
     * Creates a new instance of a {@code JobTracker}.
     */
    public JobTracker() {
        this.jobs = new ArrayList<>();
    }

    /**
     * Launches the supplied job in a new thread.
     *
     * @param job - The job to launch.
     */
    public void launchJobAsync(final MapReduceJob<?, ?, ?, ?> job) {
        job.startAsync();
        jobs.add(job);
    }

    /**
     * Prints the statuses of every job launched.
     */
    public void printStatus() {
        for (final MapReduceJob job : jobs) {
            job.printStatus();
        }
    }
}

package edu.cmu.cs.akv.ds.mapreduce;

import java.io.Serializable;

/**
 * {@code Pair} represents a pair of two different types.
 *
 * @param <T1> - The first type.
 * @param <T2> - The second type.
 */
public class Pair<T1, T2> implements Serializable {
    private final T1 t1;
    private final T2 t2;

    /**
     * Creates a new pair of the first and second parameters.
     *
     * @param t1 - The first parameter.
     * @param t2 - The second parameter.
     */
    public Pair(final T1 t1, final T2 t2) {
        this.t1=t1;
        this.t2=t2;
    }

    /**
     * Returns the first element in the pair.
     *
     * @return the first element in the pair.
     */
    public T1 getFirst() {
        return t1;
    }

    /**
     * Returns the second element in the pair.
     *
     * @return the second element in the pair.
     */
    public T2 getSecond() {
        return t2;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Pair pair = (Pair) o;

        if (t1 != null ? !t1.equals(pair.t1) : pair.t1 != null) return false;
        if (t2 != null ? !t2.equals(pair.t2) : pair.t2 != null) return false;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = t1 != null ? t1.hashCode() : 0;
        result = 53 * result + (t2 != null ? t2.hashCode() : 0);
        return result;
    }
}

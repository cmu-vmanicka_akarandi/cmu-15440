package edu.cmu.cs.akv.ds.mapreduce;

import edu.cmu.cs.akv.ds.mapreduce.exception.JobException;
import edu.cmu.cs.akv.ds.mapreduce.parser.InputFormat;
import edu.cmu.cs.akv.ds.mapreduce.remotestream.MultiFileChunk;
import edu.cmu.cs.akv.ds.mapreduce.remotestream.MultiFileChunkImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * {@code MapReduceJob} represents a single job and is responsible for executing it
 * to completion.
 *
 * @param <IK> - The input key type to the mapper.
 * @param <IV> - The input value type to the mapper.
 * @param <OK> - The output key type for the mapper.
 * @param <OV> - The output value type for the mapper.
 */
public class MapReduceJob<IK extends Serializable, IV extends Serializable,
        OK extends Comparable & Serializable, OV extends Serializable>
{
    private static final Logger LOG = LogManager.getLogger(MapReduceJob.class);

    /**
     * Represents the different states the job and its tasks can be in.
     */
    public enum Status {
        NOT_STARTED, INPROGRESS, FAILED, COMPLETED
    }
    private static final AtomicInteger JOB_ID_PROVIDER = new AtomicInteger(0);

    private final Node masterNode;
    private final String jobId;
    private final List<Node> slaveNodes;
    private final Class<Mapper<IK, IV, OK, OV>> mapperClazz;
    private final Class<Reducer<OK, OV>> reducerClazz;
    private final Class<InputFormat<IK, IV>> inputFormatClazz;
    private final List<File> inputFiles;
    private final int minSplitSize;
    private final int maxRetries;
    private final ExecutorService executorService;
    private final Object lock;
    private final Random random;
    private final List<MapperTask> mapperTasks;
    private final List<ReducerTask> reducerTasks;
    private final File workingDir;

    private Status status;
    private String failureReason;
    private File outputFile;

    /**
     * Creates a new {@code MapReduceJob} from the given configuration parameters.
     *
     * @param masterNode - The node where the job was spawned.
     * @param slaveNodes - The list of slave nodes to execute the tasks of this job.
     * @param mapperClazz - The type of the mapper.
     * @param reducerClazz - The type of the reducer.
     * @param inputFormatClazz - The type of the input format parser.
     * @param inputFiles - The list of input file.
     * @param minSplitSize - The minimum number of bytes to allocate to a single mapper.
     * @param maxRetries - The maximum number of times to retry a task in the event of failure.
     * @param workingDir - The directory where the tasks can create files to store intermediate
     *                     resuts.
     */
    public MapReduceJob(final Node masterNode, final List<Node> slaveNodes,
                        final Class<Mapper<IK, IV, OK, OV>> mapperClazz,
                        final Class<Reducer<OK, OV>> reducerClazz,
                        final Class<InputFormat<IK, IV>> inputFormatClazz,
                        final List<File> inputFiles, final int minSplitSize,
                        final int maxRetries, final File workingDir)
    {
        this.masterNode = masterNode;
        this.slaveNodes = slaveNodes;
        this.mapperClazz = mapperClazz;
        this.reducerClazz = reducerClazz;
        this.inputFormatClazz = inputFormatClazz;
        this.jobId = masterNode.getNodeId() + '-'
                + Integer.toString(JOB_ID_PROVIDER.incrementAndGet());
        this.inputFiles = inputFiles;
        this.minSplitSize = minSplitSize;
        this.maxRetries = maxRetries;
        this.executorService = Executors.newCachedThreadPool();
        this.status = Status.NOT_STARTED;
        this.lock = new Object();
        this.failureReason = "";
        this.random = new Random(Objects.hash(jobId) * System.currentTimeMillis());
        this.mapperTasks = new ArrayList<>();
        this.reducerTasks = new ArrayList<>();
        this.workingDir = workingDir;
    }

    /**
     * Returns the unique identifier for the job.
     *
     * @return the unique identifier for the job.
     */
    public String getJobId() {
        return jobId;
    }

    /**
     * Asynchronously starts the job in a separate thread.
     *
     * @return a {@link java.util.concurrent.Future} that can be used to block on the
     *         completion of the job.
     */
    public Future<?> startAsync() {
        synchronized (lock) {
            if (status == Status.NOT_STARTED) {
                // Set status to in progress
                status = Status.INPROGRESS;
                return executorService.submit(
                        new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    // Start the job synchronously.
                                    start();
                                } catch (final JobException e) {
                                    LOG.error("Exception while executing job", e);
                                    markFailed(e);
                                }

                                // Entering critical section for resource status
                                synchronized (lock) {
                                    // Print messages depending on the final status
                                    if (status == Status.COMPLETED) {
                                        System.out.println(String.format("[%s] Job completed, " +
                                                "please view the output in %s", jobId,
                                                outputFile.getAbsolutePath()));
                                    } else if (status == Status.FAILED) {
                                        System.out.println(
                                                String.format("[%s] Job failed", jobId));
                                    } else {
                                        throw new IllegalStateException(
                                                "Job status cannot be " + status.toString());
                                    }
                                }
                            }
                        }
                );
            } else {
                throw new IllegalStateException("Job cannot be started more than once");
            }
        }
    }

    /**
     * Starts and runs the job to completion including splitting the work into tasks
     * and delegating them to different slaves.
     *
     * @throws JobException if there was a permanent failure to the job.
     */
    public void start() throws JobException {
        // Split the input across files to a list of multifile chunks each of which
        // may span across files.
        final List<MultiFileChunkImpl> mapperInputChunks;
        try {
            mapperInputChunks = Utils.split(slaveNodes.size(), minSplitSize, inputFiles);
        } catch (final IOException e) {
            throw new JobException(e);
        }

        // Run the mapper tasks
        final List<File> mapperOutputFiles = runMapperTasks(mapperInputChunks);

        synchronized (lock) {
            if (status != Status.INPROGRESS) {
                throw new JobException("Job status is " + status.toString());
            }
        }

        // Merge the outputs of the mapper tasks
        final File mergedMapperOutputsFile;
        try {
            mergedMapperOutputsFile = merge(mapperOutputFiles);
        } catch (final IOException e) {
            throw new JobException("Merging mapper outputs failed", e);
        }

        for (final File mapperOutputFile : mapperOutputFiles) {
            mapperOutputFile.delete();
        }

        // Split the merged mapper outputs to distribute the work for reducers.
        final List<File> reducerInputFiles;
        try {
            reducerInputFiles = splitLines(mergedMapperOutputsFile, slaveNodes.size());
        } catch (final IOException e) {
            throw new JobException("Splitting reducer inputs failed", e);
        }

        mergedMapperOutputsFile.delete();

        // Run the reducers
        final List<File> reducerOutputFiles = runReducerTasks(reducerInputFiles);
        for (final File reducerInputFile : reducerInputFiles) {
            reducerInputFile.delete();
        }

        // Concatenate the outputs of multiple reducers to generate the final output
        try {
            outputFile = concat(reducerOutputFiles);
        } catch (final IOException e) {
            throw new JobException("Concatenating reducer outputs failed", e);
        }

        for (final File reducerOutputFile : reducerOutputFiles) {
            reducerOutputFile.delete();
        }

        synchronized (lock) {
            if (status != Status.INPROGRESS) {
                throw new JobException("Job status is " + status.toString());
            } else {
                status = Status.COMPLETED;
            }
        }
    }

    /**
     * Delegates and tracks to completion the reducer tasks.
     *
     * @param reducerInputFiles - The input files to the reducer tasks.
     * @return a list of files that contain the outputs of the reducer tasks.
     */
    private List<File> runReducerTasks(final List<File> reducerInputFiles) {
        final List<File> reducerOutputFiles = new ArrayList<>();

        System.out.println(String.format("[%s] Allocating %d reducers", jobId,
                reducerInputFiles.size()));
        final List<Future<?>> futures = new ArrayList<>();
        // Generate a random permutation of the slaves to pick to which we assign the tasks.
        final int[] perm = genRandomPerm(slaveNodes.size());
        int c = -1;
        for (final File reducerInputFile : reducerInputFiles) {
            c++;
            final int p = c;
            // Spawn a reducer task in a separate thread.
            futures.add(executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        int permIndex = p;
                        int retries = 0;
                        while (true) {
                            try {
                                System.out.println(String.format(
                                        "[%s] Assigning reducer task to %s", jobId,
                                        slaveNodes.get(perm[permIndex]).getNodeId()));
                                // Submit a reducer task to a slave
                                final ReducerTask<OK, OV> reducerTask =
                                        new ReducerTask<>(masterNode, jobId,
                                                slaveNodes.get(perm[permIndex]),
                                                reducerClazz, reducerInputFile, workingDir);
                                synchronized (lock) {
                                    reducerTasks.add(reducerTask);
                                }
                                // Block on and collect the output from the slave.
                                reducerOutputFiles.add(reducerTask.launch());
                                break;
                            } catch (final RemoteException e) {
                                System.out.println(
                                        String.format("[%s] Error executing reducer task in %s",
                                                jobId,
                                                slaveNodes.get(perm[permIndex]).getNodeId()));
                                LOG.error("Error while executing reducer task", e);
                                if (retries < maxRetries) {
                                    retries++;
                                    permIndex = (permIndex + 1) % slaveNodes.size();
                                } else {
                                    System.out.println(
                                            String.format("[%s] Retried too many times", jobId));
                                    throw new JobException(e);
                                }
                            }
                        }
                    } catch (final JobException e) {
                        LOG.error("Exception while executing job", e);
                        markFailed(e);
                    }
                }
            }));
        }

        // Block on the completion of all the tasks.
        for (final Future<?> future : futures) {
            try {
                future.get();
            } catch (final ExecutionException | InterruptedException e) {
                LOG.error("Received exception while waiting for reducer to complete", e);
                markFailed(e);
            }
        }

        return reducerOutputFiles;
    }

    /**
     * Merge the output of all the mappers while maintaining the sorted order. This
     * is similar to the merge routine of the merge sort algorithm but performs it
     * on multiple inputs instead of two.
     *
     * @param mapperOutputFiles - THe output files to merge.
     * @return a single file that contains the merged output.
     * @throws IOException if there was a problem reading from or writing to the files.
     */
    private File merge(List<File> mapperOutputFiles) throws IOException {
        final File mergedMapperOutputsFile = new File(workingDir, jobId + "-mergedmapperoutputs");
        final PrintWriter outputWriter = new PrintWriter(mergedMapperOutputsFile);
        Pair<OK, List<OV>> lastWrittenP = null;

        // Initialize the scanners
        int i = -1;
        final Scanner[] scanners = new Scanner[mapperOutputFiles.size()];
        for (final File mapperOutputFile : mapperOutputFiles) {
            i++;
            scanners[i] = new Scanner(mapperOutputFile);
        }

        // Initialize the line pointers to each file
        i = -1;
        final String[] lines = new String[mapperOutputFiles.size()];
        for (final File mapperOutputFile : mapperOutputFiles) {
            i++;
            lines[i] = scanners[i].hasNextLine() ? scanners[i].nextLine() : null;
        }

        // Repeat while there is at least one incomplete file
        boolean done = false;
        while(!done) {
            Pair<OK, List<OV>> minP = null;
            int minIndex = -1;

            // Compute the minimum amongst the lines pointed to by the line pointers in each file.
            i = -1;
            for (final String line : lines) {
                i++;
                if (line != null) {
                    final Pair<OK, List<OV>> currP = Utils.pairFromLine(line);
                    if (minP == null || currP.getFirst().compareTo(minP.getFirst()) < 0) {
                        minP = currP;
                        minIndex = i;
                    }
                }
            }

            // Insert a $ character between lines that represent different keys to make
            // it easier to split reducer load later.
            if (minIndex == -1) {
                done = true;
            } else {
                if (lastWrittenP != null) {
                    if (!minP.getFirst().equals(lastWrittenP.getFirst())) {
                        outputWriter.println('$');
                    }
                }
                lastWrittenP = minP;
                outputWriter.println(Utils.pairToLine(minP));
                lines[minIndex] = scanners[minIndex].hasNextLine() ?
                        scanners[minIndex].nextLine() : null;
            }
        }

        outputWriter.close();

        return mergedMapperOutputsFile;
    }

    /**
     * Concatenates all the reducer output files to produce the final file.
     *
     * @param reducerOutputFiles - The list of files that contain the reducer outputs.
     * @return a single file whose contents are the concatenation of all the reducer outputs.
     * @throws IOException if there was a problem reading from or writing to the files.
     */
    private File concat(List<File> reducerOutputFiles) throws IOException {
        final File concatFile = new File(workingDir, jobId + "-output");

        final PrintWriter pw = new PrintWriter(concatFile);
        for (final File reducerOutputFile : reducerOutputFiles) {
            final Scanner s = new Scanner(reducerOutputFile);
            while (s.hasNextLine()) {
                pw.println(s.nextLine());
            }
            s.close();
        }
        pw.close();

        return concatFile;
    }

    /**
     * Delegates and tracks to completion the mapper tasks.
     *
     * @param mapperInputChunks - The input chunks to the mapper tasks.
     * @return a list of files that contain the outputs of the mapper tasks.
     */
    private List<File> runMapperTasks(final List<MultiFileChunkImpl> mapperInputChunks) {
        final List<File> mapperOutputFiles = new ArrayList<>();

        System.out.println(String.format("[%s] Allocating %d mappers", jobId,
                mapperInputChunks.size()));
        final List<Future<?>> futures = new ArrayList<>();
        // Generate a random permutation to select the slave nodes to execute the reducers
        final int[] perm = genRandomPerm(slaveNodes.size());
        int c = -1;
        for (final MultiFileChunk inputChunk : mapperInputChunks) {
            c++;
            final int p = c;
            // Spawn a thread to delegate a mapper task and track it to completion
            futures.add(executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        int permIndex = p;
                        int retries = 0;
                        while (true) {
                            try {
                                System.out.println(String.format(
                                        "[%s] Assigning mapper task to %s", jobId,
                                        slaveNodes.get(perm[permIndex]).getNodeId()));
                                // Create a mapper task and launch it in a slave
                                final MapperTask<IK, IV, OK, OV> mapperTask =
                                        new MapperTask<>(masterNode, jobId,
                                                slaveNodes.get(perm[permIndex]), inputFormatClazz,
                                                mapperClazz, inputChunk.getFileChunks(),
                                                workingDir);
                                synchronized (lock) {
                                    mapperTasks.add(mapperTask);
                                }
                                mapperOutputFiles.add(mapperTask.launch());
                                break;
                            } catch (final RemoteException e) {
                                System.out.println(
                                        String.format("[%s] Error executing mapper task in %s",
                                                jobId,
                                                slaveNodes.get(perm[permIndex]).getNodeId()));
                                LOG.error("Error while executing mapper task", e);
                                if (retries < maxRetries) {
                                    retries++;
                                    permIndex = (permIndex + 1) % slaveNodes.size();
                                } else {
                                    System.out.println(
                                            String.format("[%s] Retried too many times", jobId));
                                    throw new JobException(e);
                                }
                            }
                        }
                    } catch (final JobException e) {
                        LOG.error("Exception while executing job", e);
                        markFailed(e);
                    }
                }
            }));
        }

        for (final Future<?> future : futures) {
            try {
                future.get();
            } catch (final ExecutionException | InterruptedException e) {
                LOG.error("Received exception while waiting for mapper to complete", e);
                markFailed(e);
            }
        }

        return mapperOutputFiles;
    }

    /**
     * Marks the job as failed if it is still in progress.
     *
     * @param reason - The reason for the failure.
     */
    private void markFailed(final Exception reason) {
        synchronized (lock) {
            if (status == Status.INPROGRESS) {
                failureReason = reason.getCause().getMessage();
                status = Status.FAILED;
            }
        }
    }

    /**
     * Generates a random permutaion of the given size.
     *
     * @param size - The size of the permutation.
     * @return a random perumtation of the given size.
     */
    private int[] genRandomPerm(final int size) {
        int[] perm = new int[size];
        for(int i = 0; i < perm.length; i++)
            perm[i] = i;

        for(int i = 0; i < size; i++){
            int rand = i + random.nextInt(size - i);

            int temp = perm[i];
            perm[i] = perm[rand];
            perm[rand] = temp;
        }
        return perm;
    }

    /** Splits the lines of the the given file into the requested number of splits.
     *
     *
     * @param f - The file to split.
     * @param nSplits - The number of required splits.
     * @return the list of files containing the split lines.
     * @throws IOException if there was a problem reading from or writing to a file.
     */
    private List<File> splitLines(final File f, final int nSplits) throws IOException {
        final List<File> splitFiles = new ArrayList<>();

        // Count the number of lines.
        Scanner s = new Scanner(f);
        int nLines = 0;
        while (s.hasNextLine()) {
            s.nextLine();
            nLines++;
        }
        final int minLinesPerSplit = nLines / nSplits;

        s.close();
        s = new Scanner(f);

        // Split the lines.
        int c = 0;
        while (s.hasNextLine()) {
            c++;
            final File splitFile = new File(workingDir, jobId + "-reducerinput" + c);
            final PrintWriter pw = new PrintWriter(splitFile);
            nLines = 0;
            while (s.hasNextLine()) {
                nLines++;
                final String line = s.nextLine();
                pw.println(line);
                if (nLines >= minLinesPerSplit && line.startsWith("$")) {
                    break;
                }
            }
            pw.close();
            splitFiles.add(splitFile);
        }
        s.close();

        return splitFiles;
    }

    /**
     * Prints the status of the job along with the statuses of each of its tasks.
     */
    public void printStatus() {
        System.out.println("----------------------------");
        System.out.println("Job Id = " + jobId);
        System.out.println("Job Status = " + status.toString());
        if (status == Status.COMPLETED) {
            System.out.println("Output file = " + outputFile.getAbsolutePath());
        } else if (status == Status.FAILED) {
            System.out.println("Failure reason = '" + failureReason + "'");
        }

        if (status != Status.NOT_STARTED) {
            System.out.println("Tasks");
            System.out.println("-----");
            System.out.println("Id\t\t\t\t\tType\tSlave\t\t\tStatus");
            for (final MapperTask mapperTask : mapperTasks) {
                System.out.println(String.format("%s\t%s\t%s\t%s", mapperTask.getTaskId(),
                        "Mapper", mapperTask.getSlaveNode().getNodeId(), mapperTask.getStatus()));
            }
            for (final ReducerTask reducerTask : reducerTasks) {
                System.out.println(String.format("%s\t%s\t%s\t%s", reducerTask.getTaskId(),
                        "Reducer", reducerTask.getSlaveNode().getNodeId(),
                        reducerTask.getStatus()));
            }
        }
        System.out.println("----------------------------");
    }

    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        return Objects.hash(31, jobId);
    }

    /**
     * {@inheritDoc}
     */
    public boolean equals(final Object other) {
        if (other == null) {
            return false;
        } else if (other instanceof MapReduceJob) {
            MapReduceJob<?, ?, ?, ?> otherJob = (MapReduceJob) other;
            return Objects.equals(jobId, otherJob.jobId);
        } else {
            return false;
        }
    }
}

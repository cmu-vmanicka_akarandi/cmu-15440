package edu.cmu.cs.akv.ds.mapreduce;

import edu.cmu.cs.akv.ds.mapreduce.MapReduceJob.Status;
import edu.cmu.cs.akv.ds.mapreduce.exception.JobException;
import edu.cmu.cs.akv.ds.mapreduce.remotestream.RemoteInputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * {@code ReducerTask} represents a single reducer task and is responsible for
 * delegating the task to a slave and tracking it to completion.
 *
 * @param <K> - The type of key.
 * @param <V> - The type of value.
 */
public class ReducerTask<K extends Comparable & Serializable, V extends Serializable>
{
    private static final int BUFFER_SIZE = 1000;
    private static final AtomicInteger TASK_ID_PROVIDER = new AtomicInteger(0);

    private final Node masterNode;
    private final String taskId;
    private final Node slaveNode;
    private final Class<Reducer<K, V>> reducerClazz;
    private final File inputFile;
    private final Object lock;
    private final File workingDir;

    private Status status;
    private String failureReason;

    /**
     * Creates a new reducer task using the given parameters.
     *
     * @param masterNode - The node in which the associated job was spawned.
     * @param jobId - The unique identifier of the job.
     * @param slaveNode - The slave node to delegate the task to.
     * @param reducerClazz - The type of the reducer.
     * @param inputFile - The input file to the reducer.
     * @param workingDir - The directory where the reducer can create intermediate files.
     */
    public ReducerTask(final Node masterNode, final String jobId, final Node slaveNode,
                       final Class<Reducer<K, V>> reducerClazz,
                       final File inputFile, final File workingDir)
    {
        this.masterNode = masterNode;
        this.taskId = jobId + '-' + Integer.toString(TASK_ID_PROVIDER.incrementAndGet());
        this.slaveNode = slaveNode;
        this.reducerClazz = reducerClazz;
        this.inputFile = inputFile;
        this.lock = new Object();
        this.status = Status.NOT_STARTED;
        this.workingDir = workingDir;
    }

    /**
     * Launches the reducer task and delegates it to the slave.
     *
     * @return the file containing the reducer's output.
     * @throws RemoteException if there was a problem communicating with the slave.
     * @throws JobException if there was a non-retriable exception when runnning the task.
     */
    public File launch() throws RemoteException, JobException {
        synchronized (lock) {
            // Set the status to in progress
            if (status != Status.NOT_STARTED) {
                throw new IllegalStateException(
                        "Cannot launch when status is " + status.toString());
            } else {
                status = Status.INPROGRESS;
            }
        }

        try {
            // Lookup the slave's task tracker
            final Registry slaveRegistry =
                    LocateRegistry.getRegistry(slaveNode.getIp(), slaveNode.getPort());
            final TaskTracker slaveLiaison;
            try {
                slaveLiaison = (TaskTracker) slaveRegistry.lookup("taskTracker");
            } catch (final NotBoundException e) {
                throw new RemoteException("Slave has not bound its task tracker", e);
            }
            final RemoteInputStream slaveInputStream;
            try {
                // Delegate the reducer task
                slaveInputStream = slaveLiaison.acceptReducerTask(masterNode, taskId,
                        reducerClazz, inputFile.getAbsolutePath());
            } catch (final IOException e) {
                throw new RemoteException(e.getMessage(), e);
            }

            // Stream the output from the slave to the local file system.
            try {
                final File outputFile = new File(workingDir, taskId + "-streamedreduceroutput");
                writeToFile(outputFile, slaveInputStream);

                synchronized (lock) {
                    if (status == Status.INPROGRESS) {
                        status = Status.COMPLETED;
                    }
                }

                return outputFile;
            } catch (final IOException e) {
                throw new RemoteException("Exception while reading reducer output", e);
            }
        } catch (final IOException | JobException e) {
            synchronized (lock) {
                if (status == Status.INPROGRESS) {
                    failureReason = e.getMessage();
                    status = Status.FAILED;
                }
            }
            throw e;
        }
    }

    /**
     * Returns the status of the task.
     * @return the status of the task.
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Returns the reason for the failure of the task if the task failed.
     *
     * @return the reason for the failure of the task if the task failed.
     */
    public String getFailureReason() {
        return failureReason;
    }

    /**
     * Returns a unique identifier of the task.
     *
     * @return a unique identifier of the task.
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * Returns the slave node that executed or is scheduled to execute the task.
     *
     * @return the slave node that executed or is scheduled to execute the task.
     */
    public Node getSlaveNode() {
        return slaveNode;
    }

    /**
     * Streams the remote input stream and stores the bytes to a file in the local file system.
     *
     * @param file - The file to store the streamed contents.
     * @param slaveInputStream - The remote stream to read bytes over the network.
     * @throws IOException if there was a problem reading from the stream or writing to the file.
     */
    private void writeToFile(final File file, final RemoteInputStream slaveInputStream)
            throws IOException
    {
        final FileOutputStream fos = new FileOutputStream(file);
        Pair<Integer, byte[]> readPair;

        while((readPair = slaveInputStream.read(BUFFER_SIZE)).getFirst() != -1) {
            fos.write(readPair.getSecond(), 0, readPair.getFirst());
        }

        fos.close();
        slaveInputStream.close();
    }
}

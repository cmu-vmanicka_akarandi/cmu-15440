package edu.cmu.cs.akv.ds.mapreduce.parser;

import edu.cmu.cs.akv.ds.mapreduce.Pair;

import java.io.InputStream;
import java.util.Scanner;

/**
 * {@code SpaceDelimitedInputFormat} splits the input stream into a stream of
 * space delimited words.
 */
public class SpaceDelimitedInputFormat implements InputFormat<Integer, String> {
    private Scanner scanner;
    private Integer id;

    /**
     * Initializes the instance with the provided input stream.
     *
     * @param inputStream - The inputStream to initialize the instance with.
     */
    @Override
    public void initialize(final InputStream inputStream) {
        id = 1;
        this.scanner = new Scanner(inputStream);
    }

    /**
     * Returns true if there exists a space delimited word yet to be read from the
     * stream.
     *
     * @return true if there exists a space delimited word yet to be read from the
     * stream.
     */
    @Override
    public Boolean hasNext() {
        return scanner.hasNext();
    }

    /**
     * Returns the next space delimited word in the stream along with a unique
     * identifier for it.
     *
     * @return the next space delimited word in the stream along with a unique
     * identifier for it.
     */
    @Override
    public Pair<Integer, String> next() {
        return new Pair<>(id++, scanner.next());
    }
}

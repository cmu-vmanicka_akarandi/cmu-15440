package edu.cmu.cs.akv.ds.mapreduce;

/**
 * {@code BytesChunk} represents a chunk of bytes in a single file.
 */
public class BytesChunk {
    private final String fileName;
    private final int byteStartPos;
    private final int byteEndPos;

    /**
     * Creates an instance of {@code BytesChunk} with the given file name and the
     * start and end byte positions that identify the chunk within the file.
     *
     * @param fileName - The name of the file that contains the chunk.
     * @param byteStartPos - The position of the first byte in the chunk.
     * @param byteEndPos - The position of the last byte in the chunk.
     */
    public BytesChunk(final String fileName, final int byteStartPos, final int byteEndPos) {
        this.fileName = fileName;
        this.byteStartPos = byteStartPos;
        this.byteEndPos = byteEndPos;
    }

    /**
     * Returns the name of the file that contains the chunk.
     *
     * @return the name of the file that contains the chunk.
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Returns the position of the first byte that belongs to the chunk.
     *
     * @return the position of the first byte that belongs to the chunk.
     */
    public int getByteStartPos() {
        return byteStartPos;
    }

    /**
     * Returns the position of the last byte that belongs to the chunk.
     *
     * @return the position of the last byte that belongs to the chunk.
     */
    public int getByteEndPos() {
        return byteEndPos;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BytesChunk that = (BytesChunk) o;

        if (byteEndPos != that.byteEndPos) return false;
        if (byteStartPos != that.byteStartPos) return false;
        if (!fileName.equals(that.fileName)) return false;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = fileName.hashCode();
        result = 43 * result + byteStartPos;
        result = 43 * result + byteEndPos;
        return result;
    }
}

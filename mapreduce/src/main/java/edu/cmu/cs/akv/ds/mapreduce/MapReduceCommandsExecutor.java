package edu.cmu.cs.akv.ds.mapreduce;

import edu.cmu.cs.akv.ds.mapreduce.cli.CommandsExecutor;
import edu.cmu.cs.akv.ds.mapreduce.exception.ConfigFormatException;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.cmu.cs.akv.ds.mapreduce.Constants.LIST_JOBS_COMMAND;
import static edu.cmu.cs.akv.ds.mapreduce.Constants.START_JOB_COMMAND;

/**
 * {@code MapReduceCommandsExecutor} is responsible for executing commands issued
 * to the framework either through stdin or sockets.
 */
public class MapReduceCommandsExecutor implements CommandsExecutor {
    private static final Logger LOG = LogManager.getLogger(MapReduceCommandsExecutor.class);

    private final JobTracker jobTracker;
    private final Node thisNode;
    private final List<Node> nodes;
    private final File workingDir;

    /**
     * Creates an instance of the {@code MapReduceCommandsExecutor}.
     *
     * @param jobTracker - The job tracker that can be used to dispatch jobs.
     * @param thisNode - The node in which the executor is running.
     * @param nodes - The list of all the nodes in the MapReduce cluster.
     * @param workingDir - The directory in which the jobs can store intermediate results.
     */
    public MapReduceCommandsExecutor(final JobTracker jobTracker, final Node thisNode,
                                     final List<Node> nodes, final File workingDir)
    {
        this.jobTracker = jobTracker;
        this.thisNode = thisNode;
        this.nodes = nodes;
        this.workingDir = workingDir;
    }

    /**
     * Executes a single command issued to the node.
     *
     * @param command - The command to execute
     * @return A response message to the entity that issued the command.
     */
    @Override
    public String execute(String command) {
        final String[] split = command.split(" ", 2);

        // Process a command to spawn a new job.
        if (START_JOB_COMMAND.equals(split[0])) {
            final Option configFileOption = new Option("c", "config-file", true,
                    "path of the job configuration file");
            configFileOption.setRequired(true);

            final Options options = new Options();
            options.addOption(configFileOption);

            final CommandLineParser parser = new BasicParser();
            try {
                final String[] args = split.length == 2 ? splitArgs(split[1]) : new String[]{};

                final CommandLine line = parser.parse(options, args);

                // The path of the job configuration file
                final String configFilePath = line.getOptionValue('c');
                final MapReduceJob<?, ?, ?, ?> job = ConfigUtils.parseJobConfig(thisNode, nodes,
                        new File(configFilePath), workingDir);

                // Launch the job asynchronously
                jobTracker.launchJobAsync(job);
                return String.format("Job with id [%s] launched", job.getJobId());
            } catch (final ParseException e) {
                LOG.error("Exception while parsing command line args", e);
                final HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp(START_JOB_COMMAND, options);
                return null;
            } catch (final ConfigFormatException e) {
                LOG.error("Exception while parsing configurations", e);
                System.out.println("Exception while trying to parse configurations, '" +
                        e.getMessage() + "'");
                System.out.println("Please try again!");
                return null;
            } catch (final IOException e) {
                LOG.error("Exception while reading config file", e);
                System.out.println("Exception while attempting to read config file, '" +
                        e.getMessage() + "'");
                System.out.println("Please try again!");
                return null;
            }
        } else if (LIST_JOBS_COMMAND.equals(split[0])) {
            // Process listjobs command
            jobTracker.printStatus();
            return null;
        } else if (command.isEmpty()) {
            return null;
        } else {
            return "Unknown command";
        }
    }

    /**
     * Splits arguments into an array of Strings using space delimiter (except when the arg is
     * surrounded by single quotes).
     *
     * @param unsplit - The flat String containing the space separated arguments.
     * @return the array of String with each element corresponding to an argument.
     */
    private String[] splitArgs(final String unsplit) {
        final List<String> list = new ArrayList<String>();
        final Matcher m = Pattern.compile("([^']\\S*|'.+?')\\s*").matcher(unsplit);
        while (m.find()) {
            String arg = m.group(1);
            if (arg.length() >= 2) {
                if (arg.charAt(0) == '\'') {
                    arg = arg.substring(1);
                }
                if (arg.charAt(arg.length() - 1) == '\'') {
                    arg = arg.substring(0, arg.length() - 1);
                }
            }
            list.add(arg);
        }

        final String[] args = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            args[i] = list.get(i);
        }
        return args;
    }
}

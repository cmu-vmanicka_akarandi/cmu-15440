package edu.cmu.cs.akv.ds.mapreduce;

/**
 * {@code Constants} contains a list of constants used in the framework.
 */
public class Constants {
    private Constants() {
    }

    /**
     * The script used to start a node instance.
     */
    public static final String START_NODE_SCRIPT = "./startnode.sh";

    /**
     * The command used to start a job.
     */
    public static final String START_JOB_COMMAND = "startjob";

    /**
     * The command used to list the jobs' statuses.
     */
    public static final String LIST_JOBS_COMMAND = "listjobs";
}

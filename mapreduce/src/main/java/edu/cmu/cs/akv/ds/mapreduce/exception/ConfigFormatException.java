package edu.cmu.cs.akv.ds.mapreduce.exception;

/**
 * {@code ConfigFormatException} represents an {@link java.lang.Exception} when
 * the configuration file supplied to the framework has an incorrect format.
 */
public class ConfigFormatException extends Exception {
    /**
     * {@inheritDoc}
     */
    public ConfigFormatException(final String message) {
        super(message);
    }

    /**
     * {@inheritDoc}
     */
    public ConfigFormatException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

package edu.cmu.cs.akv.ds.mapreduce.parser;

import edu.cmu.cs.akv.ds.mapreduce.Pair;

import java.io.InputStream;
import java.io.Serializable;

/**
 * {@code InputFormat} defines the interface to parse the input files supplied
 * to the MapReduce framework.
 *
 * @param <K> - The type of the mapper's input key.
 * @param <V> - The type of the mapper's input value.
 */
public interface InputFormat<K, V> extends Serializable {
    /**
     * Initializes the instance with the provided input stream. This method
     * is invoked exactly once and before any other method invocation in this
     * instance.
     *
     * @param inputStream - The inputStream to initialize the instance with.
     */
    void initialize(InputStream inputStream);

    /**
     * Returns true if there is a key value pair yet to be read from the stream
     * and false otherwise.
     *
     * @return true if there is a key value pair yet to be read from the stream
     * and false otherwise.
     */
    Boolean hasNext();

    /**
     * Returns the next key value pair from the stream.
     *
     * @return the next key value pair from the stream.
     */
    Pair<K,V> next();
}

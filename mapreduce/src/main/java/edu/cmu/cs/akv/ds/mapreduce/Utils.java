package edu.cmu.cs.akv.ds.mapreduce;

import edu.cmu.cs.akv.ds.mapreduce.remotestream.FileChunk;
import edu.cmu.cs.akv.ds.mapreduce.remotestream.MultiFileChunkImpl;
import edu.cmu.cs.akv.ds.mapreduce.remotestream.RandomAccessMultiFile;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Utils is a collection of utilities which are be used by other components.
 */

public class Utils {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private Utils() {
    }

    /**
     * Splits a list of input file into MultiFileChunks.
     * @param maxSplits -The maximum number of splits.
     * @param minSplitSize - A lower bound on all but one of the split sizes.
     * @param inputFiles - The list of input files to be split.
     * @return a list of MultiFileChunks, the result of the splitting.
     * @throws IOException if any seek or read fails.
     */
    public static List<MultiFileChunkImpl> split(final int maxSplits, final int minSplitSize,
                                             final List<File> inputFiles) throws IOException
    {
        final RandomAccessMultiFile randomAccessMultiFile=new RandomAccessMultiFile(inputFiles);
        final int fileSize = randomAccessMultiFile.length();

        final List<Integer> splits = new ArrayList<>();
        splits.add(0);
        final int width= ((maxSplits * minSplitSize < fileSize) ?
                (fileSize + maxSplits - 1) / maxSplits : minSplitSize);

        for(int p = width; p < fileSize; p += width) {
            randomAccessMultiFile.seek(p);

            for(; p < fileSize; ++p) {
                final int b = randomAccessMultiFile.read();
                final char[] c = Character.toChars(b);
                if(c[0] == '\n' || c[0] == '\t' || c[0] == ' ') {
                    break;
                }
            }

            if(p != fileSize) {
                splits.add(p);
            }
        }

        if(fileSize - splits.get(splits.size() - 1) < minSplitSize / 10) {
            splits.remove(splits.size() - 1);
        }

        final List<MultiFileChunkImpl> multiFileChunks = new ArrayList<>();
        for(int i = 0; i < splits.size(); ++i) {
            final List<FileChunk> fileChunks = randomAccessMultiFile.getChunksForRange(
                    splits.get(i), (i == splits.size() - 1 ? fileSize : splits.get(i+1)) - 1);
            multiFileChunks.add(new MultiFileChunkImpl(fileChunks));
        }

        return multiFileChunks;
    }

    /**
     * Serializes a pair to a line.
     *
     * @param p - The pair to serialize.
     * @return the serialized line.
     * @throws IOException if there was a problem serializing the pair.
     */
    public static String pairToLine(Pair<?, ?> p) throws IOException {
        final String serK = OBJECT_MAPPER.writeValueAsString(p.getFirst());
        final String serListV = OBJECT_MAPPER.writeValueAsString(p.getSecond());
        return OBJECT_MAPPER.writeValueAsString(new String[] {serK, serListV});
    }

    /**
     * Deserializes a pair from a line.
     *
     * @param line - The line that was written by serializing the pair.
     * @param <T1> - The type of the first element in the pair.
     * @param <T2> - The type of the second element in the pair.
     * @return the deserialized pair.
     * @throws IOException if there was a problem deserializing the pair.
     */
    public static <T1, T2> Pair<T1, T2> pairFromLine(final String line) throws IOException {
        final String[] deserLine = OBJECT_MAPPER.readValue(line, String[].class);
        final T1 t1 = OBJECT_MAPPER.readValue(deserLine[0], new TypeReference<T1>() {
        });
        final T2 t2 = OBJECT_MAPPER.readValue(deserLine[1], new TypeReference<T2>() {});
        return new Pair<>(t1, t2);
    }

    /**
     * Serializes the pair to a simple human readable line that can be deserialized to the pair again.
     *
     * @param p - The pair to serialize.
     * @return the serialized line.
     * @throws IOException if there was a problem serializing the pair.
     */
    public static String pairToSimpleLine(Pair<?, ?> p) throws IOException {
        final String serK = OBJECT_MAPPER.writeValueAsString(p.getFirst());
        final String serListV = OBJECT_MAPPER.writeValueAsString(p.getSecond());
        return serK + " " + serListV;
    }
}

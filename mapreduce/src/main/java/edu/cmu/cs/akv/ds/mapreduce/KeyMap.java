package edu.cmu.cs.akv.ds.mapreduce;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * {@code KeyMap} represents an association from keys to a list of values.
 *
 * @param <K> - The type of key.
 * @param <V> - The type of value.
 */
public class KeyMap<K extends Comparable & Serializable, V extends Serializable> {
    private final ConcurrentMap<K, List<V>> map;
    private final ConcurrentMap<K, Object> listLocks;

    /**
     * Creates a new instance of {@code KeyMap}.
     */
    public KeyMap() {
        map = new ConcurrentHashMap<>();
        listLocks = new ConcurrentHashMap<>();
    }

    /**
     * Adds the given value to the list of values associated with this key.
     *
     * @param key - The key to associate the value with.
     * @param value - The value to associate to the key.
     */
    public void addValue(final K key, final V value) {
        map.putIfAbsent(key, new ArrayList<V>());
        listLocks.putIfAbsent(key, new Object());

        final List<V> l = map.get(key);
        synchronized(listLocks.get(key)) {
            l.add(value);
        }
    }

    /**
     * Replaces all values associated with the given key with the provided value.
     *
     * @param key - The key whose associations must be removed and must be associated
     *              with the provided value.
     * @param value - The value that replaces all associations with the key.
     */
    public void setValue(final K key, final V value) {
        map.putIfAbsent(key, new ArrayList<V>());
        listLocks.putIfAbsent(key, new Object());

        final List<V> l = new ArrayList<>();
        l.add(value);
        synchronized(listLocks.get(key)) {
            map.put(key, l);
        }
    }

    /**
     * Returns the list of all values associated with the given key.
     *
     * @param key - The key whose associations must be returned.
     * @return the list of all values associated with the given key.
     */
    public List<V> valuesForKey(final K key) {
        map.putIfAbsent(key, new ArrayList<V>());
        listLocks.putIfAbsent(key, new Object());

        final List<V> l = map.get(key);
        synchronized(listLocks.get(key)) {
            return Collections.unmodifiableList(l);
        }
    }

    /**
     * Returns a {@link java.util.Set} of all the keys in the map.
     *
     * @return a {@link java.util.Set} of all the keys in the map.
     */
    public Set<K> getKeys() {
        return Collections.unmodifiableSet(map.keySet());
    }

    /**
     * Sorts the keys and returns them along with the list of associated values.
     *
     * @return the sorted keys and returns them along with the list of associated values.
     */
    public List<Pair<K, List<V>>> getSortedList() {
        final List<Pair<K, List<V>>> sortedList = new ArrayList<>(map.size());

        final List<K> sortedKeys = new ArrayList<>(map.keySet());
        Collections.sort(sortedKeys);

        for (final K key : sortedKeys) {
            sortedList.add(new Pair<>(key, valuesForKey(key)));
        }

        return Collections.unmodifiableList(sortedList);
    }

    /**
     * Returns the map as a list of keys and associated value pairs.
     *
     * @return the map as a list of keys and associated value pairs.
     */
    public List<Pair<K, List<V>>> getAsList() {
        final List<Pair<K, List<V>>> list = new ArrayList<>(map.size());

        final List<K> sortedKeys = new ArrayList<>(map.keySet());

        for (final K key : sortedKeys) {
            list.add(new Pair<>(key, valuesForKey(key)));
        }

        return Collections.unmodifiableList(list);
    }
}

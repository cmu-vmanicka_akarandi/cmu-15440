package edu.cmu.cs.akv.ds.mapreduce.remotestream;

import edu.cmu.cs.akv.ds.mapreduce.Pair;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * The {@code MultiFileChunk} interface is a abstraction for a collection of FileChunks.
 */

public interface MultiFileChunk extends Remote {
    /**
     * Returns the next byte in the {@code MultiFileChunk}.
     *
     * @return The next byte as an integer or -1 if there are no more bytes.
     * @throws IOException if the read fails.
     */
    int read() throws IOException;

    /**
     * Returns the length of the {@code MultiFileChunk}.
     *
     * @return the length of the {@code MultiFileChunk}
     * @throws RemoteException if there is a problem sending the result over the network.
     */
    int length() throws RemoteException;

    /**
     * Reads the next {@code size} bytes and return a pair of the number of bytes read and the array containing the bytes read,
     *
     * @param size - The number of bytes to be read.
     * @return A pair of the number of bytes read and the array containing the bytes read.
     * @throws IOException if the read fails.
     */
    Pair<Integer, byte[]> read(int size) throws IOException;

    /**
     * Return the list of fileChunks underlying the MultiFileChunk.
     *
     * @return the list of fileChunks underlying the MultiFileChunk
     * @throws RemoteException if there is a problem sending the result over the network.
     */
    List<FileChunk> getFileChunks() throws RemoteException;
}

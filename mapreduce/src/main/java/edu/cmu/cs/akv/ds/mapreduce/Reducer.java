package edu.cmu.cs.akv.ds.mapreduce;

import java.io.Serializable;
import java.util.List;

/**
 * The {@code Reducer} interface contains the methods the framework user must implement
 * to represent the reducer business logic of his application.
 *
 * @param <K> - The type of the key.
 * @param <V> - The type of the value.
 */
public interface Reducer<K extends Comparable & Serializable, V extends Serializable> {
    /**
     * Reduces the given key and the list of values and updates the reduced map to
     * reflect the same.
     *
     * @param key - The key to reduce.
     * @param values - The list of values to reduce.
     * @param reduced - The reduced output.
     * @throws Exception if there was a problem while reducing.
     */
    void reduce(K key, List<V> values, KeyMap<K, V> reduced) throws Exception;
}
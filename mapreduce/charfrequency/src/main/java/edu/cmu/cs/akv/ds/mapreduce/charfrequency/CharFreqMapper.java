package edu.cmu.cs.akv.ds.mapreduce.charfrequency;

import edu.cmu.cs.akv.ds.mapreduce.KeyMap;
import edu.cmu.cs.akv.ds.mapreduce.Mapper;

public class CharFreqMapper implements Mapper<Integer, String, String, Integer> {
    @Override
    public void map(final Integer key, final String value,
                    final KeyMap<String, Integer> mapped)
            throws Exception
    {
        final char c = value.toLowerCase().charAt(0);
        if (c >= 'a' && c <= 'z') {
            mapped.addValue(Character.toString(c), 1);
        }
    }
}

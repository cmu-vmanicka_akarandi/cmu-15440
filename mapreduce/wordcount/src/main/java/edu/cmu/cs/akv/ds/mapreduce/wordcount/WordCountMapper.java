package edu.cmu.cs.akv.ds.mapreduce.wordcount;

import edu.cmu.cs.akv.ds.mapreduce.KeyMap;
import edu.cmu.cs.akv.ds.mapreduce.Mapper;

public class WordCountMapper implements Mapper<Integer, String, String, Integer> {
    @Override
    public void map(final Integer key, final String value, final KeyMap<String, Integer> mapped)
            throws Exception
    {
        String word = value;

        if (word.startsWith(",") || word.startsWith(".") ||
                word.startsWith(";") || word.startsWith(":") ||
                word.startsWith("?")) {
            word = word.substring(1, word.length());
        }

        if (word.endsWith(",") || word.endsWith(".") ||
                word.endsWith(";") || word.endsWith(":") ||
                word.endsWith("?")) {
            word = word.substring(0, word.length() - 1);
        }

        mapped.addValue(word, 1);
    }
}

package edu.cmu.cs.akv.ds.mapreduce.wordcount;

import edu.cmu.cs.akv.ds.mapreduce.KeyMap;
import edu.cmu.cs.akv.ds.mapreduce.Reducer;

import java.util.List;

public class WordCountReducer implements Reducer<String, Integer> {
    @Override
    public void reduce(String key, List<Integer> values, KeyMap<String, Integer> reduced)
            throws Exception
    {
        int count = 0;
        for (final Integer value : values) {
            count += value;
        }

        final List<Integer> currList = reduced.valuesForKey(key);
        final int currCount = currList.isEmpty() ? 0 : currList.get(0);
        reduced.setValue(key, currCount + count);
    }
}
